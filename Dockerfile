# Dockerfile
FROM python:3.12

COPY requirements.txt /docker-pytest/requirements.txt
WORKDIR /docker-pytest

RUN apt-get update -y && apt-get install -y curl
RUN curl -fsSL https://raw.githubusercontent.com/databricks/setup-cli/main/install.sh | sh
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
