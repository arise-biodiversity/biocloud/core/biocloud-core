# Biocloud Core

## Intro

This solution handles the movement of media-items (or payloads, e.g. camera images) from the landing zone (LZ: an S3
bucket) to a blob-storage S3 bucket. It also moves separate metadata from the LZ to raw, to enriched and to the
curated layer of a lakehouse following the medallion architecture (also called bronze, silver and gold layers).
Validations are performed in the raw layer. Raw and enriched layers consist of Delta tables. The Curated layer consists
for now of Parquet files. Every layer has its own S3 bucket. This makes a total of 5 buckets used in the production
pipeline and 5 more in the development/ testing pipeline. The pipelines are scheduled to run using Databricks.

Currently, the pipeline handles data from 2 sources: Diopsis and MDS (work in progress). We use 2 separate repos to get
this data into the landing zone.

We make use of Delta Lake, Databricks, Pyspark and AWS Boto3.

## Getting started

[biocloud-core](https://gitlab.com/arise-biodiversity/biocloud/core/biocloud-core)

We are working on separate feature branches that we try to keep as small as possible
and prefer doing fast releases on the main branch. It should be possible to deploy the
main branch at any point and have a stable working production environment.

```bash
cd existing_repo
git remote add origin https://gitlab.com/arise-biodiversity/biocloud/core/biocloud-core.git
git branch -M main-databricks
git push -uf origin main-databricks
```

## 3 Configurations

Currently, we are in the process of moving to 3 different configurations:

* PROD for production (also using different S3 buckets)
* TEST for the integration test runs
* DEV for development

## Running the application locally

It is handy to make a virtual environment and install all required packages:

```bash
python3 -m venv <venv_dir>
source <venv_dir>/bin/activate
python3 -m pip install -r requirements.txt
```

And then activate or deactivate using:

```bash
source <venv_dir>/bin/activate
deactivate
```

You will need to set some environment variables, see .env-example for an example. Also, you will need to set some secrets to connect to the S3 buckets. Secrets can be found elsewhere. Ask your colleagues. You can use a .env file in
your repo to set all the variables.

The solution currently contains 3 pipeline stages which can be run independently: the raw, enriched and curated layer.
In biocloudcore/__main__.py the 3 orchestrators that run the 3 stages are called depending on the chosen layer given as a commandline argument. You can run e.g. the enriched layer with:

```bash
__main__.py --layer enriched
```

The solution can be run from within your IDE setting the Env. var. SPARK_ENVIRONMENT to "LOCAL" as well as setting up
Databricks for local development by defining the file .databrickscfg in your Home folder.

See the [wiki](https://gitlab.com/groups/arise-biodiversity/biocloud/-/wikis/6.-Databricks) and or the
Databricks docs for further help in setting up.

### Arguments

It is possible to add env arguments locally with VSCode or via Databricks
Workflows parameters.

The following arguments are required:

-`domain` ('biocloud', csc') - single string value
-`sources` ('diopsis', 'mds') - Multiple values possible Ex. "['diopsis']"
-`layer` ('raw', 'enriched', 'curated') - single string value

To test locally on vscode, the following settings can be added to the `launch.json` file with the needed arguments. To create your own launch.json you can use `vscode_example_launch.json` in the root of the domain as a base for your own launch.json by renaming it and moving it to the .vscode folder.

For example:

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Raw layer",
      "type": "python",
      "request": "launch",
      "module": "biocloudcore.__main__",
      "env": {
        "domain": "biocloud",
        "layer": "raw",
        "sources": "['diopsis']",
        "camera_numbers": "['455']",
      },
      "justMyCode": true
    },
  ]
}
```

## Running the application in Databricks GUI

You can also run the application from within the Databricks GUI using notebooks:
biocloudcore/notebooks/run_pipeline.py

Tests can also be run via the GUI:
tests/run_tests.py

## Automatic runs

The pipeline is scheduled to run every night in Databricks. 1 Hour before this pipeline, the Diopsis and MDS apps are
run. They put data in the landing zone which is then picked up by the pipeline of this biocloud-core app.

## Ingestion

The ingestion module in our code is designed to handle the ingestion of multiple sources, also known as providers, each as an individual pipeline. This module utilizes the same codebase as the biocloudcore module but with a different entry point called "run_ingest". When the "run_ingest" command is executed, it triggers the execution of the __main__.py file in the ingestion module instead of the biocloudcore module.

The ingestion module shares the same configurations, Spark and S3 services, as well as the GitLab CI/CD pipeline and Databricks template approach with the biocloudcore module. However, each source has its own specific configurations which are set in the providers folder within the respective source folder. In this folder, each source has its own config file named "{source}.py" where the specific configurations for that source are defined.

By utilizing this approach, we can easily manage and run ingestion pipelines for different sources using a single codebase and infrastructure. This allows us to maintain consistency and scalability across all the ingestion processes.

## Arguments

Just like with the main pipeline it is possible to add arguments in the launch.json or in the Databricks workflows. These are source-specific as they influence how and which data will be ingested.

One argument will always be required for each ingestion pipeline, which is "sources". With this argument
you can set which specific class in the ingestion module you want to run. For some sources this will be the
only needed argument to run the harvester.

### Nanopore arguments

```json
  {
    "name": "Ingest Nanopore",
    "type": "debugpy",
    "request": "launch",
    "module": "biocloudcore.ingestion.__main__",
    "env": {
      "sources": "['nanopore']",
    },
    "justMyCode": true
  },
```

### Diopsis arguments

Some of the sources have a lot of custom arguments to decide how much data and how we want to ingest.
Diopsis has the following arguments that can be set locally in your launch.json or in the Databricks workflow parameters:

-`camera_numbers`
-`max_payload_files_per_device`
-`max_pool_connections`
-`parallel_downloads`
-`startdate_filter`
-`enddate_filter`

They follow the same rules as the config / env variables `CAMERA_NUMBERS`, `MAX_PAYLOAD_FILES_PER_DEVICE`, `S3_MAX_POOL_CONNECTIONS`, `PARALLEL_DOWNLOADS`, `STARTDATE_FILTER` and `ENDDATE_FILTER` respectively.
To test locally on vscode, the following settings can be added to the `launch.json` file with the needed arguments, for example:

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Ingest Diopsis",
      "type": "debugpy",
      "request": "launch",
      "module": "biocloudcore.ingestion.__main__",
      "env": {
        "sources": "['diopsis']",
        "camera_numbers": "['447']",
        "max_payload_files_per_device": "-1",
        "max_pool_connections": "30",
        "parallel_downloads": "10",
        "startdate_filter": "2023-10-10",
      },
      "justMyCode": true
    }
  ]
}
```

## Unit and integration tests

The unittests and integration tests are respectively in the folders:

`tests/unit`
`tests/integration`

Unit tests are written using pytest and can be run using the standard pytest calls.

## Integration Tests

Add following entry point configuration to the vscode `launch.json` to run the integration tests.

```json
    {
      "name": "Integration Test",
      "type": "debugpy",
      "request": "launch",
      "module": "tests.integration.__main__",
      "env": {
        "domain": "biocloud",
        "layers": "['enriched']",
        "sources": "['diopsis']",
      },
      "justMyCode": true
    }
```

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 *`pre-commit autoupdate`
 *`pre-commit install`
