from setuptools import find_packages, setup

with open("requirements.txt") as f:
    required = [line for line in f.read().splitlines() if not line.startswith("-")]

setup(
    name="biocloudcore",
    version='0.1.2',
    author="Team biocloud",
    url="https://gitlab.com/arise-biodiversity/biocloud/core/biocloud-core",
    author_email="",
    description="biocloud-core",
    packages=find_packages(exclude=["tests.*", "tests"]),
    include_package_data=True,
    entry_points={"group_1": ["run=biocloudcore.__main__:main"]},
    install_requires=required,
    dependency_links=["https://gitlab.com/api/v4/groups/63885559/-/packages/pypi/simple/"],
    long_description="This package contains the core Databricks workflows of the biocloud project",
    long_description_content_type="text/markdown",
)
