resources:
  jobs:
    raw_csc_sources:
      name: raw_csc_sources
      description: >-
        # Raw CSC sources

        In this jobs we combine all the jobs as tasks that handle ingestion, and the raw cleaning steps that will apply
        minimal cleaning to the raw data so that the data is ready to be used in Enriched. We combine these tasks
        all into 1 job so that it is easy to trigger Enriched as Databricks Workflows for now do not have the
        functionality to wait on multiple jobs before executing :(

        To see the datamodel and description of this model check the Gitlab
        wiki:
        [Insert link to documentation]
        More info can be added here about the pipeline, pretty neat right!

      schedule:
        quartz_cron_expression: '34 0 0 * * ?'
        timezone_id: UTC
        pause_status: "PAUSED" #${PAUSE_STATUS}    FIXED ON PAUSED UNTIL RELEASE OF CSC PROD

      timeout_seconds: 4200
      health:
        rules:
          - metric: RUN_DURATION_SECONDS
            op: GREATER_THAN
            value: 2400 # Warning notification will be send after this amount of seconds

      tasks:
        # INGESTION
        - task_key: ingest_ada
          spark_python_task:
            python_file: biocloudcore/landing_zone/ada/run_ingest_ada.py
            source: GIT
          environment_key: serverless_environment

        - task_key: ingest_nanopore_api
          spark_python_task:
            python_file: biocloudcore/landing_zone/nanopore/run_ingest_nanopore.py
            source: GIT
          environment_key: serverless_environment

        # RAW ADA
        - task_key: raw_ada_registrations
          depends_on:
            - task_key: ingest_ada
          spark_python_task:
            python_file: biocloudcore/raw/ada/run_registrations.py
            source: GIT
          environment_key: serverless_environment

        - task_key: raw_ada_sheets
          depends_on:
            - task_key: ingest_ada
          spark_python_task:
            python_file: biocloudcore/raw/ada/run_sheets.py
            source: GIT
          environment_key: serverless_environment

        # RAW NANOPORE
        - task_key: raw_amplicons
          depends_on:
            - task_key: ingest_nanopore_api
          spark_python_task:
            python_file: biocloudcore/raw/nanopore/run_amplicons.py
            source: GIT
          environment_key: serverless_environment

        - task_key: raw_consensuses
          depends_on:
            - task_key: ingest_nanopore_api
          spark_python_task:
            python_file: biocloudcore/raw/nanopore/run_consensuses.py
            source: GIT
          environment_key: serverless_environment

        - task_key: raw_sequencing_runs
          depends_on:
            - task_key: ingest_nanopore_api
          spark_python_task:
            python_file: biocloudcore/raw/nanopore/run_sequencing_runs.py
            source: GIT
          environment_key: serverless_environment

        # TRIGGER ENRICHED CSC
        - task_key: trigger_enriched_csc
          depends_on:
            - task_key: raw_ada_registrations
            - task_key: raw_ada_sheets
            - task_key: raw_amplicons
            - task_key: raw_consensuses
            - task_key: raw_sequencing_runs
          run_job_task:
            job_id: ${enriched_csc} #We dynamically set the job id extracted from databricks

      # Static pipeline settings managed by CI/CD depending on environment

      git_source:
        git_url: https://gitlab.com/arise-biodiversity/biocloud/core/biocloud-core
        git_provider: gitLab
        git_branch: ${CI_COMMIT_BRANCH} # Git branch from where is deployed through CI/CD

      # optional parameter to queue pipelines to wait on set limits of runs on the compute
      # queue:
      #  enabled: true

      # optional parameter to make the workflows editable by default
      # edit_mode: EDITABLE

      environments:
        - environment_key: serverless_environment
          spec:
            client: "2"
            dependencies:
              # Currently we manage 1 environment via Gitlab CI/CD of python package dependencies on top of the
              # Databricks Serverless environments' packages.
              ${SERVERLESS_ENVIRONMENT_DEPENDENCIES}

      webhook_notifications:
        on_failure:
          - id: ${WEBHOOK_NOTIFICATIONS}

      run_as:
        service_principal_name: ${SERVICE_PRINCIPAL_NAME}

      tags:
        data-domain: csc
        git-branch: ${CI_COMMIT_BRANCH}
        git-deploy-started: ${CI_JOB_STARTED_AT}
        git-last-commit-time: ${CI_COMMIT_TIMESTAMP}
