import logging

from biocloudcore.curated.csc_api import CSCDashboardExport
from biocloudcore.data_lake import DataLake
from biocloudcore.spark_session import get_environment, set_default_catalog, spark_session


def run():
    tables = [
        "amplicon",
        "consensus_sequence",
        "dna_extract",
        "material_entity",
        "sequencing_run",
        "identification",
    ]

    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    catalog = set_default_catalog(spark=spark, domain="csc")
    data_lake = DataLake(env=get_environment(spark))

    CSCDashboardExport(spark=spark, catalog=catalog, data_lake=data_lake).run(tables=tables)

    spark.stop()


if __name__ == "__main__":
    run()
