import logging

from biocloudcore.curated.nimbostratus_api import DSIDiopsisExport
from biocloudcore.data_lake import DataLake
from biocloudcore.spark_session import get_environment, set_default_catalog, spark_session


def run():
    tables = [
        "location",
        "project",
        "deployment",
    ]

    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    catalog = set_default_catalog(spark=spark, domain="nimbostratus")
    data_lake = DataLake(env=get_environment(spark))

    DSIDiopsisExport(spark=spark, catalog=catalog, data_lake=data_lake).run(tables=tables)

    spark.stop()


if __name__ == "__main__":
    run()
