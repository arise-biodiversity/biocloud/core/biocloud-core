import logging

from biocloudcore.curated.dsi.media_item_joined import MediaItemJoined
from biocloudcore.data_lake import DataLake
from biocloudcore.spark_session import get_environment, set_default_catalog, spark_session


def run():
    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    catalog = set_default_catalog(spark=spark, domain="biocloud")
    data_lake = DataLake(env=get_environment(spark))
    data_consumer = "dsi"
    view_name = "media_item_joined"

    MediaItemJoined(spark=spark, catalog=catalog, data_lake=data_lake).run(
        view_name=view_name,
        data_consumer=data_consumer,
        destination_path=data_lake.dsi.curated.get_path(subdir=view_name, dataset_name="biocloud_simplified_model"),
    )


if __name__ == "__main__":
    run()
