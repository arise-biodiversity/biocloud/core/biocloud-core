import pyspark.sql.functions as F
from pyspark.sql import Catalog, SparkSession

from biocloudcore.data_lake import DataLake
from biocloudcore.utils.spark_utils import delta_to_df


class Project:
    def __init__(self, spark: SparkSession, catalog: Catalog, data_lake: DataLake):
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake

    def run(self, view_name: str, data_consumer: str, destination_path: str):
        sensor = delta_to_df(self.spark, db="enriched", table="project").select(
            # Duplicate the id column for easy migration Howard TODO: to be removed after Howard migrate
            "project_id",
            F.col("project_id").alias("project_golden_id"),
            # Select only the necessary columns
            "name",  # TODO: to be removed after Howard migrate
            F.col("name").alias("project_name"),
            "description",
            "start_date",
            "end_date",
            "inserted_ts_utc",
            "updated_ts_utc",
        )

        sensor.write.mode("overwrite").parquet(destination_path)

        # Create a view on the parquet file so it is visible in the Databricks Catalog
        self.spark.sql(
            f"CREATE OR REPLACE VIEW curated.{data_consumer}_{view_name}" " AS SELECT * FROM parquet.`{}`".format(
                destination_path
            )
        )
