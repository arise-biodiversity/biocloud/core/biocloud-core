import pyspark.sql.functions as F
from pyspark.sql import Catalog, SparkSession

from biocloudcore.data_lake import DataLake
from biocloudcore.utils.spark_utils import delta_to_df


class MediaItem:
    def __init__(self, spark: SparkSession, catalog: Catalog, data_lake: DataLake):
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake

    def run(self, view_name: str, data_consumer: str, destination_path: str):
        media_item = delta_to_df(self.spark, db="enriched", table="media_item").select(
            # Duplicate the id column for easy migration Howard TODO: to be removed after Howard migrate
            "media_item_id",
            F.col("media_item_id").alias("media_item_golden_id"),
            "sensor_golden_id",
            F.col("sensor_golden_id").alias("sensor_id"),
            "deployment_id",
            F.col("deployment_id").alias("deployment_golden_id"),
            # Select only the necessary columns
            "sensor_name",
            "deployment_name",
            "uri",
            "capture_ts_utc",
            "kind",
            "mime_type",
            "upload_success",
            "inserted_ts_utc",
            "updated_ts_utc",
        )

        media_item.write.mode("overwrite").parquet(destination_path)

        # Create a view on the parquet file so it is visible in the Databricks Catalog
        self.spark.sql(
            f"CREATE OR REPLACE VIEW curated.{data_consumer}_{view_name}" " AS SELECT * FROM parquet.`{}`".format(
                destination_path
            )
        )
