import pyspark.sql.functions as F
from pyspark.sql import Catalog, SparkSession

from biocloudcore.data_lake import DataLake
from biocloudcore.utils.spark_utils import delta_to_df


class MediaItemJoined:
    def __init__(self, spark: SparkSession, catalog: Catalog, data_lake: DataLake):
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake

    def run(self, view_name: str, data_consumer: str, destination_path: str):
        deployment = (
            delta_to_df(self.spark, db="enriched", table="deployment").select(
                "deployment_id",
                "latitude",
                "longitude",
                F.col("start_ts_utc").alias("deployment_start_ts_utc"),
                F.col("end_ts_utc").alias("deployment_end_ts_utc"),
            )
        ).dropDuplicates(["deployment_id"])

        sensor = (
            delta_to_df(self.spark, db="enriched", table="sensor").select(
                F.col("sensor_golden_id").alias("sensor_id"),
                "type",
                "model",
            )
        ).dropDuplicates(["sensor_id"])

        deployment_project = delta_to_df(self.spark, db="enriched", table="deployment_project").select(
            "deployment_id",
            "project_id",
        )

        project = (
            delta_to_df(self.spark, db="enriched", table="project").select(
                "project_id",
                F.col("name").alias("project_name"),
                F.col("description").alias("project_description"),
                F.col("start_date").alias("project_start_date"),
                F.col("end_date").alias("project_end_date"),
            )
        ).dropDuplicates(["project_id"])

        media_item_joined = (
            delta_to_df(self.spark, db="enriched", table="media_item")
            .select(
                # Duplicate the id column for easy migration Howard TODO: to be removed after Howard migrate
                "media_item_id",
                F.col("media_item_id").alias("media_item_golden_id"),
                "sensor_golden_id",
                F.col("sensor_golden_id").alias("sensor_id"),
                "deployment_id",
                F.col("deployment_id").alias("deployment_golden_id"),
                # Select only the necessary columns
                "sensor_name",
                "deployment_name",
                "uri",
                "capture_ts_utc",
                "kind",
                "mime_type",
                "upload_success",
                "inserted_ts_utc",
                "updated_ts_utc",
            )
            .join(
                deployment,
                on="deployment_id",
                how="left",
            )
            .join(
                sensor,
                on="sensor_id",
                how="left",
            )
            .join(
                deployment_project,
                on="deployment_id",
                how="left",
            )
            .join(
                project,
                on="project_id",
                how="left",
            )
        )

        media_item_joined.write.mode("overwrite").parquet(destination_path)

        # Create a view on the parquet file so it is visible in the Databricks Catalog
        self.spark.sql(
            f"CREATE OR REPLACE VIEW curated.{data_consumer}_{view_name}" " AS SELECT * FROM parquet.`{}`".format(
                destination_path
            )
        )
