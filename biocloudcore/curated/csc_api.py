from pyspark.sql import Catalog, SparkSession

from biocloudcore.data_lake import DataLake
from biocloudcore.utils.spark_utils import delta_to_df


class CSCDashboardExport:
    def __init__(self, spark: SparkSession, catalog: Catalog, data_lake: DataLake):
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake

    def run(self, tables: list[str]):
        for table_name in tables:
            enriched_df = delta_to_df(self.spark, db="enriched", table=table_name)

            enriched_df.write.mode("overwrite").parquet(self.data_lake.dna.curated.get_path(table_name))

            if not self.spark.catalog.tableExists(f"curated.service_layer_{table_name}"):
                self.spark.catalog.createTable(
                    f"curated.service_layer_{table_name}",
                    path=self.data_lake.dna.curated.get_path(table_name),
                    source="parquet",
                )
