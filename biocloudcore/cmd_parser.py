import logging
import sys
from datetime import datetime
from typing import Any

logger = logging.getLogger(__name__)


class ParameterParser:
    """
    Class to parse parameters from Databricks Workflow UI or CLI arguments.

    parser = ParameterParser.parse()

    print(parser.get("is_active", bool))       # True
    print(parser.get("start_date", datetime.date))  # 2024-01-15
    print(parser.get("items", list))           # ['apple', 'banana']
    print(parser.get("threshold", int))        # 10 (if passed)
    """

    def __init__(self, parameters=None):
        """Initialize with a dictionary of parameters."""
        self.parameters = parameters

    def parse_boolean(self, value: str) -> bool:
        """Convert a string to a boolean."""
        if isinstance(value, bool):
            return value
        if isinstance(value, str):
            value = value.strip().lower()
            if value in ("true", "1", "yes", "y"):
                return True
            elif value in ("false", "0", "no", "n"):
                return False
        raise ValueError(f"Invalid boolean value: {value}")

    def parse_list(self, value: str) -> list:
        """Convert a string to a comma-separated list."""
        if isinstance(value, list):
            return value
        if isinstance(value, str):
            string_list = value.split(",") if value else []
            return [str(s).strip() for s in string_list] if len(string_list) > 0 else []
        raise ValueError(f"Invalid list value: {value}")

    def parse_date(self, value: str) -> datetime.date:
        """Convert a string to a date object (yy-mm-dd format)."""
        try:
            return datetime.strptime(value, "%y-%m-%d").date()
        except ValueError as e:
            raise ValueError(f"Invalid date format (expected yy-mm-dd): {value}") from e

    def get(self, key: str, dtype: type = str, default: Any | None = None) -> Any:
        """
        Get a parameter value from the stored parameters with type conversion.

        :param str key: the name of the cmd parameter
        :param type dtype: the expected type of the parameter, to which the original string value will be converted to
        :param Any default: The default value to return when no value or parameter is set. Default is None
        :return Any: The converted parameter from the cmd line arguments or Databricks Workflow parameters

        If the value for the key is not `None`, the function attempts to convert it
        into the specified `dtype`. If the dtype is `bool`, `list`, or `datetime.date`,
        it uses specific parsing methods (`parse_boolean`, `parse_list`, `parse_date`).
        Otherwise, it uses the `dtype` constructor to perform the conversion.
        """
        value = self.parameters.get(key, default)
        if value is None:
            return default
        if dtype is bool:
            return self.parse_boolean(value)
        if dtype is list:
            return self.parse_list(value)
        if dtype is datetime.date:
            return self.parse_date(value)
        return dtype(value)  # Default conversion

    def parse(self):
        """
        Loads the Databricks job and task parameters or local CLI command line
        parameters as a dict object so that a user can influence how the pipeline code
        will be executed from the Databricks workflow UI or from the IDE.

        For more information on how to set these parameters in Databricks,
        check the wiki on Gitlab under the Databricks Workflows section.
        """

        params = {}
        if len(sys.argv[1:]) > 0:
            for databricks_parameter in sys.argv[1:]:
                key, value = databricks_parameter.split("=", 1)
                params[key.strip("-")] = value
        else:
            logger.info("No command line parameters provided.")
        return ParameterParser(params)
