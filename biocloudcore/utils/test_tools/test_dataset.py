from pyspark.sql.types import StructType


class TestDataset:
    # Disables Pytest trying to discover this class as a test (because of the name starting with 'Test')
    __test__ = False

    def __init__(self, filename: str, data: list[dict[str, any]], schema: StructType):
        """
        :param filename: filename of the dataset, extension NOT included
        :param data: content of the dataset
        :param schema: schema of the dataset (eg required when writing to parquet)
        """
        self.filename = filename
        self.data = data
        self.schema = schema


class CsvTestDataset:
    def __init__(self, filename: str, data: list[dict[str, any]], header: list[str]):
        """
        :param filename: filename of the dataset, extension NOT included
        :param data: content of the dataset
        :param header: CSV header of the dataset
        """
        self.filename = filename
        self.data = data
        self.header = header


class JsonTestDataset:
    def __init__(self, filename: str, data: int | float | str | tuple | list | dict):
        """
        :param filename:
        :param data: data to be formatted as JSON, so NOT yet a string containing JSON itself.
        Regarding accepted types:  note that JSON can take the form of any data type that is
        valid for inclusion inside JSON, not just JSON arrays or JSON objects.
        """
        self.filename = filename
        self.data = data
