import logging

from delta import DeltaTable
from deprecated import deprecated
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.types import StructType
from pyspark.sql.utils import AnalysisException

from biocloudcore.data_lake import S3Client

logger = logging.getLogger(__name__)


def create_database_if_not_exists(
    spark: SparkSession, catalog: Catalog, layer: str, domain: str, bucket: str, id: str = ""
) -> None:
    """
    Creates a new database within a Databricks Catalog if it does not already exist,
    and sets it as the current default database for subsequent operations.

    This function constructs the database name based on the domain name and the
    specified layer, ensuring that each layer of the domain has its own dedicated
    database. It then checks if the database exists, and if not, creates it with
    a location specified in an S3 bucket. After creation, the database is set as
    the current default database for the session.

    :param SparkSession spark: The Spark session
    :param Catalog catalog: The Databricks Catalog instance to interact with the catalog
    :param Layer layer: The layer of the domain for which the database  is being created
    :param str bucket: The name of the S3 bucket where the database and its tables will be stored
    """
    layer = layer.lower()

    database_name = f"`{catalog.currentCatalog()}`.{layer.lower()}{id}"

    if not catalog.databaseExists(database_name):
        spark.sql(
            f"""
            CREATE DATABASE IF NOT EXISTS {database_name}
            COMMENT 'Database containing the tables for the {layer}
                    layer for the {domain} domain.'
            MANAGED LOCATION 's3://{bucket}/{"id_tables/" if id else ""}{domain}'
            """
        )

        logger.info(
            f"Created database {database_name} for the {layer} layer" f" in the {domain} domain Databricks Catalog."
        )


def create_table_if_not_exists(
    spark: SparkSession, catalog: Catalog, schema: StructType, table_name: str, path: str, database=None
) -> None:
    """Creates a schema/database for the specified layer, if it doesn't already exist.
    It creates the schema in the specified layer, in the environment-specific bucket.
    """

    catalog.setCurrentDatabase(database) if database else None

    if not catalog.tableExists(table_name):
        schema_to_write = spark.createDataFrame([], schema)
        try:
            # TODO: make mode again error after testing to prevent overwriting s3 when catalog is deleted
            (
                schema_to_write.write.format("delta")
                .option("path", path)
                .option("delta.enableIcebergCompatV2", "true")
                .option("delta.universalFormat.enabledFormats", "iceberg")
                .mode("overwrite")
                .saveAsTable(table_name)
            )
            logger.info(f"Table {table_name} written to {path} and is now available in the Databricks Catalog.")
        except AnalysisException as e:
            raise Exception(
                f"Table {table_name} could not be written at {path}. "
                f"Although the table seems to not exist anymore in the Databricks Catalog, there is "
                "still data saved at the exact same file location in S3. Please check the S3 bucket."
                f"Exception: {e}"
            ) from e


def use_database(catalog: Catalog, layer: str) -> None:
    """Set the current default database to a specific layer in our DataLake model."""
    catalog.setCurrentDatabase(layer.lower())


def create_dataframe_from_tsv(spark: SparkSession, s3_client: S3Client, file_path: str) -> DataFrame:
    """Create a dataframe from a tsv file located at file_path."""
    # Check if the path file we received ends with .tsv.
    if not file_path.endswith(".tsv"):
        raise Exception(f"The given file does not have the .tsv extension: {file_path}")

    try:
        # TODO: Is there a cleaner way to pass the s3 client to the spark.read.csv method?
        dataframe = (
            spark.read.option("delimiter", "\t")
            .option("fs.s3a.access.key", s3_client.access_key_id)
            .option("fs.s3a.secret.key", s3_client.secret_access_key)
            .csv(file_path, header=True)
        )
    except AnalysisException as e:
        raise Exception(f"Could not convert given tsv file to dataframe. Exception: {e}") from e

    logger.debug(f"Created dataframe from tsv-file: {file_path}.")
    return dataframe


def create_dataframe_from_json(
    spark: SparkSession, file_path, schema: StructType | None = None, multiline: str = "true"
) -> DataFrame:
    """
    Create a dataframe from a JSON file located at file_path.
    The schema is deduced from the JSON if it is not present. This is e.g. the case for Diopsis.
    For MDS there is a schema-file available, so it is used for creating the dataframe.
    """
    if schema is not None:
        dataframe = spark.read.schema(schema).option("multiline", multiline).json(file_path)
    else:
        logger.info("No schema file available. Creating a schema on the fly.")
        dataframe = spark.read.option("multiline", multiline).json(file_path)

    logger.debug(dataframe.printSchema())
    logger.debug(f"Created dataframe from JSON-file: {file_path}.")
    return dataframe


@deprecated(version="0.1.2", reason="delta_to_df instead as it is more clear and uses the DeltaTable API")
def load_table(spark: SparkSession, table_path: str) -> None | DataFrame:
    """Create a dataframe from the Delta table located at <table_path>.

    Returns the dataframe or None if the table does not exist.
    """
    dataframe = None
    if DeltaTable.isDeltaTable(spark, table_path):
        dataframe = spark.read.load(table_path)
        logger.debug(dataframe.printSchema())
        logger.debug(f"Created dataframe from Delta table with path: {table_path}.")
    else:
        logger.debug(f"No dataframe loaded from path: {table_path}, because the path does not exist.")
    return dataframe


def delta_to_df(spark: SparkSession, db: str, table: str, catalog: Catalog = None) -> None | DataFrame:
    """
    Create a dataframe from the Delta table located at <database>.<table_name>.

    Returns the dataframe or None if the table does not exist.

    :param spark: The Spark session
    :param db: The database name
    :param table: The table name
    :param catalog: The Spark catalog, optional only if you want to use a different catalog
    :return: DataFrame or None
    """

    catalog = catalog if catalog else spark.catalog
    df = None
    if catalog.tableExists(f"{db}.{table}"):
        df = DeltaTable.forName(spark, f"`{catalog.currentCatalog()}`.{db}.{table}").toDF()
        logger.debug(df.printSchema())
    else:
        logger.debug(f"Table: `{catalog}`.{db}.{table}, was not found in the catalog.")

    return df


def _generate_schema_mismatch_exception(
    spark: SparkSession,
    source_df: DataFrame,
    database: str,
    destination_table_name: str,
    exception: AnalysisException,
) -> str:
    """Generate a detailed exception message when there is a schema mismatch
    between the source DataFrame and the destination Delta table.

    :param SparkSession spark: spark session
    :param DataFrame source_df: the source dataframe to be merged
    :param str database: the name of the database in the Delta catalog
    :param str destination_table_name: the name of the destination Delta table
    :param AnalysisException exception: the original exception
    :raises Exception: Raises an exception if there is a mismatch in columns
    :return str: detailed error message
    """

    # Load the existing Delta table and get schema as a set
    delta_table_cols = set(DeltaTable.forName(spark, f"{database}.{destination_table_name}").toDF().columns)

    # Convert source DataFrame columns to a set
    source_df_cols = set(source_df.columns)

    # Find missing columns using set operations
    missing_in_source = delta_table_cols - source_df_cols
    missing_in_delta = source_df_cols - delta_table_cols

    if missing_in_source or missing_in_delta:
        error_message = (
            "Column mismatch detected in schema's of source DataFrame "
            "and destination Delta Table.\n"
            f"Destination table: {database}.{destination_table_name}\n"
            "Missing columns in the source DataFrame: "
            f"{sorted(missing_in_source)}\n"
            "Missing columns in the destination table: "
            f"{sorted(missing_in_delta)}\n\n"
            f"The original exception: {exception} \n"
        )
    else:
        error_message = (
            "Cannot Resolve AnalysisException but no schema mismatch detected. \n"
            f"Throwing original exception: {exception} \n"
        )
    return error_message


@deprecated(version="0.1.2", reason="Use upsert_to_delta_table instead. This is the old version")
def write_to_delta_table(
    spark: SparkSession,
    table_path: str,
    df: DataFrame,
    primary_key_columns: list,
    update_columns: list | None = None,
    change_columns: list | None = None,
    partition_by: str | None = None,
    auto_schema_update: bool = False,
) -> None:
    """Writes a DataFrame to a Delta table, either by creating a new table
    or merging into an existing one.

    TODO: Caution this is a legacy function that will be deprecated in the future.
            Use upsert_to_delta_table instead.

    This function checks if the specified Delta table exists
    If it does, it performs a merge operation using the provided
    primary key columns. If the table does not exist, it creates a new Delta
    table with the DataFrame's schema. The function supports optional schema
    evolution, partitioning, and selective column updates.

    :param SparkSession spark: spark session
    :param str table_path: the destination Delta table path
    :param DataFrame df: the source DataFrame
    :param list primary_key_columns: Column names that constitute
        the primary key for upsert operations. These columns are used
        to determine matches between the DataFrame and the Delta table.
    :param list update_columns: A list of column names that should be updated
        during the merge operation. Default is None.
    :param Optional[list] change_columns: A list of column names to determine
        if a row has changed and should trigger an update. Default is None.
    :param Optional[str] partition_by: A column name or a list of column names
        to partition the Delta table by. Default is None.
    :param bool auto_schema_update: Flag indicating whether
        the Delta table schema should be automatically updated to accommodate
        the DataFrame's schema. Defaults to False.

    :raises Exception: Raises an exception if there is a schema mismatch when
        auto_schema_update is not enabled.
    """

    if DeltaTable.isDeltaTable(spark, table_path):
        try:
            if not update_columns:
                _merge_into_delta_table(spark, table_path, df, primary_key_columns)
            else:
                update_expr = {col: f"source.{col}" for col in update_columns}
                if change_columns:
                    _merge_into_delta_table(
                        spark,
                        table_path,
                        df,
                        primary_key_columns,
                        update_expr,
                        change_columns,
                    )
                else:
                    _merge_into_delta_table(spark, table_path, df, primary_key_columns, update_expr)
        except AnalysisException as e:
            error_message = str(e)
            if "Cannot resolve" in error_message:
                raise Exception(_generate_schema_mismatch_exception(spark, df, table_path, e)) from e
            else:
                raise e
    else:
        _save_as_delta_table(
            df,
            table_path,
            partition_by=partition_by,
            auto_schema_update=auto_schema_update,
        )


@deprecated(version="0.1.2", reason="Use upsert_to_delta_table instead. This is the old version")
def _merge_into_delta_table(
    spark: SparkSession,
    table_path: str,
    df: DataFrame,
    primary_key_columns: list[str],
    update_expr: dict | None = None,
    changed_cols: list[str] | None = None,
) -> None:
    """Merges a DataFrame into an existing Delta table, performing updates or
    inserts as necessary. This function performs an "upsert" operation on a
    Delta table based on a set of primary key columns. If a row in the
    DataFrame matches an existing row in the Delta table
    (based on the primary key columns), it will update the Delta table
    row based on the specified update expressions.
    If there is no match, it will insert the new row from the DataFrame
    into the Delta table.

    :param SparkSession spark: SparkSession object.
    :param str table_path: Path to the Delta table.
    :param DataFrame df: DataFrame to upsert.
    :param list[str] primary_key_columns: Column names that constitute
        the primary key for upsert operations. These columns are used
        to determine matches between the DataFrame and the Delta table.
    :param Optional[dict] update_expr: A dict where keys are column names
        in the Delta table that need to be updated, and values are expressions
        (as strings) defining how to update the column based on the matched row
        in the DataFrame. Default is None.
    :param Optional[list[str]] changed_cols: Column names to check for changes.
        If specified, only rows where at least one of these columns
        has changed will be updated. Default is None, which means all
        matched rows will be updated regardless of changes.
    """
    table = DeltaTable.forPath(spark, table_path)
    condition = " AND ".join(f"table.{col} = source.{col}" for col in primary_key_columns)

    merge_builder = table.alias("table").merge(df.alias("source"), condition)

    if update_expr:
        update_condition = " OR ".join(f"table.{col} != source.{col}" for col in changed_cols) if changed_cols else None
        merge_builder = merge_builder.whenMatchedUpdate(condition=update_condition, set=update_expr)

    merge_builder.whenNotMatchedInsertAll().execute()


def upsert_to_delta_table(
    spark: SparkSession,
    database: str,
    table_name: str,
    df: DataFrame,
    primary_key_columns: list[str],
    static_columns: list = None,
    ignore_change_columns: list = None,
    auto_schema_merge: bool = False,
    optimize_table: bool = False,
) -> None:
    """Merges a DataFrame into an existing Delta table, performing updates or
    inserts as necessary. This function performs an "upsert" operation on a
    Delta table based on a set of primary key columns. If a row in the
    DataFrame matches an existing row in the Delta table
    (based on the primary key columns), it will update the Delta table
    row based on the specified update expressions.
    If there is no match, it will insert the new row from the DataFrame
    into the Delta table.

    :param SparkSession spark: SparkSession object.
    :param str database: Name of the database in the Delta catalog.
    :param str table_name: Name of the Delta table.
    :param DataFrame df: DataFrame to upsert.
    :param list[str] primary_key_columns: Column names that constitute
        the primary key for upsert operations. These columns are used
        to determine matches between the DataFrame and the Delta table.
    :param Optional[list] static_columns: A list of columns that should not be updated.
    :param Optional[list[str]] ignore_change_columns: Column names that are not checked on changes.
        If specified, changes to the columns in this list will not result as an updated row.
        This is useful when you have columns that are updated by the system and should
        not trigger an update. Example: "updated_ts_utc"
    :param bool auto_schema_merge: If True, automatically add new columns form the source dataframe
        to the Delta table schema. Defaults to False.
    :param bool optimize_table: If True, optimize the layout of the table. Defaults to False.
    """
    if spark.catalog.tableExists(f"{database}.{table_name}"):
        try:
            delta_table = DeltaTable.forName(spark, f"{database}.{table_name}")

            # Performs an automatic schema update if enabled
            if auto_schema_merge:
                merge_delta_schema(spark, database, table_name, df.schema)

            # Perform the upsert operation and update the possible newly added columns
            merge_builder = (
                delta_table.alias("destination")
                .merge(
                    source=df.alias("source"),
                    condition=" AND ".join(f"destination.{col} = source.{col}" for col in primary_key_columns),
                )
                .whenMatchedUpdate(
                    condition=" OR ".join(
                        f"(destination.{col} IS DISTINCT FROM source.{col})"
                        for col in df.columns
                        if col not in ignore_change_columns
                    ),
                    set={col: f"source.{col}" for col in df.columns if col not in static_columns},
                )
                .whenNotMatchedInsertAll()
                # .whenNotMatchedBySourceDelete() TODO: can be added in the future when needed
            )
            result = merge_builder.execute()

            # Show the write action results
            result.show() if result else None

            if optimize_table:
                delta_table.optimize().executeCompaction()

        except AnalysisException as e:
            error_message = str(e)
            if "Cannot resolve" in error_message:
                raise Exception(_generate_schema_mismatch_exception(spark, df, database, table_name, e)) from e
            else:
                raise e


def merge_delta_schema(spark: SparkSession, database: str, table_name: str, source_schema: StructType) -> None:
    """
    Merge a source schema with a target schema in a Delta table. This function is an automatic schema
    updater for Delta tables, which is useful when the schema of the source DataFrame changes and the
    Delta table schema is behind.

    :param SparkSession spark: SparkSession object.
    :param str database: Name of the database in the Delta catalog.
    :param str table_name: Name of the Delta table.
    :param StructType source_schema: The schema of the source DataFrame or a self defined StructType schema
    """
    if spark.catalog.tableExists(f"{database}.{table_name}"):
        delta_table = DeltaTable.forName(spark, f"{database}.{table_name}")

        # Insert an empty DataFrame with the source schema to trigger schema evolution
        merge_builder = (
            delta_table.alias("destination")
            .merge(source=spark.createDataFrame([], source_schema).alias("source"), condition="false")
            .whenNotMatchedInsertAll()
            .withSchemaEvolution()
        )
        merge_builder.execute()

        # Log the schema evolution and the new columns that potentially have been added
        logging.info(
            f"Schema Evolution executed for Delta table {table_name}. "
            f"If there were new columns in the source schema, they are now added in the Delta table."
        )

    else:
        raise Exception(f"Table {database}.{table_name} does not exist in the Delta catalog.")


@deprecated(
    version="0.1.2", reason="This function is deprecated. No new function available yet, but not sure if needed."
)
def _save_as_delta_table(
    df: DataFrame,
    delta_table_path: str,
    write_mode: str = "error",
    partition_by: str | None = None,
    auto_schema_update: bool = False,
) -> None:
    """
    Saves a Spark DataFrame to a Delta table at the specified path,
    with configurable options for write mode, partitioning, and
    schema evolution. This function provides a flexible interface
    to write Spark DataFrames to Delta tables, allowing for control
    over how data is appended or overwritten, how the table is partitioned,
    and whether the table schema should be updated automatically to
    accommodate the DataFrame's schema.

    :param DataFrame df: The Spark DataFrame to save to Delta table.
    :param str delta_table_path: The path where the Delta table will be saved.
    :param Optional[str] write_mode: The write mode, defaults to 'error'.
        Other options include 'append', 'overwrite', etc.
    :param Optional[str] partition_by: The column to partition by,
        defaults to None. If specified, the table data will be partitioned
        based on this column.
    :param bool auto_schema_update: If True, automatically adds
        missing columns to the table schema, defaults to False.
    :return: None
    """
    writer = df.write.mode(write_mode).format("delta")

    if auto_schema_update:
        writer = writer.option("mergeSchema", "true")
    if partition_by:
        writer = writer.partitionBy(partition_by)
    writer.save(delta_table_path)
    logger.info(f"Created Delta table {delta_table_path} " "with write_mode: {write_mode}.")


def write_as_parquet_file(
    df: DataFrame, parquet_file: str, write_mode: str = "error", partition_by: str | None = None
) -> None:
    """Write Spark dataframe as parquet to specific path with <write_mode>."""
    if partition_by is None:
        df.write.mode(write_mode).parquet(parquet_file)
    else:
        df.write.mode(write_mode).parquet(parquet_file).partitionBy(partition_by)
    logger.debug(df.show())
    logger.info(f"Wrote parquet file {parquet_file}.")


def delete_rows_from_delta_table(
    spark: SparkSession,
    database: str,
    table_name: str,
    logical_key_columns: list[str],
    rows_to_match: DataFrame,
    inverted_match: bool = False,
) -> None:
    """Deletes a subset of rows from a Delta table. We delete the rows based
    on the provided logical_key_columns. Whenever there is a match between
    these logical_key_columns between the provided dataframe and the destination
    Delta table, the rows will be deleted.

    When Deletion Vectors are enabled, this operation is way more efficient,
    as not the whole parquet files have to be recreated.

    see: https://docs.databricks.com/en/admin/workspace-settings/deletion-vectors.html

    TODO: Enable deletion vectors on all tables in the Data Lake.

    :param SparkSession spark: SparkSession object.
    :param str database: Name of the database in the Delta catalog.
    :param str table_name: Name of the Delta table.
    :param list[str] logical_key_columns: Column names that constitute
        unique identifiers between the rows to delete and the destination
        Delta table. Typically these are the enriched logical_keys.
        These columns are used to determine matches between the DataFrame
         and the Delta table and when matched delete them.
    :param DataFrame rows_to_match: A DataFrame containing a selection
    of rows that have to be matched (or not matched) to trigger a delete.
    :param Optional[bool] inverted_match: Optional parameter to change the default
    behaviour (to delete when matched) to an inverted match behaviour, in which all the
    rows will be deleted that do not match the provided rules.
    """

    if spark.catalog.tableExists(f"{database}.{table_name}"):
        try:
            delta_table = DeltaTable.forName(spark, f"{database}.{table_name}")

            # Delete the records from the Delta table
            delete_builder = delta_table.alias("destination").merge(
                source=rows_to_match.alias("source"),
                condition=" AND ".join(f"destination.{col} = source.{col}" for col in logical_key_columns),
            )
            delete_builder = (
                delete_builder.whenMatchedDelete()
                if not inverted_match
                else delete_builder.whenNotMatchedBySourceDelete()
            )
            result = delete_builder.execute()

            # Show the delete action results
            result.show() if result else None

        except AnalysisException as e:
            raise e
    else:
        raise Exception(f"Table {database}.{table_name} does not exist in the Delta catalog.")


def repartition_table(spark: SparkSession, table_path: str, partitions: list | None = None) -> None:
    """This method repartitions an existing Delta table according to the specified partition columns."""
    delta_table = DeltaTable.forPath(spark, table_path)

    if partitions is None:
        raise ValueError("Partition columns must be specified.")
    else:
        delta_table.repartition(*partitions)
        delta_table.write.format("delta").mode("overwrite").save(table_path)
