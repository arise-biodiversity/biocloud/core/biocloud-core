import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import LongType, StringType, StructField, StructType, TimestampType

from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    add_timestamp_and_source_columns,
    generate_enriched_dataframes_with_golden_ids,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable, IdTableGenerator
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delta_to_df,
    merge_delta_schema,
    upsert_to_delta_table,
)


class DnaExtract:
    """
    **Description**

    The dna_extract table contains information about the dna extracts.
    This table is used to track the dna extracts

    **Available fields**

    +---------------------+--------------------------------------------------+----------+
    | NAME                | DESCRIPTION                                      | TYPE     |
    +=====================+==================================================+==========+
    |dna_extract_golden_id| Golden ID of the dna_extract [primary key]       | long     |
    +---------------------+--------------------------------------------------+----------+
    | material_entity     | The source of the dna extract                    | string   |
    | _golden_id          |                                                  |          |
    +---------------------+--------------------------------------------------+----------+
    | source              | The source of the dna extract                    | string   |
    +---------------------+--------------------------------------------------+----------+
    | source_id           | Original identifier of the item in the source    | string   |
    +---------------------+--------------------------------------------------+----------+
    | catalog_number      |                                                  | string   |
    +---------------------+--------------------------------------------------+----------+
    | stock_plate_id      | Identifier of the stock plate containing the     | string   |
    |                     | DNA/specimen extract                             |          |
    +---------------------+--------------------------------------------------+----------+
    | stock_plate_position| Position of the sample on the PCR plate          | string   |
    |                     | (well position)                                  |          |
    +---------------------+--------------------------------------------------+----------+
    | ggbn_preparationType|                                                  | string   |
    +---------------------+--------------------------------------------------+----------+
    | inserted_ts_utc     |                                                  | timestamp|
    +---------------------+--------------------------------------------------+----------+
    | updated_ts_utc      |                                                  | timestamp|
    +---------------------+--------------------------------------------------+----------+


    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.dna.enriched,
            schema=StructType(
                [
                    StructField("dna_extract_golden_id", LongType(), True),
                    StructField("material_entity_golden_id", LongType(), True),
                    StructField("dna_extract_id", StringType(), True),
                    StructField("catalog_number", StringType(), True),
                    StructField("stock_plate_id", StringType(), True),
                    StructField("last_updated_source", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), False),
                ]
            ),
            logical_keys=["dna_extract_id"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
        )
        self.id_table = IdTableGenerator(self.enriched_table)

    def run(self) -> None:
        """
        Runs the logic for the dna_extract table.
        Loading, validating, transforming and writing the dataframe.
        """
        for table in [self.enriched_table, self.id_table]:
            create_table_if_not_exists(
                self.spark,
                self.catalog,
                schema=table.schema,
                table_name=table.table_name,
                path=table.table_path,
                database=table.database,
            )

        # Merge the defined enriched StructType schema with the current schema in the Delta table
        merge_delta_schema(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            source_schema=self.enriched_table.schema,
        )

        raw_amplicons = delta_to_df(self.spark, db="raw", table="amplicons_validated")
        valid_data, invalid_data = self._validate(raw_amplicons)

        dna_extract_transformed = self._transform_columns(valid_data)
        dna_extract_transformed = add_timestamp_and_source_columns(dna_extract_transformed, self.source)

        # Add the material_entity_golden_id to the table
        material_entity = delta_to_df(self.spark, db="enriched", table="material_entity")
        dna_extract_with_relationships = dna_extract_transformed.join(
            material_entity.select("material_entity_golden_id", "catalog_number"),
            on=["catalog_number"],
            how="left",
        )

        dna_extract_with_golden_ids = generate_enriched_dataframes_with_golden_ids(
            self.spark,
            id_table_path=self.id_table.table_path,
            df=dna_extract_with_relationships,
            logical_keys=self.enriched_table.logical_keys,
            table_name=self.table_name,
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        for table in [self.enriched_table, self.id_table]:
            upsert_to_delta_table(
                self.spark,
                database=table.database,
                table_name=table.table_name,
                df=dna_extract_with_golden_ids[table.table_name],
                primary_key_columns=table.primary_key_columns,
                static_columns=table.static_columns,
                ignore_change_columns=table.ignore_change_columns,
            )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(lit(False))

    def _transform_columns(self, valid_data: DataFrame) -> DataFrame:
        """Transform Nanopore validated data df into enriched df following
        the data-model for the dna_extract table.
        source_id               id
        title                   title
        end_datetime            end_time
        """
        return valid_data.select(
            F.col("extract_id").cast("string").alias("source_id"),
            F.col("extract_id").cast("string").alias("dna_extract_id"),
            F.col("name").cast("string").alias("catalog_number"),
            F.col("stock_plate_id").cast("string"),
            # F.col("pcr_plate_position").cast("string"),
            # F.col("ggbn_preparationType").cast("string"),
            # F.col("inserted_ts_utc").cast("timestamp"),
            # F.col("updated_ts_utc").cast("timestamp"),
        )
