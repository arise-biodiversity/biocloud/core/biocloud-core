import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import (
    BooleanType,
    DecimalType,
    LongType,
    StringType,
    StructField,
    StructType,
    TimestampType,
)

from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    add_timestamp_and_source_columns,
    generate_primary_keys,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable, IdTableGenerator
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delta_to_df,
    merge_delta_schema,
    upsert_to_delta_table,
)


class ConsensusSequence:
    """
    **Description**

    The consensus_sequence table contains information about the consensus sequences.
    This table is used to track the sequencing

    **Available fields**

    +-----------------------------+--------------------------------------------------+----------+
    | NAME                        | DESCRIPTION                                      | TYPE     |
    +=============================+==================================================+==========+
    | consensus_sequence_id       | ID of the sequencingrun [primary key]            | long     |
    +-----------------------------+--------------------------------------------------+----------+
    | amplicon_golden_id          | Golden ID of the amplicon                        | long     |
    +-----------------------------+--------------------------------------------------+----------+
    | source                      | The source of the consensus sequence             | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | source_id                   | Original identifier of the item in the source    | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | amplicon_id                 |                                                  | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | consensus_sequence          |                                                  | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | is_valid_protein            | True if the sequence was translated into valid   | boolean  |
    |                             | protein (without stop codons). A null value      |          |
    |                             | means not translated                             |          |
    +-----------------------------+--------------------------------------------------+----------+
    | supporting_read_count       | Number of reads used to construct this consensus | long     |
    +-----------------------------+--------------------------------------------------+----------+
    | sample_designation          |                                                  | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | is_validated                |                                                  | boolean  |
    +-----------------------------+--------------------------------------------------+----------+
    | sequence_generated_datetime |                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+
    | legacy_last_updated_datetime|                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+
    | inserted_ts_utc             |                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+
    | updated_ts_utc              |                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+


    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.dna.enriched,
            schema=StructType(
                [
                    StructField("consensus_sequence_id", LongType(), True),
                    StructField("amplicon_golden_id", LongType(), True),
                    StructField("is_valid_protein", BooleanType(), True),
                    StructField("supporting_read_count", LongType(), True),
                    StructField("amplicon_id", StringType(), True),
                    StructField("read_ratio", DecimalType(), True),
                    StructField("consensus_sequence_length", LongType(), True),
                    StructField("consensus_sequence", StringType(), True),
                    # StructField("sample_designation", StringType(), True),
                    # StructField("is_validated", BooleanType(), True),
                    StructField("sequence_generated_datetime", TimestampType(), True),
                    # StructField("legacy_last_updated_datetime", TimestampType(), True),
                    StructField("last_updated_source", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), True),
                    StructField("source_id", StringType(), True),
                    StructField("source", StringType(), True),
                ]
            ),
            logical_keys=["source_id", "source"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
            primary_key_columns=[f"{self.table_name}_id"],
        )
        self.id_table = IdTableGenerator(self.enriched_table)

    def run(self) -> None:
        """
        Runs the logic for the consensus_sequence table.
        Loading, validating, transforming and writing the dataframe.
        """
        create_table_if_not_exists(
            self.spark,
            self.catalog,
            schema=self.enriched_table.schema,
            table_name=self.enriched_table.table_name,
            path=self.enriched_table.table_path,
            database=self.enriched_table.database,
        )

        # Merge the defined StructType schema with the current schema in the Delta table
        merge_delta_schema(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            source_schema=self.enriched_table.schema,
        )

        raw_consensuses = delta_to_df(self.spark, db="raw", table="consensuses_validated")
        valid_data, invalid_data = self._validate(raw_consensuses)

        consensus_sequence_transformed = self._transform_columns(valid_data)
        consensus_sequence_transformed = add_timestamp_and_source_columns(consensus_sequence_transformed, self.source)

        # Add the amplicon_golden_id to the table
        amplicon = delta_to_df(self.spark, db="enriched", table="amplicon")
        consensus_sequence_with_relationships = consensus_sequence_transformed.join(
            amplicon.select("amplicon_golden_id", "amplicon_id"),
            on="amplicon_id",
            how="left",
        )

        consensus_sequence_with_primary_keys = generate_primary_keys(
            df=consensus_sequence_with_relationships,
            logical_keys=self.enriched_table.logical_keys,
            primary_key_column=self.enriched_table.primary_key_columns[0],
            existing_data=delta_to_df(self.spark, db="enriched", table=self.table_name),
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        upsert_to_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            df=consensus_sequence_with_primary_keys,
            primary_key_columns=self.enriched_table.primary_key_columns,
            static_columns=self.enriched_table.static_columns,
            ignore_change_columns=self.enriched_table.ignore_change_columns,
        )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(lit(False))

    def _transform_columns(self, valid_data: DataFrame) -> DataFrame:
        """Transform Nanopore validated data df into enriched df following
        the data-model for the consensus_sequence table.
        """
        return valid_data.select(
            F.col("base_count").cast("long").alias("consensus_sequence_length"),
            F.col("is_valid_protein").cast("boolean"),
            F.col("read_ratio").cast("decimal"),
            F.col("supporting_read_count").cast("long"),
            F.col("id").cast("string").alias("source_id"),
            F.col("amplicon").cast("string").alias("amplicon_id"),
            F.col("sequence").cast("string").alias("consensus_sequence"),
            F.col("generated_at").cast("timestamp").alias("sequence_generated_datetime"),
        )
