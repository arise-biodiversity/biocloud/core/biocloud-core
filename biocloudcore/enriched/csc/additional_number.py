import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.types import LongType, StringType, StructField, StructType, TimestampType

from biocloudcore.common_dataframe_functions import (
    add_timestamp_columns,
)
from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    generate_primary_keys,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable, IdTableGenerator
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delta_to_df,
    merge_delta_schema,
    upsert_to_delta_table,
)


class AdditionalNumber:
    """
    **Description**

    The additional number table contains information about the additional number.

    **Available fields**

    +-----------------------------+--------------------------------------------------+----------+
    | NAME                        | DESCRIPTION                                      | TYPE     |
    +=============================+==================================================+==========+
    | additional_number_id        | ID of the additional number [primary key]        | long     |
    +-----------------------------+--------------------------------------------------+----------+
    | material_entity_golden_id   |                                                  | long     |
    +-----------------------------+--------------------------------------------------+----------+
    | number_type                 |                                                  | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | number_value                |                                                  | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | number_comment              |                                                  | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | source_id                   | Original identifier of the item in the source    | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | source                      | The source of the additional number              | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | inserted_ts_utc             |                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+
    | updated_ts_utc              |                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+


    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.dna.enriched,
            schema=StructType(
                [
                    StructField("additional_number_id", LongType(), True),
                    StructField("material_entity_golden_id", LongType(), True),
                    StructField("number_type", StringType(), True),
                    StructField("number_value", StringType(), True),
                    StructField("number_comment", StringType(), True),
                    StructField("source_id", StringType(), True),
                    StructField("source", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), True),
                ]
            ),
            logical_keys=["source_id", "source"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
            primary_key_columns=[f"{self.table_name}_id"],
        )
        self.id_table = IdTableGenerator(self.enriched_table)

    def run(self) -> None:
        """
        Runs the logic for the additional number table. Loading, validating, transforming and writing the dataframe.
        """

        create_table_if_not_exists(
            self.spark,
            self.catalog,
            schema=self.enriched_table.schema,
            table_name=self.enriched_table.table_name,
            path=self.enriched_table.table_path,
            database=self.enriched_table.database,
        )

        # Merge the defined enriched StructType schema with the current schema in the Delta table
        merge_delta_schema(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            source_schema=self.enriched_table.schema,
        )

        # Load raw tables we need to create the additional number enriched table
        raw_sheets = delta_to_df(self.spark, db="raw", table="sheets_validated")

        # Validate, transform and write the bulk registrations
        valid_sheet_data, invalid_sheet_data = self._validate(raw_sheets)

        sheets_transformed = self._transform_sheets(valid_sheet_data)
        sheets_transformed = add_timestamp_columns(sheets_transformed)

        # Add the material_entity_golden_id to the table
        material_entity = delta_to_df(self.spark, db="enriched", table="material_entity")

        additional_number_with_relationships = sheets_transformed.join(
            material_entity.select("material_entity_golden_id", F.col("catalog_number").alias("registration_number")),
            on="registration_number",
            how="left",
        )

        additional_number_transformed = self._transform_additional_number(additional_number_with_relationships)

        additional_number_with_primary_keys = generate_primary_keys(
            df=additional_number_transformed,
            logical_keys=self.enriched_table.logical_keys,
            primary_key_column=self.enriched_table.primary_key_columns[0],
            existing_data=delta_to_df(self.spark, db="enriched", table=self.table_name),
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        upsert_to_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            df=additional_number_with_primary_keys,
            primary_key_columns=self.enriched_table.primary_key_columns,
            static_columns=self.enriched_table.static_columns,
            ignore_change_columns=self.enriched_table.ignore_change_columns,
        )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(F.lit(False))

    def _transform_sheets(self, valid_data: DataFrame) -> DataFrame:
        """Transform the raw sheets dataframe into the enriched dataframe following
        the data-model for the additional number table.
        """

        # The spreadsheets allow three additional numbers to be provided.
        # However, each additional number needs to be a row in the additional numbers table.
        # We therefore map each additional number (one, two and three) to its corresponding column without the number.
        # This is basically an unpivot, but as we have nine columns that need to be mapped, this approach is simpler.
        unpivot_df = (
            valid_data.select(
                F.col("Additional_numbers_1_type").alias("number_type"),
                F.col("Additional_numbers_1_value").alias("number_value"),
                F.col("Additional_numbers_1_comment").alias("number_comment"),
                F.col("registration_number"),
                F.col("id").alias("source_id"),
            )
            .unionByName(
                valid_data.select(
                    F.col("Additional_numbers_2_type").alias("number_type"),
                    F.col("Additional_numbers_2_value").alias("number_value"),
                    F.col("Additional_numbers_2_comment").alias("number_comment"),
                    F.col("registration_number"),
                    F.col("id").alias("source_id"),
                )
            )
            .unionByName(
                valid_data.select(
                    F.col("Additional_numbers_3_type").alias("number_type"),
                    F.col("Additional_numbers_3_value").alias("number_value"),
                    F.col("Additional_numbers_3_comment").alias("number_comment"),
                    F.col("registration_number"),
                    F.col("id").alias("source_id"),
                )
            )
        )

        # Filter away all the additional number rows that are completely empty.
        unpivot_df = unpivot_df.filter(
            ~(F.col("number_type").isNull() & F.col("number_value").isNull() & F.col("number_comment").isNull())
        )

        return unpivot_df

    def _transform_additional_number(self, df: DataFrame) -> DataFrame:
        """
        Transforms the additional number dataframe with any transformations needed
        :param df (DataFrame)
        :return DataFrame
        """
        # We do not need the registration number column anymore
        df = df.withColumn("source", F.lit(self.source))
        return df.drop("registration_number")
