import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import (
    LongType,
    StringType,
    StructField,
    StructType,
    TimestampType,
)

from biocloudcore.common_dataframe_functions import add_timestamp_columns
from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    generate_primary_keys,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable, IdTableGenerator
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delta_to_df,
    merge_delta_schema,
    upsert_to_delta_table,
)


class Identification:
    """
    **Description**

    The Identification table contains all information about identifications of samples.

    **Available fields**

    +------------------------------+------------------------------------------------------------+-----------+
    | NAME                         | DESCRIPTION                                                | TYPE      |
    +==============================+============================================================+===========+
    | identification_id            |                                                            | long      |
    +------------------------------+------------------------------------------------------------+-----------+
    | material_entity_golden_id    |                                                            | long      |
    +------------------------------+------------------------------------------------------------+-----------+
    | consensus_sequence_golden_id |                                                            | long      |
    +------------------------------+------------------------------------------------------------+-----------+
    | identification_method        |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | verbatim_identification      |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | verbatim_identification_rank |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | verbatim_kingdom             |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | identified_by                |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | date_identified              |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | identification_certainty     |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | identification_qualifier     |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | identification_qualifier     |                                                            | string    |
    | _entire_name                 |                                                            |           |
    +------------------------------+------------------------------------------------------------+-----------+
    | identification_qualifier     |                                                            | string    |
    | _species_name                |                                                            |           |
    +------------------------------+------------------------------------------------------------+-----------+
    | identification_qualifier     |                                                            | string    |
    | _subspecies_name             |                                                            |           |
    +------------------------------+------------------------------------------------------------+-----------+
    | identification_remarks       |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | identification_verification  |                                                            | string    |
    | _status                      |                                                            |           |
    +------------------------------+------------------------------------------------------------+-----------+
    | type_status                  |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | inserted_ts_utc              |                                                            | timestamp |
    +------------------------------+------------------------------------------------------------+-----------+
    | updated_ts_utc               |                                                            | timestamp |
    +------------------------------+------------------------------------------------------------+-----------+

    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.dna.enriched,
            schema=StructType(
                [
                    StructField("identification_id", LongType(), True),
                    StructField("material_entity_golden_id", LongType(), True),
                    StructField("consensus_sequence_golden_id", LongType(), True),
                    StructField("catalog_number", StringType(), True),
                    StructField("date_identified", StringType(), True),
                    StructField("identification_certainty", StringType(), True),
                    StructField("identification_method", StringType(), True),
                    StructField("identification_qualifier", StringType(), True),
                    StructField("identification_qualifier_entire_name", StringType(), True),
                    StructField("identification_qualifier_species_name", StringType(), True),
                    StructField("identification_qualifier_subspecies_name", StringType(), True),
                    StructField("identification_remarks", StringType(), True),
                    StructField("identification_verification_status", StringType(), True),
                    StructField("identified_by", StringType(), True),
                    StructField("type_status", StringType(), True),
                    StructField("verbatim_identification", StringType(), True),
                    StructField("verbatim_identification_rank", StringType(), True),
                    StructField("verbatim_kingdom", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), True),
                    StructField("source_id", StringType(), True),
                    StructField("source", StringType(), True),
                ]
            ),
            logical_keys=["source_id", "source"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
            primary_key_columns=[f"{self.table_name}_id"],
        )
        self.id_table = IdTableGenerator(self.enriched_table)

    def run(self) -> None:
        """
        Runs the logic for the sequencing_run table. Loading, validating, transforming and writing the dataframe.
        """

        create_table_if_not_exists(
            self.spark,
            self.catalog,
            schema=self.enriched_table.schema,
            table_name=self.enriched_table.table_name,
            path=self.enriched_table.table_path,
            database=self.enriched_table.database,
        )

        # Merge the defined enriched StructType schema with the current schema in the Delta table
        merge_delta_schema(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            source_schema=self.enriched_table.schema,
        )

        # Load raw tables we need to create the identification enriched table
        raw_registrations = delta_to_df(self.spark, db="raw", table="registrations_validated")
        raw_sheets = delta_to_df(self.spark, db="raw", table="sheets_validated")

        # Validate, transform and write the bulk registrations
        valid_data, invalid_data = self._validate(raw_registrations)
        valid_sheet_data, invalid_sheet_data = self._validate(raw_sheets)

        registrations_transformed = self._transform_registrations(valid_data)
        registrations_transformed = add_timestamp_columns(registrations_transformed)
        registrations_transformed = registrations_transformed.withColumn("source", F.lit("ada"))

        sheets_transformed = self._transform_sheets(valid_sheet_data)
        sheets_transformed = add_timestamp_columns(sheets_transformed)
        sheets_transformed = sheets_transformed.withColumn("source", F.lit("sampling-sheet"))

        # Combine the transformed dataframes
        combined_dataframes = registrations_transformed.unionByName(sheets_transformed, allowMissingColumns=True)

        # Apply transformations specific to the enriched identification table
        combined_dataframes_transformed = self._transform_identification(combined_dataframes)

        # Add the material_entity_golden_id to the table
        material_entity = raw_sheets = delta_to_df(self.spark, db="enriched", table="material_entity")
        identification_with_relationships = combined_dataframes_transformed.join(
            material_entity.select("material_entity_golden_id", "catalog_number"),
            on=["catalog_number"],
            how="left",
        )

        identification_with_primary_keys = generate_primary_keys(
            df=identification_with_relationships,
            logical_keys=self.enriched_table.logical_keys,
            primary_key_column=self.enriched_table.primary_key_columns[0],
            existing_data=delta_to_df(self.spark, db="enriched", table=self.table_name),
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        upsert_to_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            df=identification_with_primary_keys,
            primary_key_columns=self.enriched_table.primary_key_columns,
            static_columns=self.enriched_table.static_columns,
            ignore_change_columns=self.enriched_table.ignore_change_columns,
        )

    def _transform_identification(self, identification: DataFrame) -> DataFrame:
        """
        Transforms the material entity dataframe with transformations not performed by
        the table registrations.
        """
        return identification.withColumn("identification_method", F.lit("Morphology"))

    def _transform_registrations(self, valid_data: DataFrame) -> DataFrame:
        """Transform the raw registrations dataframe into the enriched dataframe following
        the data-model for the material entity table.
        """
        valid_data = valid_data.select(
            F.col("Id").cast("string").alias("source_id"),
            F.col("ContainerCode").cast("string").alias("catalog_number"),
            F.col("Determinator_Givenname").cast("string").alias("determinator_given_name"),
            F.col("Determinator_Lastname").cast("string").alias("determinator_last_name"),
            F.col("Determinator_Preposition").cast("string").alias("determinator_preposition"),
            F.col("Uncertain").cast("string").alias("identification_certainty"),
            F.col("Verbatim_identification").cast("string").alias("verbatim_identification"),
            F.col("Verbatim_kingdom").cast("string").alias("verbatim_kingdom"),
            F.col("Verbatim_taxon_rank").cast("string").alias("verbatim_identification_rank"),
        )

        valid_data = valid_data.withColumn(
            "identified_by",
            F.concat(
                F.col("determinator_last_name"),
                F.lit(", "),
                F.col("determinator_given_name"),
                F.lit(" "),
                F.col("determinator_preposition"),
            ),
        ).drop(
            "determinator_last_name",
            "determinator_given_name",
            "determinator_preposition",
        )

        return valid_data

    def _transform_sheets(self, valid_data: DataFrame) -> DataFrame:
        """Transform the raw sheets dataframe into the enriched dataframe following
        the data-model for the material entity table.
        """
        # Fill the verbatim_identification_rank based on the provided rank
        valid_data = valid_data.withColumn(
            "verbatim_identification_rank",
            F.when(F.col("subspecies") != "", F.lit("subspecies"))
            .when(F.col("species") != "", F.lit("species"))
            .when(F.col("subgenus") != "", F.lit("subgenus"))
            .when(F.col("genus") != "", F.lit("genus"))
            .when(F.col("subfamily_name") != "", F.lit("subfamily"))
            .when(F.col("family_name") != "", F.lit("family"))
            .otherwise(""),
        )

        valid_data = valid_data.select(
            F.col("verbatim_identification_rank").cast("string"),
            F.col("qualifier_entire_name").cast("string").alias("identification_qualifier_entire_name"),
            F.col("qualifier_species").cast("string").alias("identification_qualifier_species_name"),
            F.col("qualifier_subspecies").cast("string").alias("identification_qualifier_subspecies_name"),
            F.col("full_name_no_authority").cast("string").alias("verbatim_identification"),
            F.col("registration_type").cast("string").alias("verbatim_kingdom"),
            F.col("type_status").cast("string").alias("type_status"),
            F.col("determination_name_comments").cast("string").alias("identification_remarks"),
            F.col("determination_identifier").cast("string").alias("identified_by"),
            F.col("identification_start_date").cast("string").alias("date_identified"),
            F.concat(F.col("registration_number"), F.coalesce(F.col("Suffix"), F.lit(""))).alias("source_id"),
            F.concat(F.col("registration_number"), F.coalesce(F.col("Suffix"), F.lit(""))).alias("catalog_number"),
        )

        return valid_data

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(lit(False))
