import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.types import LongType, StringType, StructField, StructType, TimestampType

from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    add_timestamp_and_source_columns,
    generate_primary_keys,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable, IdTableGenerator
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delta_to_df,
    merge_delta_schema,
    upsert_to_delta_table,
)


class SequencingRun:
    """
    **Description**

    The sequencing_run table contains information about the sequencing run.
    This table is used to track the sequencing

    **Available fields**

    +-----------------------------+--------------------------------------------------+----------+
    | NAME                        | DESCRIPTION                                      | TYPE     |
    +=============================+==================================================+==========+
    | sequencing_run_id            | ID of the sequencing_run [primary key]          | long     |
    +-----------------------------+--------------------------------------------------+----------+
    | amplicon_golden_id          | Golden ID of the amplicon                        | long     |
    +-----------------------------+--------------------------------------------------+----------+
    | source                      | The source of the sequencing run                 | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | source_id                   | Original identifier of the item in the source    | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | project_name                | Name of the project/experiment                   | string   |
    +-----------------------------+--------------------------------------------------+----------+
    | title                       | Name of the sample/pool input by the user        | string   |
    |                             | (MinKNOW) before running the sequencing.         |          |
    +-----------------------------+--------------------------------------------------+----------+
    | end_date_time               |                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+
    | legacy_created_datetime     |                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+
    | legacy_last_updated_datetime|                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+
    | inserted_ts_utc             |                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+
    | updated_ts_utc              |                                                  | timestamp|
    +-----------------------------+--------------------------------------------------+----------+


    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.dna.enriched,
            schema=StructType(
                [
                    StructField("sequencing_run_id", LongType(), True),
                    StructField("title", StringType(), True),
                    StructField("project_name", StringType(), True),
                    StructField("end_date_time", TimestampType(), True),
                    StructField("last_updated_source", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), True),
                    StructField("source_id", StringType(), True),
                    StructField("source", StringType(), True),
                ]
            ),
            logical_keys=["source_id", "source"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
            primary_key_columns=[f"{self.table_name}_id"],
        )
        self.id_table = IdTableGenerator(self.enriched_table)

    def run(self) -> None:
        """
        Runs the logic for the sequencing_run table. Loading, validating, transforming and writing the dataframe.
        """

        create_table_if_not_exists(
            self.spark,
            self.catalog,
            schema=self.enriched_table.schema,
            table_name=self.enriched_table.table_name,
            path=self.enriched_table.table_path,
            database=self.enriched_table.database,
        )

        # Merge the defined enriched StructType schema with the current schema in the Delta table
        merge_delta_schema(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            source_schema=self.enriched_table.schema,
        )

        raw_sequencing_run = delta_to_df(self.spark, db="raw", table="sequencing_runs_validated")
        valid_data, invalid_data = self._validate(raw_sequencing_run)

        sequencing_run_transformed = self._transform_columns(valid_data)
        sequencing_run_transformed = add_timestamp_and_source_columns(sequencing_run_transformed, self.source)

        sequencing_run_with_primary_keys = generate_primary_keys(
            df=sequencing_run_transformed,
            logical_keys=self.enriched_table.logical_keys,
            primary_key_column=self.enriched_table.primary_key_columns[0],
            existing_data=delta_to_df(self.spark, db="enriched", table=self.table_name),
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        upsert_to_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            df=sequencing_run_with_primary_keys,
            primary_key_columns=self.enriched_table.primary_key_columns,
            static_columns=self.enriched_table.static_columns,
            ignore_change_columns=self.enriched_table.ignore_change_columns,
        )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(F.lit(False))

    def _transform_columns(self, valid_data: DataFrame) -> DataFrame:
        """Transform Nanopore validated data df into enriched df following
        the data-model for the sequencing_run table.
        """
        return valid_data.select(
            F.col("title_alias").cast("string").alias("title"),
            F.col("experiment_name").cast("string").alias("project_name"),
            F.col("end_time").cast("timestamp").alias("end_date_time"),
            F.col("id").cast("string").alias("source_id"),
        )
