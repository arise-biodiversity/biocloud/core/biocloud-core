import logging

from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.csc.additional_number import AdditionalNumber
from biocloudcore.spark_session import get_environment, set_default_catalog, spark_session


def run():
    table_name = "additional_number"
    source_name = "sampling-sheet"

    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    catalog = set_default_catalog(spark=spark, domain="csc")
    data_lake = DataLake(env=get_environment(spark))

    AdditionalNumber(spark=spark, catalog=catalog, data_lake=data_lake, source=source_name, table_name=table_name).run()


if __name__ == "__main__":
    run()
