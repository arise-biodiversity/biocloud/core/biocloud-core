import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import (
    BooleanType,
    LongType,
    StringType,
    StructField,
    StructType,
    TimestampType,
)

from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    add_timestamp_and_source_columns,
    generate_enriched_dataframes_with_golden_ids,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable, IdTableGenerator
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delta_to_df,
    merge_delta_schema,
    upsert_to_delta_table,
)


class Amplicon:
    """
    **Description**

    The amplicon table contains information about the amplicons.
    This table is used to track the amplicons

    **Available fields**

    +---------------------+------------------------------------------------------------+-----------+
    | NAME                | DESCRIPTION                                                | TYPE      |
    +=====================+============================================================+===========+
    | amplicon_golden_id  |                                                            | long      |
    +---------------------+------------------------------------------------------------+-----------+
    | dna_extract         |                                                            | string    |
    | _golden_id          |                                                            |           |
    +---------------------+------------------------------------------------------------+-----------+
    | pcr_id              |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | marker              |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | dna_extract_id      |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | project_id          |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | is_control          |                                                            | boolean   |
    +---------------------+------------------------------------------------------------+-----------+
    | source_id           |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | sequencing_run_id   |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | prime_name_forward  |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | prime_name_reverse  |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | source              |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | last_updated_source |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | inserted_ts_utc     |                                                            | timestamp |
    +---------------------+------------------------------------------------------------+-----------+
    | updated_ts_utc      |                                                            | timestamp |
    +---------------------+------------------------------------------------------------+-----------+

    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.dna.enriched,
            schema=StructType(
                [
                    StructField("amplicon_golden_id", LongType(), True),
                    StructField("dna_extract_golden_id", LongType(), True),
                    StructField("sequencing_run_id", LongType(), True),
                    StructField("amplicon_id", StringType(), True),
                    StructField("pcr_id", StringType(), True),
                    StructField("marker", StringType(), True),
                    StructField("dna_extract_id", StringType(), True),
                    StructField("project_id", StringType(), True),
                    StructField("is_control", BooleanType(), True),
                    StructField("sequencing_run_id", StringType(), True),
                    StructField("prime_name_forward", StringType(), True),
                    StructField("prime_name_reverse", StringType(), True),
                    # StructField("sequence_generated_datetime", TimestampType(), True),
                    # StructField("legacy_last_updated_datetime", TimestampType(), True),
                    StructField("last_updated_source", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), False),
                ]
            ),
            logical_keys=["amplicon_id"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
        )
        self.id_table = IdTableGenerator(self.enriched_table)

    def run(self) -> None:
        """
        Runs the logic for the amplicon table.
        Loading, validating, transforming and writing the dataframe.
        """
        for table in [self.enriched_table, self.id_table]:
            create_table_if_not_exists(
                self.spark,
                self.catalog,
                schema=table.schema,
                table_name=table.table_name,
                path=table.table_path,
                database=table.database,
            )

        # Merge the defined enriched StructType schema with the current schema in the Delta table
        merge_delta_schema(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            source_schema=self.enriched_table.schema,
        )

        raw_amplicons = delta_to_df(self.spark, db="raw", table="amplicons_validated")
        valid_data, invalid_data = self._validate(raw_amplicons)

        amplicon_transformed = self._transform_columns(valid_data)
        amplicon_transformed = add_timestamp_and_source_columns(amplicon_transformed, self.source)

        # Add the dna_extract_golden_id and sequencing_run_id to the table
        dna_extract = delta_to_df(self.spark, db="enriched", table="dna_extract")
        sequencing_run = delta_to_df(self.spark, db="enriched", table="sequencing_run")

        amplicon_with_relationships = amplicon_transformed.join(
            dna_extract.select("dna_extract_golden_id", "dna_extract_id"),
            on="dna_extract_id",
            how="left",
        ).join(
            sequencing_run.select("sequencing_run_id", "sequencing_run_id"),
            on="sequencing_run_id",
            how="left",
        )

        amplicon_with_golden_ids = generate_enriched_dataframes_with_golden_ids(
            self.spark,
            id_table_path=self.id_table.table_path,
            df=amplicon_with_relationships,
            logical_keys=self.enriched_table.logical_keys,
            table_name=self.table_name,
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        for table in [self.enriched_table, self.id_table]:
            upsert_to_delta_table(
                self.spark,
                database=table.database,
                table_name=table.table_name,
                df=amplicon_with_golden_ids[table.table_name],
                primary_key_columns=table.primary_key_columns,
                static_columns=table.static_columns,
                ignore_change_columns=table.ignore_change_columns,
            )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(lit(False))

    def _transform_columns(self, valid_data: DataFrame) -> DataFrame:
        """Transform Nanopore validated data df into enriched df following
        the data-model for the amplicon table.
        """
        return valid_data.select(
            F.col("pcr_id").cast("string"),
            F.col("marker").cast("string"),
            F.col("project_id").cast("string"),
            F.col("is_control").cast("boolean"),
            F.col("id").cast("string").alias("source_id"),
            F.col("id").cast("string").alias("amplicon_id"),
            F.col("extract_id").cast("string").alias("dna_extract_id"),
            F.col("sequencing_run").cast("string").alias("sequencing_run_id"),
            F.col("forward_primer").cast("string").alias("prime_name_forward"),
            F.col("reverse_primer").cast("string").alias("prime_name_reverse"),
        )
