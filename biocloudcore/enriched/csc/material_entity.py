import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.types import (
    BooleanType,
    DecimalType,
    LongType,
    StringType,
    StructField,
    StructType,
    TimestampType,
)

from biocloudcore.common_dataframe_functions import add_timestamp_columns
from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    generate_enriched_dataframes_with_golden_ids,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable, IdTableGenerator
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delta_to_df,
    merge_delta_schema,
    upsert_to_delta_table,
)


class MaterialEntity:
    """
    **Description**

    The Material Entity table contains all information about specimen and trap registrations.

    **Available fields**

    +------------------------------+------------------------------------------------------------+-----------+
    | NAME                         | DESCRIPTION                                                | TYPE      |
    +==============================+============================================================+===========+
    | material_entity_golden_id    |                                                            | long      |
    +------------------------------+------------------------------------------------------------+-----------+
    | bait                         |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | basis_of_record              |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | catalog_number               |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | collection_code              |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | dataset_name                 |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | decimal_latitude             |                                                            | decimal   |
    +------------------------------+------------------------------------------------------------+-----------+
    | decimal_longitude            |                                                            | decimal   |
    +------------------------------+------------------------------------------------------------+-----------+
    | event_date_end               |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | event_date_start             |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | geodetic_datum               |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | habitat                      |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | identification_certainty     |                                                            | boolean   |
    +------------------------------+------------------------------------------------------------+-----------+
    | identified_by_id             |                                                            | long      |
    +------------------------------+------------------------------------------------------------+-----------+
    | identified_by                |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | inserted_ts_utc              |                                                            | timestamp |
    +------------------------------+------------------------------------------------------------+-----------+
    | updated_ts_utc               |                                                            | timestamp |
    +------------------------------+------------------------------------------------------------+-----------+
    | lifestage                    |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | location_remarks             |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | location                     |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | maximum_elevation_meters     |                                                            | long      |
    +------------------------------+------------------------------------------------------------+-----------+
    | minimum_elevation_meters     |                                                            | long      |
    +------------------------------+------------------------------------------------------------+-----------+
    | occurrence_remarks           |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | organism_remarks             |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | parent_id                    |                                                            | long      |
    +------------------------------+------------------------------------------------------------+-----------+
    | preservation                 |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | preserved_part               |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | record_number                |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | recorded_by_id               |                                                            | long      |
    +------------------------------+------------------------------------------------------------+-----------+
    | recorded_by                  |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | registration_status          |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | rights_holder_id             |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | sample_kit_number            |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | sample_plate_id              |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | sample_plate_position        |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | sampling_protocol            |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | sex                          |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | source_id                    |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | source                       |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | specimen_mount               |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+
    | subject                      |                                                            | string    |
    +------------------------------+------------------------------------------------------------+-----------+


    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.dna.enriched,
            schema=StructType(
                [
                    StructField("material_entity_golden_id", LongType(), True),
                    StructField("bait", StringType(), True),
                    StructField("basis_of_record", StringType(), True),
                    StructField("catalog_number", StringType(), True),
                    StructField("collection_code", StringType(), True),
                    StructField("dataset_name", StringType(), True),
                    StructField("decimal_latitude", DecimalType(scale=5), True),
                    StructField("decimal_longitude", DecimalType(scale=5), True),
                    StructField("event_date_end", StringType(), True),
                    StructField("event_date_start", StringType(), True),
                    StructField("geodetic_datum", StringType(), True),
                    StructField("habitat", StringType(), True),
                    StructField("identification_certainty", BooleanType(), True),
                    StructField("identified_by", StringType(), True),
                    StructField("identified_by_id", LongType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), True),
                    StructField("lifestage", StringType(), True),
                    StructField("location", StringType(), True),
                    StructField("location_remarks", StringType(), True),
                    StructField("maximum_elevation_meters", DecimalType(), True),
                    StructField("minimum_elevation_meters", DecimalType(), True),
                    StructField("occurrence_remarks", StringType(), True),
                    StructField("organism_remarks", StringType(), True),
                    StructField("parent_id", LongType(), True),
                    StructField("preservation", StringType(), True),
                    StructField("preserved_part", StringType(), True),
                    StructField("record_number", StringType(), True),
                    StructField("recorded_by", StringType(), True),
                    StructField("recorded_by_id", StringType(), True),
                    StructField("registration_status", StringType(), True),
                    StructField("rights_holder_id", StringType(), True),
                    StructField("sample_kit_number", StringType(), True),
                    StructField("sample_plate_id", StringType(), True),
                    StructField("sample_plate_position", StringType(), True),
                    StructField("sampling_protocol", StringType(), True),
                    StructField("sex", StringType(), True),
                    StructField("specimen_mount", StringType(), True),
                    StructField("subject", StringType(), True),
                    # Spreadsheet specific fields
                    StructField("coordinate_uncertainty_in_meters", StringType(), True),
                    StructField("country", StringType(), True),
                    StructField("crs_datagroup_description", StringType(), True),
                    StructField("disposition", StringType(), True),
                    StructField("event_date_verbatim", StringType(), True),
                    StructField("event_name", StringType(), True),
                    StructField("event_time_end", StringType(), True),
                    StructField("event_time_start", StringType(), True),
                    StructField("individual_count", LongType(), True),
                    StructField("individual_count_approximately", LongType(), True),
                    StructField("island", StringType(), True),
                    StructField("locality", StringType(), True),
                    StructField("locality_verbatim", StringType(), True),
                    StructField("material_sample_id", StringType(), True),
                    StructField("minimum_depth_meters", LongType(), True),
                    StructField("minimum_distance_above_surface_meters", LongType(), True),
                    StructField("place_name", StringType(), True),
                    StructField("place_type", StringType(), True),
                    StructField("preservative", StringType(), True),
                    StructField("state_province", StringType(), True),
                    StructField("station_number", StringType(), True),
                    StructField("synecology_associated_specimen_lifestage", StringType(), True),
                    StructField("synecology_associated_specimen_sex", StringType(), True),
                    StructField("synecology_associated_taxon_name", StringType(), True),
                    StructField("synecology_associated_taxon_type", StringType(), True),
                    StructField("synecology_comment", StringType(), True),
                    StructField("verbatim_coordinates", StringType(), True),
                ]
            ),
            logical_keys=["catalog_number"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
        )
        self.id_table = IdTableGenerator(self.enriched_table)

    def run(self) -> None:
        """
        Runs the logic for the sequencing_run table. Loading, validating, transforming and writing the dataframe.
        """

        for table in [self.enriched_table, self.id_table]:
            create_table_if_not_exists(
                self.spark,
                self.catalog,
                schema=table.schema,
                table_name=table.table_name,
                path=table.table_path,
                database=table.database,
            )

        # Merge the defined enriched StructType schema with the current schema in the Delta table
        merge_delta_schema(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            source_schema=self.enriched_table.schema,
        )

        # Load raw tables we need to create the material_entity enriched table
        raw_registrations = delta_to_df(self.spark, db="raw", table="registrations_validated")
        raw_sheets = delta_to_df(self.spark, db="raw", table="sheets_validated")

        # Validate, transform and write the bulk registrations
        valid_data, invalid_data = self._validate(raw_registrations)
        valid_sheet_data, invalid_sheet_data = self._validate(raw_sheets)

        registrations_transformed = self._transform_registrations(valid_data)
        registrations_transformed = add_timestamp_columns(registrations_transformed)
        registrations_transformed = registrations_transformed.withColumn("source", F.lit("ada"))

        sheets_transformed = self._transform_sheets(valid_sheet_data)
        sheets_transformed = add_timestamp_columns(sheets_transformed)
        # N.B. the source field from spreadsheets is called "sampling-sheet"
        sheets_transformed = sheets_transformed.withColumn("source", F.lit("sampling-sheet"))

        # Combine the transformed dataframes
        combined_dataframes = registrations_transformed.unionByName(sheets_transformed, allowMissingColumns=True)

        # Apply transformations specific to the enriched material entity table
        combined_dataframes_transformed = self._transform_material_entity(combined_dataframes)

        # We now have one dataframe which we need to seamlessly merge with the material_entity schema and its golden ids
        enriched_schema = delta_to_df(self.spark, db="enriched", table=self.table_name).schema
        enriched_schema_df = self.spark.createDataFrame([], enriched_schema)

        material_entity_with_golden_ids = generate_enriched_dataframes_with_golden_ids(
            self.spark,
            id_table_path=self.id_table.table_path,
            df=combined_dataframes_transformed,
            logical_keys=self.enriched_table.logical_keys,
            table_name=self.table_name,
        )

        material_entity_with_golden_ids[self.enriched_table.table_name] = enriched_schema_df.unionByName(
            material_entity_with_golden_ids[self.enriched_table.table_name], allowMissingColumns=True
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        for table in [self.enriched_table, self.id_table]:
            upsert_to_delta_table(
                self.spark,
                database=table.database,
                table_name=table.table_name,
                df=material_entity_with_golden_ids[table.table_name],
                primary_key_columns=table.primary_key_columns,
                static_columns=table.static_columns,
                ignore_change_columns=table.ignore_change_columns,
            )

    def _transform_material_entity(self, material_entity: DataFrame) -> DataFrame:
        """
        Transforms the material entity dataframe with transformations not performed by
        the table registrations.
        """
        return (
            material_entity.withColumn(
                # If both longitude and latitude are available in ADA, we will write
                # the string 'WGS84' to the geodetic datum field
                "geodetic_datum",
                F.when(
                    (F.col("decimal_longitude").isNotNull()) & (F.col("decimal_latitude").isNotNull()),
                    "WGS84",
                ).otherwise(None),
            )
            .withColumn(
                # Basis of record should have a darwinCore term.
                "basis_of_record",
                F.when((F.col("basis_of_record") == "Traps"), "MaterialSample")
                .when((F.col("basis_of_record").isin(["Unspecified", "Plantae", "Animalia"])), "PreservedSpecimen")
                .otherwise("Unknown"),
            )
            .withColumn(
                # If the recorded field is not yet populated, populate it via a concatenation of collector fields
                "recorded_by",
                F.when(
                    (F.col("recorded_by").isNull()),
                    F.concat(
                        F.coalesce(F.col("collector_last_name"), F.lit("")),
                        F.lit(", "),
                        F.coalesce(F.col("collector_given_name"), F.lit("")),
                        F.lit(" "),
                        F.coalesce(F.col("collector_preposition"), F.lit("")),
                    ),
                ).otherwise(F.col("recorded_by")),
            )
            .withColumn(
                "identified_by",
                F.when(
                    (F.col("determinator_last_name").isNotNull()),
                    F.concat(
                        F.coalesce(F.col("determinator_last_name"), F.lit("")),
                        F.lit(", "),
                        F.coalesce(F.col("determinator_given_name"), F.lit("")),
                        F.lit(" "),
                        F.coalesce(F.col("determinator_preposition"), F.lit("")),
                    ),
                ),
            )
            .drop(
                "collector_last_name",
                "collector_given_name",
                "collector_preposition",
                "determinator_last_name",
                "determinator_given_name",
                "determinator_preposition",
            )
        )

    def _transform_registrations(self, valid_data: DataFrame) -> DataFrame:
        """Transform the raw registrations dataframe into the enriched dataframe following
        the data-model for the material entity table.
        """
        # cast all date related values to timestamps
        valid_data = (
            valid_data.withColumn("TrapSet", F.col("TrapSet").cast("timestamp"))
            .withColumn("TrapEnd", F.col("TrapEnd").cast("timestamp"))
            .withColumn("Date", F.col("Date").cast("timestamp"))
        )
        # if TrapEnd is empty, we write the value of TrapSet to this column.
        # Later, we transform these fields to event_{...}_start and event_{...}_end
        valid_data = valid_data.withColumn(
            "TrapEnd",
            F.when(F.col("TrapEnd").isNull(), F.col("TrapSet").cast("timestamp")).otherwise(
                F.col("TrapEnd").cast("timestamp")
            ),
        )
        valid_data = valid_data.select(
            # If trap dates are available, fill event_start and event_end with these fields.
            # If not, we will fill these fields with the Date column
            F.when(F.col("TrapSet").isNotNull(), F.to_date("TrapSet", "dd/MM/yyyy"))
            .otherwise(F.to_date("Date", "dd/MM/yyyy"))
            .alias("event_date_start"),
            F.when(F.col("TrapEnd").isNotNull(), F.to_date("TrapEnd", "dd/MM/yyyy"))
            .otherwise(F.to_date("Date", "dd/MM/yyyy"))
            .alias("event_date_end"),
            F.when(F.col("TrapSet").isNotNull(), F.date_format("TrapSet", "HH:mm:ss"))
            .otherwise(F.date_format("Date", "HH:mm:ss"))
            .alias("event_time_start"),
            F.when(F.col("TrapEnd").isNotNull(), F.date_format("TrapEnd", "HH:mm:ss"))
            .otherwise(F.date_format("Date", "HH:mm:ss"))
            .alias("event_time_end"),
            F.col("Altitude").cast("decimal").alias("maximum_elevation_meters"),
            F.col("Altitude").cast("decimal").alias("minimum_elevation_meters"),
            F.col("Bait").cast("string").alias("bait"),
            F.col("Biotope_label").cast("string").alias("habitat"),
            F.col("Collector_Givenname").cast("string").alias("collector_given_name"),
            F.col("Collector_Lastname").cast("string").alias("collector_last_name"),
            F.col("Collector_Preposition").cast("string").alias("collector_preposition"),
            F.col("Collector_Serial").cast("string").alias("record_number"),
            F.col("ContainerCode").cast("string").alias("catalog_number"),
            F.col("Determinator_Givenname").cast("string").alias("determinator_given_name"),
            F.col("Determinator_Lastname").cast("string").alias("determinator_last_name"),
            F.col("Determinator_Preposition").cast("string").alias("determinator_preposition"),
            F.col("Groupname").cast("string").alias("collection_code"),
            F.col("Id").cast("string").alias("source_id"),
            F.col("Latitude").cast(DecimalType(scale=5)).alias("decimal_latitude"),
            F.col("Lifestage_label").cast("string").alias("lifestage"),
            F.col("Location").cast("string").alias("location"),
            F.col("LocationRemarks").cast("string").alias("location_remarks"),
            F.col("Longitude").cast(DecimalType(scale=5)).alias("decimal_longitude"),
            F.col("Mount_label").cast("string").alias("specimen_mount"),
            F.col("Preservation_label").cast("string").alias("preservation"),
            F.col("PreservedPart_label").cast("string").alias("preserved_part"),
            F.col("RegistrationStatus_label").cast("string").alias("registration_status"),
            F.col("RegistrationType_label").cast("string").alias("basis_of_record"),
            F.col("RegistrationType_label").cast("string").alias("dataset_name"),
            F.col("Remarks").cast("string").alias("occurrence_remarks"),
            F.col("SampleKitNumber").cast("string").alias("sample_kit_number"),
            F.col("Sexe_label").cast("string").alias("sex"),
            F.col("SpeciesRemarks").cast("string").alias("organism_remarks"),
            F.col("Tags").cast("string").alias("subject"),
            F.col("TrapType_label").cast("string").alias("sampling_protocol"),
            F.when(F.col("Uncertain").cast("string") == "true", True)
            .otherwise(False)
            .cast("boolean")
            .alias("identification_certainty"),
            F.col("UserId").cast("string").alias("recorded_by_id"),
        )

        return valid_data

    def _transform_sheets(self, valid_data: DataFrame) -> DataFrame:
        """Transform the raw sheets dataframe into the enriched dataframe following
        the data-model for the material entity table.
        """
        valid_data = valid_data.select(
            F.col("id").cast("string").alias("source_id"),
            F.col("recorded_by_id").cast("string").alias("recorded_by_id"),
            F.col("mount").cast("string").alias("specimen_mount"),
            F.col("preservation").cast("string").alias("preservation"),
            F.col("basis_of_record").cast("string").alias("basis_of_record"),
            F.col("preserved_part").cast("string").alias("preserved_part"),
            F.col("present_in_collection").cast("string").alias("disposition"),
            F.col("remarks").cast("string").alias("occurrence_remarks"),
            F.col("sheet_name").cast("string").alias("dataset_name"),
            F.col("count").cast("long").alias("individual_count"),
            F.col("count_approximate").cast("long").alias("individual_count_approximately"),
            F.col("sex").cast("string").alias("sex"),
            F.col("phase_or_stage").cast("string").alias("lifestage"),
            F.col("collector_name").cast("string").alias("recorded_by"),
            F.col("collecting_method").cast("string").alias("sampling_protocol"),
            F.col("collecting_date_string").cast("string").alias("event_date_verbatim"),
            F.col("gatheringsites_country").cast("string").alias("country"),
            F.col("gatheringsites_state_province").cast("string").alias("state_province"),
            F.col("gatheringsites_island").cast("string").alias("island"),
            F.col("gatheringsites_locality").cast("string").alias("locality"),
            F.col("gatheringsites_full_locality_text").cast("string").alias("locality_verbatim"),
            F.col("place_type").cast("string").alias("place_type"),
            F.col("place_name").cast("string").alias("place_name"),
            F.col("station_number").cast("string").alias("station_number"),
            F.col("sample_id").cast("string").alias("material_sample_id"),
            F.col("survey_name").cast("string").alias("event_name"),
            F.col("synecology_scientific_or_informal_name").cast("string").alias("synecology_associated_taxon_name"),
            F.col("synecology_sex").cast("string").alias("synecology_associated_specimen_sex"),
            F.col("synecology_determination_type").cast("string").alias("synecology_associated_taxon_type"),
            F.col("synecology_phase_or_stage").cast("string").alias("synecology_associated_specimen_lifestage"),
            F.col("synecology_comment").cast("string").alias("synecology_comment"),
            F.col("biotope_text").cast("string").alias("habitat"),
            F.col("verbatim_coordinates").cast("string").alias("verbatim_coordinates"),
            F.col("decimal_Latitude_WGS84").cast(DecimalType(scale=5)).alias("decimal_latitude"),
            F.col("decimal_Longitude_WGS84").cast(DecimalType(scale=5)).alias("decimal_longitude"),
            F.col("gatheringcoordinates_uncertainty_m").cast("string").alias("coordinate_uncertainty_in_meters"),
            F.col("altitude_meters").cast("decimal").alias("minimum_elevation_meters"),
            F.col("height_meters").cast("long").alias("minimum_distance_above_surface_meters"),
            F.col("depth_meters").cast("long").alias("minimum_depth_meters"),
            F.col("applied_preservative").cast("string").alias("preservative"),
            F.col("datagroup_description").cast("string").alias("crs_datagroup_description"),
            F.to_date("collecting_date_start", "dd/MM/yyyy").alias("event_date_start"),
            F.to_date("collecting_date_end", "dd/MM/yyyy").alias("event_date_end"),
            F.date_format("collecting_time_start", "HH:mm:ss").alias("event_time_start"),
            F.date_format("collecting_time_end", "HH:mm:ss").alias("event_time_end"),
            F.concat(F.col("registration_number"), F.coalesce(F.col("suffix"), F.lit(""))).alias("catalog_number"),
        )

        # Rename the disposition value to a DWC compatible value.
        valid_data = valid_data.withColumn(
            "disposition",
            F.when(F.col("disposition") == "yes", "in collection")
            .when(F.col("disposition") == "no", "not in collection")
            .otherwise(F.col("disposition")),
        )

        return valid_data

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(F.lit(False))
