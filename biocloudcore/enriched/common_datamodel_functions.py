import operator
from functools import reduce

import pyspark.sql.functions as F
from deprecated import deprecated
from pyspark.sql import DataFrame, SparkSession, Window


@deprecated(version="0.1.2", reason="Use add_timestamp_columns instead. This is the old version")
def add_timestamp_and_source_columns(df: DataFrame, source: str) -> DataFrame:
    """Add following columns which are the same for all entities.
    source                  string of source
    last_updated_source     string of source
    inserted_ts_utc         timestamp
    updated_ts_utc     timestamp
    """
    current_utc_timestamp = F.current_timestamp()

    transformed = df.select(
        *df.columns,  # Unpack all existing columns
        F.lit(source).alias("source"),
        F.lit(source).alias("last_updated_source"),
        F.lit(current_utc_timestamp).alias("inserted_ts_utc"),
        F.lit(current_utc_timestamp).alias("updated_ts_utc"),
    )
    return transformed


def generate_enriched_dataframes_with_golden_ids(
    spark: SparkSession,
    df: DataFrame,
    logical_keys: list,
    table_name: str,
    id_table_path: str = "",
) -> dict[str, DataFrame]:
    """
    Generates a dict with 2 dataframes:
    - one with the enriched data which includes the correct golden_ids, that can be tracked back in the ID table
    - and one with the ID table that contains the golden_ids and the logical keys to track back the golden_ids
    to the source dataset.

    A Golden ID is a unique system generated identifier assigned to each record to ensure that each record is
    uniquely identifiable within our Data Lake.

    For more info on Golden IDs, see the wiki page:
    https://gitlab.com/groups/arise-biodiversity/biocloud/-/wikis/4.-Data-model/4.2-Golden-records-&-ids

    Steps:
    1. Load the existing ID table and join it with the filtered new data based on the logical keys and source columns.
    2. Fill in the Golden IDs for rows with the same logical keys by checking if the previous row has the same
    logical keys.
    3. Assign new Golden IDs to rows with NULL Golden IDs by adding the maximum existing Golden ID to a row number
     generated for each row.
    4. Select the data in the correct schema to return as the ID table and the enriched table.

    Note:
    - The function assumes that the id_table is created and is a Delta table.
    - If the enriched table does not exist or does not contain any rows, the Golden IDs will start from 1.

    :param SparkSession spark: spark session
    :param DataFrame df: the input DataFrame to which Golden IDs will be added
    :param list logical_keys: A list of column names that constitute the logical keys for deduplication
    :param str table_name: The name of the table for which Golden IDs are being generated
    :param str id_table_path: the path to the existing id delta table TODO: use table_name in the future
    :return dict: A dict containing the enriched DataFrame and the id DataFrame as {table_name} and {table_name}_id
    """

    golden_id_column = f"{table_name}_golden_id"

    # Preprocess raw data to ensure there are no duplicate logical keys - source combinations and make sure
    # the most recent data is kept
    duplicate_raw_window = Window.partitionBy(*logical_keys, "source", "source_id").orderBy(
        F.col("inserted_ts_utc").desc()
    )
    filtered_df = (
        df.withColumn("row_number", F.row_number().over(duplicate_raw_window))
        .filter(F.col("row_number") == 1)
        .drop("row_number")
    )

    # TODO: change table_name instead of path in the future with a check if table exists as soon as a better Test
    #  solution is avaible in which we create a test catalog with the same structure as the real one
    # TODO: UPDATE -> see in generate_primary_keys, refactor once Nimbostratus is launched
    # Load the current existing id table
    id_table_df = spark.read.format("delta").load(id_table_path)

    # Join the existing id table with the new data that has already been prepped
    joined_id_df = id_table_df.join(filtered_df, on=["source_id", "source", *logical_keys], how="outer")

    # Generate the Golden IDs for the joined data
    data_with_golden_ids = generate_unique_ids(df=joined_id_df, logical_keys=logical_keys, id_column=golden_id_column)

    # With the golden ids filled in, we can now select the data to return as the ID table
    id_data = data_with_golden_ids.select(
        golden_id_column,
        "source",
        "source_id",
        *logical_keys,
        F.when(F.col("source_inserted_ts_utc").isNull(), F.col("inserted_ts_utc"))
        .otherwise(F.col("source_inserted_ts_utc"))
        .alias("source_inserted_ts_utc"),
        F.when(F.col("updated_ts_utc").isNotNull(), F.col("updated_ts_utc"))
        .otherwise(F.col("source_updated_ts_utc"))
        .alias("source_updated_ts_utc"),
    ).dropDuplicates([golden_id_column, "source", "source_id"])

    # Select the data to return as the enriched table
    enriched_data = (
        data_with_golden_ids.drop("source", "source_id", "source_inserted_ts_utc", "source_updated_ts_utc")
        .filter(F.col("updated_ts_utc").isNotNull())
        .dropDuplicates(logical_keys)
    )

    return {table_name: enriched_data, f"{table_name}_id": id_data}


def generate_primary_keys(
    df: DataFrame, logical_keys: list, primary_key_column: str, existing_data: DataFrame
) -> DataFrame:
    """
    Generates primary keys for a given DataFrame based on logical keys. This function is an alternative for the
    generate_enriched_dataframes_with_golden_ids function, when the primary keys are needed instead of the golden ids.
    This is for example the case relationship tables, transactional data tables or other tables that do not have a
    golden id because they need to use the original primary keys from the source data to have unique rows.

    This function ensures that each record in the DataFrame has a unique primary key. It first fills in the primary keys
    for rows with the same logical keys by checking if the previous row has the same logical keys. If not, it assigns
    new primary key to rows with NULL primary keys by adding the maximum existing primary key to a row number generated
    for each row.

    :param DataFrame df: The input DataFrame to which Golden IDs will be added.
    :param list logical_keys: A list of column names that constitute the logical keys for deduplication.
        This would typically be the source_id and the name of the source.
    :param str primary_key_column: The name of the column where the primary keys will be stored.
    :param str existing_data: A dataframe (usually a delta table) that contains the existing data, which we will upsert
    new or updated primary keys to.
    :return DataFrame: A DataFrame with the primary keys added to it.
    """
    # Preprocess raw data to ensure there are no duplicate logical keys, the most recent data is kept
    duplicate_raw_window = Window.partitionBy(*logical_keys).orderBy(F.col("inserted_ts_utc").desc())

    filtered_df = (
        df.withColumn("row_number", F.row_number().over(duplicate_raw_window))
        .filter(F.col("row_number") == 1)
        .drop("row_number")
    )

    # Union the existing data with the new data that has already been prepped
    union_df = existing_data.unionByName(other=filtered_df, allowMissingColumns=True)

    # Create a window to keep the most recent version of each primary key row
    duplicate_primary_key_window = Window.partitionBy(primary_key_column).orderBy(F.col("inserted_ts_utc").desc())

    # Generate the primary keys for the joined data and keep the newest version of each row
    return (
        generate_unique_ids(df=union_df, logical_keys=logical_keys, id_column=primary_key_column)
        # Keep the newest version of each primary key by filtering on the primary key window
        .withColumn("row_number", F.row_number().over(duplicate_primary_key_window))
        .filter(F.col("row_number") == 1)
        .drop("row_number")
    )


def generate_unique_ids(df: DataFrame, logical_keys: list, id_column: str) -> DataFrame:
    """
    Generates unique IDS for a given DataFrame based on logical keys.

    This function ensures that each record in the DataFrame has a unique Golden ID. It first fills in the Golden IDs
    for rows with the same logical keys by checking if the previous row has the same logical keys. If not, it assigns
    new Golden IDs to rows with NULL Golden IDs by adding the maximum existing Golden ID to a row number generated
    for each row.

    :param DataFrame df: The input DataFrame to which Golden IDs will be added.
    :param list logical_keys: A list of column names that constitute the logical keys for deduplication.
    :param str id_column: The name of the column where the Golden IDs will be stored.
    :return DataFrame: A DataFrame with the Golden IDs filled in.
    """

    # Create a window grouped by the logical keys and order by the logical keys and the golden id
    duplicate_window = Window.partitionBy(*logical_keys).orderBy(
        *[F.col(col).desc() for col in logical_keys], F.col(id_column).desc()
    )

    """
    With the window created above, we can now fill in the golden ids for the rows that have the same logical keys.
    We do this by checking if the previous row has the same logical keys and if so, we fill in the golden id of the
    previous row. If not, we keep the current value (which is NULL when new) and fill it later in this function.

    For Example (with the logical keys being ["name"]):
    INPUT DATA:                                       FILLED GOLDEN IDS:
    | name       | region | color     | golden_id |   | name       | region | color     | golden_id |
    |------------|--------|-----------|-----------|   |------------|--------|-----------|-----------|
    | Charmander | Kanto  | red       | 1         |   | Charmander | Kanto  | red       | 1         |
    | Charmander | Kanto  | red       | NULL      |   | Charmander | Kanto  | red       | 1         |
    | Pikachu    | Kanto  | yellow    | 2         |   | Pikachu    | Kanto  | yellow    | 2         |
    | Squirtle   | Kanto  | blue      | NULL      |   | Squirtle   | Kanto  | blue      | NULL      |
    """

    filled_golden_ids = df.withColumn(
        id_column,
        F.when(
            # The reduce function is used to create a logical AND between all the columns in the list
            # Here we check if the previous row has the same logical keys
            reduce(operator.and_, [F.lag(col).over(duplicate_window) == F.col(col) for col in logical_keys]),
            # If yes, take the previous row's id
            F.lag(id_column).over(duplicate_window),
        ).otherwise(
            # If not we just keep the current id (even if it is NULL)
            F.col(id_column)
        ),
    )

    # Create a window that groups the data in a way that the rows with NULL ids are ordered last
    id_window = Window.partitionBy(F.col(id_column).isNull()).orderBy(*logical_keys)

    # Get the current maximum golden id
    max_golden_id = filled_golden_ids.select(F.coalesce(F.max(id_column), F.lit(0)).alias("max_id")).first()["max_id"]

    """
    With the window created above and the max_golden_id, we can now fill in the NULL golden ids.
    We do this by starting to count from the max_golden_id and adding the row number to it for each row that has a NULL
    golden id.

    For Example (with the logical keys being ["name"] and the max_golden_id being 2):
    INPUT DATA:                                       FILLED GOLDEN IDS:
    | name       | region | color     | golden_id |   | name       | region | color     | golden_id |
    |------------|--------|-----------|-----------|   |------------|--------|-----------|-----------|
    | Charmander | Kanto  | red       | 1         |   | Charmander | Kanto  | red       | 1         |
    | Charmander | Kanto  | red       | 1         |   | Charmander | Kanto  | red       | 1         |
    | Pikachu    | Kanto  | yellow    | 2         |   | Pikachu    | Kanto  | yellow    | 2         |
    | Squirtle   | Kanto  | blue      | NULL      |   | Squirtle   | Kanto  | blue      | 3         |
    | Bulbasaur  | Kanto  | green     | NULL      |   | Bulbasaur  | Kanto  | green     | 4         |
    """

    return filled_golden_ids.withColumn(
        id_column,
        F.when(
            # Check if the golden id for the current row is NULL
            F.col(id_column).isNull(),
            # Create a new golden id.
            # The dense rank ensures that if the logical keys are the same in 2 rows that the golden id is the same.
            # This ensures 2 rows in _id table, but 1 row in the enriched.
            F.dense_rank().over(id_window) + max_golden_id,
        ).otherwise(
            # If the row already has a golden id, keep it
            F.col(id_column)
        ),
    ).drop("row_number")
