import datetime
import os
import uuid
from collections.abc import Callable
from pathlib import Path

import pytest
import time_machine
from pyspark.testing.utils import assertDataFrameEqual

import biocloudcore.enriched.tests.data.common_datamodel_functions_data as data
from biocloudcore.enriched.common_datamodel_functions import (
    generate_enriched_dataframes_with_golden_ids,
    generate_primary_keys,
)
from biocloudcore.utils.test_tools.test_dataset import TestDataset
from biocloudcore.utils.test_tools.test_functions import persist_datasets_to_delta


@pytest.fixture(scope="module")
def test_data_path(spark, tmp_path_factory) -> Callable[[str], str]:
    # We create a unique test path, so that spark cache will not be confused.
    # Folder will automatically be deleted after running the test.
    tmp_dir: Path = tmp_path_factory.mktemp(f"testdata_{uuid.uuid4()}")

    delta_datasets = [
        TestDataset(
            "existing_id_table",
            data.existing_id_table["INPUT_DATA"],
            data.existing_id_table["INPUT_SCHEMA"],
        ),
        TestDataset(
            "existing_pk_table",
            data.existing_pk_table["INPUT_DATA"],
            data.existing_pk_table["INPUT_SCHEMA"],
        ),
    ]

    persist_datasets_to_delta(spark, tmp_dir, delta_datasets)
    return lambda x: os.path.join(tmp_dir, x)


@pytest.fixture(scope="module")
def test_dataframes(spark):
    test_dfs = [
        TestDataset(
            "pokedex",
            data.pokedex_data["INPUT_DATA"],
            data.pokedex_data["INPUT_SCHEMA"],
        ),
        TestDataset(
            "wiki",
            data.wiki_data["INPUT_DATA"],
            data.wiki_data["INPUT_SCHEMA"],
        ),
    ]

    return {dataset.filename: spark.createDataFrame(dataset.data, dataset.schema) for dataset in test_dfs}


@time_machine.travel(datetime.datetime(2024, 1, 1, 0, 0, tzinfo=datetime.UTC))
def test_generate_enriched_dataframes_with_golden_ids(spark, test_data_path, test_dataframes, tmpdir):
    pokedex_df = test_dataframes["pokedex"]
    wiki_df = test_dataframes["wiki"]

    # First execute with the pokedex data
    golden_id_data_pokedex = generate_enriched_dataframes_with_golden_ids(
        spark,
        id_table_path=test_data_path("existing_id_table"),
        df=pokedex_df,
        logical_keys=["name"],
        table_name="pokemon",
    )

    """
    TODO: In the future we would like to do the actual upsert to a test_catalog but
    this depends on performance and the future implementation of our datalake
    """
    # For now just compare dataframes and not upserted output
    assertDataFrameEqual(
        actual=golden_id_data_pokedex["pokemon"],
        expected=data.expected_enriched_pokedex_output,
        includeDiffRows=True,
    )
    assertDataFrameEqual(
        actual=golden_id_data_pokedex["pokemon_id"], expected=data.expected_id_pokedex_output, includeDiffRows=True
    )

    # Overwrite the delta table with the output of the first execution
    golden_id_data_pokedex["pokemon_id"].write.format("delta").mode("overwrite").save(
        test_data_path("existing_id_table")
    )

    # Secondly execute it again with the output of the first execution and the wiki data
    end_result = generate_enriched_dataframes_with_golden_ids(
        spark,
        id_table_path=test_data_path("existing_id_table"),
        df=wiki_df,
        logical_keys=["name"],
        table_name="pokemon",
    )

    # For now just compare dataframes and not upserted output
    assertDataFrameEqual(
        actual=end_result["pokemon"], expected=data.expected_enriched_wiki_output, includeDiffRows=True
    )
    assertDataFrameEqual(actual=end_result["pokemon_id"], expected=data.expected_id_wiki_output, includeDiffRows=True)


@time_machine.travel(datetime.datetime(2024, 1, 1, 0, 0, tzinfo=datetime.UTC))
def test_generate_primary_keys(spark, test_data_path, test_dataframes, tmpdir):
    pokedex_df = test_dataframes["pokedex"]
    wiki_df = test_dataframes["wiki"]

    # First execute with the pokedex data
    primary_keys_pokedex = generate_primary_keys(
        df=pokedex_df,
        logical_keys=["source_id", "source"],
        primary_key_column="pokemon_id",
        existing_data=spark.read.format("delta").load(test_data_path("existing_pk_table")),
    )

    # Compare the new created dataframe with the expected output
    assertDataFrameEqual(
        actual=primary_keys_pokedex,
        expected=data.expected_pk_pokedex_output,
        includeDiffRows=True,
    )

    # Overwrite the delta table with the output of the first execution
    primary_keys_pokedex.write.format("delta").mode("overwrite").save(test_data_path("existing_pk_table"))

    # Secondly execute it again with the output of the first execution and the wiki data
    end_result = generate_primary_keys(
        df=wiki_df,
        logical_keys=["source_id", "source"],
        primary_key_column="pokemon_id",
        existing_data=spark.read.format("delta").load(test_data_path("existing_pk_table")),
    )

    # For now just compare dataframes and not upserted output
    assertDataFrameEqual(actual=end_result, expected=data.expected_pk_wiki_output, includeDiffRows=True)
