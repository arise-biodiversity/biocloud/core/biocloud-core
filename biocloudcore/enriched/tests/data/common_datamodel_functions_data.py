import datetime

from pyspark.sql.types import IntegerType, Row, StringType, StructField, StructType, TimestampType

pokedex_data = {
    "INPUT_SCHEMA": StructType(
        [
            StructField("name", StringType(), True),
            # StructField("type", StringType(), True),
            # StructField("lvl", IntegerType(), True),
            StructField("email", StringType(), True),
            StructField("source_id", StringType(), True),
            StructField("source", StringType(), True),
            StructField("inserted_ts_utc", TimestampType(), True),
            StructField("updated_ts_utc", TimestampType(), True),
        ]
    ),
    "INPUT_DATA": [
        {
            "name": "Pikachu",
            # 'type': 'Electric',
            # 'lvl': 99,
            "email": "pikachu@pokemon.com",  # This is the original email
            "source_id": "xxx1",
            "source": "pokedex",
            "inserted_ts_utc": datetime.datetime(2024, 1, 2),
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
        {
            "name": "Charmander",
            # 'type': 'Fire',
            # 'lvl': 5,
            "email": "charmander@pokemon.com",
            "source_id": "xxx2",
            "source": "pokedex",
            "inserted_ts_utc": datetime.datetime(2024, 1, 2),
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
        {
            "name": "Jigglypuff",
            # 'type': 'Normal/Fairy',
            # 'lvl': 25,
            "email": "jigglypuff@pokemon.com",
            "source_id": "xxx3",
            "source": "pokedex",
            "inserted_ts_utc": datetime.datetime(2024, 1, 2, 1),
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
        # IMPORTANT EDGE CASE: duplicate data in the raw table, nothing changed even same date
        {
            "name": "Jigglypuff",
            # 'type': 'Normal/Fairy',
            # 'lvl': 25,
            "email": "jigglypuff@pokemon.com",
            "source_id": "xxx3",
            "source": "pokedex",
            "inserted_ts_utc": datetime.datetime(2024, 1, 2, 1),
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
        # IMPORTANT EDGE CASE 2: duplicate data in the raw table, same date, BUT different source_id
        {
            "name": "Jigglypuff",
            # 'type': 'Normal/Fairy',
            # 'lvl': 25,
            "email": "jigglypuff@pokemon.com",
            "source_id": "different source_id should not appear in enriched! but should in enriched_id",
            "source": "pokedex",
            "inserted_ts_utc": datetime.datetime(2024, 1, 2, 2),
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
    ],
    "EXPECTED_ID_OUTPUT": [],
}

wiki_data = {
    "INPUT_SCHEMA": StructType(
        [
            StructField("name", StringType(), True),
            # StructField("region", StringType(), True),
            # StructField("color", StringType(), True),
            StructField("email", StringType(), True),
            StructField("source_id", StringType(), True),
            StructField("source", StringType(), True),
            StructField("inserted_ts_utc", TimestampType(), True),
            StructField("updated_ts_utc", TimestampType(), True),
        ]
    ),
    "INPUT_DATA": [
        {
            "name": "Pikachu",
            # 'region': 'Kanto',
            # 'color': "yellow",
            "email": "pikapika@naturalis.nl",  # This is the new email and should appear in the output
            "source_id": "id1",
            "source": "wiki",
            "inserted_ts_utc": datetime.datetime(2024, 1, 2),
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
        {
            "name": "Charmander",
            # 'region': 'Kanto',
            # 'color': "orange",
            "email": "charmander@pokemon.com",
            "source_id": "id2",
            "source": "wiki",
            "inserted_ts_utc": datetime.datetime(2024, 1, 2),
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
        {
            "name": "Snorlax",
            # 'region': 'Kanto',
            # 'color': "green",
            "email": "snorlax@pokemon.com",
            "source_id": "id5",
            "source": "wiki",
            "inserted_ts_utc": datetime.datetime(2024, 1, 2),
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
        # IMPORTANT EDGE CASE: duplicate data in the raw table, but one is more recent than the other
        {
            "name": "Snorlax",
            # 'region': 'Kanto',
            # 'color': "green",
            "email": "snorlax_this_email_should_be_the_result@pokemon.com",  # updated email should appear in result
            "source_id": "id5",
            "source": "wiki",
            "inserted_ts_utc": datetime.datetime(2024, 1, 3),
            "updated_ts_utc": datetime.datetime(2024, 1, 3),
        },
    ],
}


existing_id_table = {
    "INPUT_SCHEMA": StructType(
        [
            StructField("pokemon_golden_id", IntegerType(), True),
            StructField("source_id", StringType(), True),
            StructField("source", StringType(), True),
            StructField("name", StringType(), True),
            StructField("source_inserted_ts_utc", TimestampType(), True),
            StructField("source_updated_ts_utc", TimestampType(), True),
        ]
    ),
    "INPUT_DATA": [
        {
            "pokemon_golden_id": 1,
            "source_id": "id1",
            "source": "wiki",
            "name": "Pikachu",
            "source_inserted_ts_utc": datetime.datetime(2024, 1, 1),
            "source_updated_ts_utc": datetime.datetime(2024, 1, 1),
        }
    ],
}

existing_pk_table = {
    "INPUT_SCHEMA": StructType(
        [
            StructField("pokemon_id", IntegerType(), True),
            StructField("source_id", StringType(), True),
            StructField("source", StringType(), True),
            StructField("name", StringType(), True),
            StructField("email", StringType(), True),
            StructField("inserted_ts_utc", TimestampType(), True),
            StructField("updated_ts_utc", TimestampType(), True),
        ]
    ),
    "INPUT_DATA": [
        {
            "pokemon_id": 1,
            "source_id": "xxx1",
            "source": "pokedex",
            "name": "Pikachu",
            "email": "pikachu@pokemon.com",
            "inserted_ts_utc": datetime.datetime(2024, 1, 1),
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
        {
            "pokemon_id": 2,
            "source_id": "xxx2",  # same source_id as in pokedex data
            "source": "pokedex",
            "name": "Charmander",
            "email": "OLD_EMAIL_SHOULD_BE_UPDATED@pokemon.com",  # This email should be overwritten
            "inserted_ts_utc": datetime.datetime(2024, 1, 1),  # one day older data
            "updated_ts_utc": datetime.datetime(2024, 1, 2),
        },
    ],
}


# Pokedex resulsts

expected_id_pokedex_output = [
    Row(
        pokemon_golden_id=1,
        source="pokedex",
        source_id="xxx1",
        name="Pikachu",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_golden_id=1,
        source="wiki",
        source_id="id1",
        name="Pikachu",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 1, 0, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 1, 0, 0),
    ),
    Row(
        pokemon_golden_id=2,
        source="pokedex",
        source_id="xxx2",
        name="Charmander",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_golden_id=3,
        source="pokedex",
        source_id="xxx3",
        name="Jigglypuff",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 2, 1, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_golden_id=3,
        source="pokedex",
        source_id="different source_id should not appear in enriched! but should in enriched_id",
        name="Jigglypuff",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 2, 2, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
]


expected_enriched_pokedex_output = [
    Row(
        name="Charmander",
        pokemon_golden_id=2,
        email="charmander@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        name="Jigglypuff",
        pokemon_golden_id=3,
        email="jigglypuff@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 2, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        name="Pikachu",
        pokemon_golden_id=1,
        email="pikachu@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
]

# Wiki results
expected_id_wiki_output = [
    Row(
        pokemon_golden_id=1,
        source="pokedex",
        source_id="xxx1",
        name="Pikachu",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_golden_id=1,
        source="wiki",
        source_id="id1",
        name="Pikachu",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 1, 0, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_golden_id=2,
        source="pokedex",
        source_id="xxx2",
        name="Charmander",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_golden_id=2,
        source="wiki",
        source_id="id2",
        name="Charmander",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_golden_id=3,
        source="pokedex",
        source_id="xxx3",
        name="Jigglypuff",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 2, 1, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_golden_id=3,
        source="pokedex",
        source_id="different source_id should not appear in enriched! but should in enriched_id",
        name="Jigglypuff",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 2, 2, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_golden_id=4,
        source="wiki",
        source_id="id5",
        name="Snorlax",
        source_inserted_ts_utc=datetime.datetime(2024, 1, 3, 0, 0),
        source_updated_ts_utc=datetime.datetime(2024, 1, 3, 0, 0),
    ),
]


expected_enriched_wiki_output = [
    Row(
        name="Charmander",
        pokemon_golden_id=2,
        email="charmander@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        name="Pikachu",
        pokemon_golden_id=1,
        email="pikapika@naturalis.nl",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        name="Snorlax",
        pokemon_golden_id=4,
        email="snorlax_this_email_should_be_the_result@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 3, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 3, 0, 0),
    ),
]

expected_pk_pokedex_output = [
    Row(
        pokemon_id=2,
        source_id="xxx2",
        source="pokedex",
        name="Charmander",
        email="charmander@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_id=4,
        source_id="xxx3",
        source="pokedex",
        name="Jigglypuff",
        email="jigglypuff@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 1, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_id=1,
        source_id="xxx1",
        source="pokedex",
        name="Pikachu",
        email="pikachu@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        # This is the edge case where in the Primary Key approach this row should be included, while in the golden_id
        # approach it should not be included.
        pokemon_id=3,
        source_id="different source_id should not appear in enriched! but should in enriched_id",
        source="pokedex",
        name="Jigglypuff",
        email="jigglypuff@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 2, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
]


expected_pk_wiki_output = [
    Row(
        pokemon_id=2,
        source_id="xxx2",
        source="pokedex",
        name="Charmander",
        email="charmander@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_id=4,
        source_id="xxx3",
        source="pokedex",
        name="Jigglypuff",
        email="jigglypuff@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 1, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_id=1,
        source_id="xxx1",
        source="pokedex",
        name="Pikachu",
        email="pikachu@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        # This is the edge case where in the Primary Key approach this row should be included, while in the golden_id
        # approach it should not be included.
        pokemon_id=3,
        source_id="different source_id should not appear in enriched! but should in enriched_id",
        source="pokedex",
        name="Jigglypuff",
        email="jigglypuff@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 2, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    # Wiki rows will all just be add and not updated in the pokedex rows
    Row(
        pokemon_id=5,
        source_id="id1",
        source="wiki",
        name="Pikachu",
        email="pikapika@naturalis.nl",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        pokemon_id=6,
        source_id="id2",
        source="wiki",
        name="Charmander",
        email="charmander@pokemon.com",
        inserted_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 2, 0, 0),
    ),
    Row(
        # Important EDGE CASE: duplicate data in the raw table, but one is more recent than the other so
        # the email should be updated
        pokemon_id=7,
        source_id="id5",
        source="wiki",
        name="Snorlax",
        email="snorlax_this_email_should_be_the_result@pokemon.com",
        # Although the inserted_ts_utc is updated, we will keep the old inserted_ts because of the way our upsert
        # function works to Delta Table works (which is exactly what we want).
        inserted_ts_utc=datetime.datetime(2024, 1, 3, 0, 0),
        updated_ts_utc=datetime.datetime(2024, 1, 3, 0, 0),
    ),
]
