import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import (
    LongType,
    StructField,
    StructType,
)

from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delta_to_df,
    upsert_to_delta_table,
)


class DeploymentProject:
    """
    **Description**

    The deployment table contains information about the deployments.
    This table is used to track the deployments

    **Available fields**

    +---------------------+------------------------------------------------------------+-----------+
    | NAME                | DESCRIPTION                                                | TYPE      |
    +=====================+============================================================+===========+
    | deployment_project  |                                                            | string    |
    | _golden_id          |                                                            |           |
    +---------------------+------------------------------------------------------------+-----------+
    | deployment_golden_id|                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | project_golden_id   |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+

    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.nimbo_diopsis.enriched,
            schema=StructType(
                [
                    StructField("deployment_id", LongType(), True),
                    StructField("project_id", LongType(), True),
                ]
            ),
            logical_keys=["deployment_id", "project_id"],
            primary_key_columns=["deployment_id", "project_id"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
        )

    def run(self) -> None:
        """
        Runs the logic for the deployment table.
        Loading, validating, transforming and writing the dataframe.
        """
        create_table_if_not_exists(
            self.spark,
            self.catalog,
            schema=self.enriched_table.schema,
            table_name=self.enriched_table.table_name,
            path=self.enriched_table.table_path,
            database=self.enriched_table.database,
        )

        raw_deployments = (
            delta_to_df(self.spark, db="raw", table="deployments_validated")
            .select(
                F.col("id").cast("string").alias("source_id"),
                F.col("research_id").cast("string").alias("source_id_project"),
                F.lit("diopsis").alias("source"),
            )
            .dropDuplicates()
        )

        deployment_table = (
            delta_to_df(self.spark, db="enriched", table="deployment")
            .select("deployment_id", "source_id", "source")
            .dropDuplicates()
        )

        deployment_with_project_id = deployment_table.join(
            raw_deployments,
            on=["source_id", "source"],
            how="inner",
        )

        valid_data, invalid_data = self._validate(deployment_with_project_id)

        # join the project_golden_id to the deployment table based on the raw relationship
        project_table = delta_to_df(self.spark, db="enriched", table="project")

        deployment_project_table = valid_data.join(
            project_table.select("project_id", "source", F.col("source_id").alias("source_id_project")),
            on=["source_id_project", "source"],
            how="inner",
        )

        deployment_project_table = self._transform_columns(deployment_project_table).dropDuplicates()

        upsert_to_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            df=deployment_project_table,
            primary_key_columns=self.enriched_table.primary_key_columns,
            static_columns=self.enriched_table.static_columns,
            ignore_change_columns=self.enriched_table.ignore_change_columns,
        )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(lit(False))

    def _transform_columns(self, valid_data: DataFrame) -> DataFrame:
        """Transform Nanopore validated data df into enriched df following
        the data-model for the deployment table.
        """
        return valid_data.select(
            F.col("project_id").cast("long"),
            F.col("deployment_id").cast("long"),
        )
