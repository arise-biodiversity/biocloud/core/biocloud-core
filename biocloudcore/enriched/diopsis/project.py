import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import DateType, LongType, StringType, StructField, StructType, TimestampType

from biocloudcore.common_dataframe_functions import add_timestamp_columns
from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    generate_primary_keys,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delete_rows_from_delta_table,
    delta_to_df,
    upsert_to_delta_table,
)


class Project:
    """
    **Description**

    The project table contains information about the projects.
    This table is used to track the projects

    **Available fields**

    +---------------------+------------------------------------------------------------+-----------+
    | NAME                | DESCRIPTION                                                | TYPE      |
    +=====================+============================================================+===========+
    | project_golden_id   |                                                            | long      |
    +---------------------+------------------------------------------------------------+-----------+
    | name                |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | description         |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | start_date          |                                                            | datetime  |
    +---------------------+------------------------------------------------------------+-----------+
    | end_date            |                                                            | datetime  |
    +---------------------+------------------------------------------------------------+-----------+
    | country_code        |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | source_id           |                                                            | long      |
    +---------------------+------------------------------------------------------------+-----------+
    | source              |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | last_updated_source |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | inserted_ts_utc     |                                                            | timestamp |
    +---------------------+------------------------------------------------------------+-----------+
    | updated_ts_utc |                                                            | timestamp |
    +---------------------+------------------------------------------------------------+-----------+

    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=data_lake.nimbo_diopsis.enriched,
            schema=StructType(
                [
                    StructField("project_id", LongType(), True),
                    StructField("name", StringType(), True),
                    StructField("description", StringType(), True),
                    StructField("source_id", StringType(), True),
                    StructField("source", StringType(), True),
                    StructField("start_date", DateType(), True),
                    StructField("end_date", DateType(), False),
                    StructField("country_code", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), False),
                ]
            ),
            logical_keys=["source_id", "source"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
            primary_key_columns=[f"{self.table_name}_id"],
        )

    def run(self) -> None:
        """
        Runs the logic for the project table.
        Loading, validating, transforming and writing the dataframe.
        """
        create_table_if_not_exists(
            self.spark,
            self.catalog,
            schema=self.enriched_table.schema,
            table_name=self.enriched_table.table_name,
            path=self.enriched_table.table_path,
            database=self.enriched_table.database,
        )

        raw_projects = delta_to_df(self.spark, db="raw", table="projects_validated")

        # Filter deleted_at to prevent soft deleted records from always being recreated in enriched
        raw_projects_without_deleted = raw_projects.filter(F.col("deleted_at").isNull())

        valid_data, invalid_data = self._validate(raw_projects_without_deleted)

        project_transformed = self._transform_columns(valid_data)
        project_transformed = add_timestamp_columns(project_transformed).withColumnRenamed(
            "updated_ts_utc", "updated_ts_utc"
        )

        project_with_primary_keys = generate_primary_keys(
            df=project_transformed,
            logical_keys=self.enriched_table.logical_keys,
            primary_key_column=self.enriched_table.primary_key_columns[0],
            existing_data=delta_to_df(self.spark, db="enriched", table=self.table_name),
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        upsert_to_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            df=project_with_primary_keys,
            primary_key_columns=self.enriched_table.primary_key_columns,
            static_columns=self.enriched_table.static_columns,
            ignore_change_columns=self.enriched_table.ignore_change_columns,
        )

        # Delete the soft_deleted records from raw in enriched
        delete_rows_from_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            logical_key_columns=self.enriched_table.logical_keys,
            rows_to_match=(
                raw_projects.filter(F.col("deleted_at").isNotNull())
                .select(F.col("id").alias("source_id"), F.lit(self.source).alias("source"))
                .dropDuplicates()
            ),
        )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(lit(False))

    def _transform_columns(self, valid_data: DataFrame) -> DataFrame:
        """Transform Nanopore validated data df into enriched df following
        the data-model for the project table.
        """
        return valid_data.select(
            F.col("id").cast("long").alias("source_id"),
            F.col("name").cast("string"),
            F.col("description").cast("string"),
            F.to_date("start_date").alias("start_date"),
            F.to_date("end_date").alias("end_date"),
            F.lit(None).cast("string").alias("country_code"),
            F.lit(self.source).alias("source"),
        )
