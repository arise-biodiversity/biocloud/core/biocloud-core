import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import (
    LongType,
    StringType,
    StructField,
    StructType,
    TimestampType,
)

from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    add_timestamp_and_source_columns,
    generate_enriched_dataframes_with_golden_ids,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable, IdTableGenerator
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delete_rows_from_delta_table,
    delta_to_df,
    upsert_to_delta_table,
)


class Sensor:
    """
    **Description**

    The sensor table contains information about the sensors.
    This table is used to track the sensors

    **Available fields**

    +---------------------+------------------------------------------------------------+-----------+
    | NAME                | DESCRIPTION                                                | TYPE      |
    +=====================+============================================================+===========+
    | sensor_golden_id    |                                                            | long      |
    +---------------------+------------------------------------------------------------+-----------+
    | name                |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | type                |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | model               |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | make                |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | source_id           |                                                            | long      |
    +---------------------+------------------------------------------------------------+-----------+
    | source              |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | last_updated_source |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | inserted_ts_utc     |                                                            | timestamp |
    +---------------------+------------------------------------------------------------+-----------+
    | updated_ts_utc |                                                            | timestamp |
    +---------------------+------------------------------------------------------------+-----------+

    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.nimbo_diopsis.enriched,
            schema=StructType(
                [
                    StructField("sensor_golden_id", LongType(), True),
                    StructField("name", StringType(), True),
                    StructField("type", StringType(), True),
                    StructField("model", StringType(), True),
                    StructField("make", StringType(), True),
                    StructField("last_updated_source", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), False),
                ]
            ),
            logical_keys=["name"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
        )
        self.id_table = IdTableGenerator(self.enriched_table)

        self.data_lake = data_lake

    def run(self) -> None:
        """
        Runs the logic for the sensor table.
        Loading, validating, transforming and writing the dataframe.
        """
        for table in [self.enriched_table, self.id_table]:
            create_table_if_not_exists(
                self.spark,
                self.catalog,
                schema=table.schema,
                table_name=table.table_name,
                path=table.table_path,
                database=table.database,
            )

        raw_sensors = delta_to_df(self.spark, db="raw", table="devices_validated")

        # Filter deleted_at to prevent soft deleted records from always being recreated in enriched
        raw_sensors_without_deleted = raw_sensors.filter(F.col("deleted_at").isNull())

        valid_data, invalid_data = self._validate(raw_sensors_without_deleted)

        sensor_transformed = self._transform_columns(valid_data)
        sensor_transformed = add_timestamp_and_source_columns(sensor_transformed, self.source)

        sensor_with_golden_ids = generate_enriched_dataframes_with_golden_ids(
            self.spark,
            id_table_path=self.id_table.table_path,
            df=sensor_transformed,
            logical_keys=self.enriched_table.logical_keys,
            table_name=self.table_name,
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        for table in [self.enriched_table, self.id_table]:
            upsert_to_delta_table(
                self.spark,
                database=table.database,
                table_name=table.table_name,
                df=sensor_with_golden_ids[table.table_name],
                primary_key_columns=table.primary_key_columns,
                static_columns=table.static_columns,
                ignore_change_columns=table.ignore_change_columns,
            )

        # Delete the soft_deleted records from raw in the enriched id_table
        delete_rows_from_delta_table(
            self.spark,
            database=self.id_table.database,
            table_name=self.id_table.table_name,
            logical_key_columns=["source_id", "source"],
            rows_to_match=(
                raw_sensors.filter(F.col("deleted_at").isNotNull())
                .select(F.col("id").alias("source_id"), F.lit(self.source).alias("source"))
                .dropDuplicates()
            ),
        )

        # Delete all rows in enriched (using golden_id) that do not appear in the id_table anymore
        delete_rows_from_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            logical_key_columns=self.enriched_table.primary_key_columns,
            rows_to_match=(
                delta_to_df(self.spark, db="enriched_id", table="sensor_id")
                .select(*self.id_table.primary_key_columns)
                .dropDuplicates()
            ),
            inverted_match=True,
        )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(lit(False))

    def _transform_columns(self, valid_data: DataFrame) -> DataFrame:
        """Transform Nanopore validated data df into enriched df following
        the data-model for the sensor table.
        """
        return valid_data.select(
            F.col("id").cast("string").alias("source_id"),
            F.col("name").cast("string").alias("name"),
            F.col("style").cast("string").alias("model"),
            # F.col("created_at"),
            # F.col("deleted_at"),
            F.lit("insect_camera").cast("string").alias("type"),
            F.lit(None).cast("string").alias("make"),
        )
