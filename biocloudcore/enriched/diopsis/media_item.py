import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import BooleanType, LongType, StringType, StructField, StructType, TimestampType

from biocloudcore.common_dataframe_functions import add_timestamp_columns
from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    generate_primary_keys,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delta_to_df,
    upsert_to_delta_table,
)


class MediaItem:
    """
    **Description**

    The location table contains information about the locations.
    This table is used to track the locations

    **Available fields**

    +---------------------+------------------------------------------------------------+-----------+
    | NAME                | DESCRIPTION                                                | TYPE      |
    +=====================+============================================================+===========+
    | media_item_golden_id|                                                            | long      |
    +---------------------+------------------------------------------------------------+-----------+

    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;
    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.nimbo_diopsis.enriched,
            schema=StructType(
                [
                    StructField("media_item_id", LongType(), True),
                    StructField("sensor_golden_id", LongType(), True),
                    StructField("sensor_name", StringType(), True),
                    StructField("deployment_id", LongType(), True),
                    StructField("deployment_name", StringType(), True),
                    StructField("uri", StringType(), True),
                    StructField("capture_ts_utc", TimestampType(), True),
                    StructField("kind", StringType(), True),
                    StructField("mime_type", StringType(), True),
                    StructField("upload_success", BooleanType(), True),
                    StructField("last_updated_source", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), False),
                ]
            ),
            logical_keys=["uri"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
            primary_key_columns=[f"{self.table_name}_id"],
        )

    def run(self) -> None:
        """
        Runs the logic for the location table.
        Loading, validating, transforming and writing the dataframe.
        """
        create_table_if_not_exists(
            self.spark,
            self.catalog,
            schema=self.enriched_table.schema,
            table_name=self.enriched_table.table_name,
            path=self.enriched_table.table_path,
            database=self.enriched_table.database,
        )

        raw_media_item = delta_to_df(self.spark, db="raw", table="sensor_media_items_validated")
        valid_data, invalid_data = self._validate(raw_media_item)

        media_item_transformed = self._transform_columns(valid_data)
        media_item_transformed = (
            add_timestamp_columns(media_item_transformed)
            .withColumn("last_updated_source", lit(self.source))
            .withColumnRenamed("updated_ts_utc", "updated_ts_utc")
        )

        # Add the sensor golden id and deployment_id to the transformed data
        sensor = delta_to_df(self.spark, db="enriched", table="sensor")
        deployment = delta_to_df(self.spark, db="enriched", table="deployment")

        media_item_with_sensor = media_item_transformed.join(
            sensor.select("sensor_golden_id", F.col("name").alias("sensor_name")), on="sensor_name", how="left"
        )

        media_item_with_relationships = (
            media_item_with_sensor.alias("media_item")
            .join(
                deployment,
                (F.col("media_item.sensor_golden_id") == F.col("deployment.sensor_golden_id"))
                & (F.col("capture_ts_utc").between(F.col("start_ts_utc"), F.col("end_ts_utc"))),
                how="left",
            )
            .select("media_item.*", "deployment.deployment_id", F.col("deployment.name").alias("deployment_name"))
        )

        media_item_with_primary_keys = generate_primary_keys(
            df=media_item_with_relationships,
            logical_keys=self.enriched_table.logical_keys,
            primary_key_column=self.enriched_table.primary_key_columns[0],
            existing_data=delta_to_df(self.spark, db="enriched", table=self.table_name),
        )

        # Upsert the id and the enriched table to the Databricks Catalog
        upsert_to_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            df=media_item_with_primary_keys,
            primary_key_columns=self.enriched_table.primary_key_columns,
            static_columns=self.enriched_table.static_columns,
            ignore_change_columns=self.enriched_table.ignore_change_columns,
        )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(lit(False))

    def _transform_columns(self, valid_data: DataFrame) -> DataFrame:
        """Transform Nanopore validated data df into enriched df following
        the data-model for the location table.
        """
        return valid_data.select(
            "mime_type",
            "upload_success",
            F.col("device_name").cast("string").alias("sensor_name"),
            F.concat(
                F.lit(f"s3://{self.data_lake.nimbo_diopsis.blob.get_bucket_name()}/"), F.col("blob_path").cast("string")
            ).alias("uri"),
            F.col("capture_datetime").cast("timestamp").alias("capture_ts_utc"),
            F.lit("image").alias("kind"),
        )
