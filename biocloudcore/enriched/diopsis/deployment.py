import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import BooleanType, DecimalType, LongType, StringType, StructField, StructType, TimestampType

from biocloudcore.common_dataframe_functions import add_timestamp_columns
from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.common_datamodel_functions import (
    generate_primary_keys,
)
from biocloudcore.enriched.enriched_delta_table import EnrichedDeltaTable
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    delete_rows_from_delta_table,
    delta_to_df,
    upsert_to_delta_table,
)


class Deployment:
    """
    **Description**

    The deployment table contains information about the deployments.
    This table is used to track the deployments

    **Available fields**

    +---------------------+------------------------------------------------------------+-----------+
    | NAME                | DESCRIPTION                                                | TYPE      |
    +=====================+============================================================+===========+
    | deployment_golden_id|                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | sensor_golden_id    |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | source_id           |                                                            | int       |
    +---------------------+------------------------------------------------------------+-----------+
    | source              |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | name                |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | start_ts_utc        |                                                            | timestamp |
    +---------------------+------------------------------------------------------------+-----------+
    | end_ts_utc          |                                                            | timestamp |
    +---------------------+------------------------------------------------------------+-----------+
    | sensor_orientation  |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | screen_orientation  |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | sensor_height       |                                                            | string    |
    +---------------------+------------------------------------------------------------+-----------+
    | bait                |                                                            | boolean   |
    +---------------------+------------------------------------------------------------+-----------+

    **Schematic overview**

    .. uml::

    :raw_delta
    **path/to/raw/nanopore**;

    :enriched_delta
    **path/to/enriched/nanopore**;

    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, source: str, table_name: str
    ) -> None:
        self.source = source
        self.table_name = table_name
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            table_stage=self.data_lake.nimbo_diopsis.enriched,
            schema=StructType(
                [
                    StructField("deployment_id", LongType(), True),
                    StructField("sensor_golden_id", LongType(), True),
                    StructField("sensor_name", StringType(), True),
                    StructField("name", StringType(), True),
                    StructField("latitude", DecimalType(8, 5), True),
                    StructField("longitude", DecimalType(8, 5), True),
                    StructField("start_ts_utc", TimestampType(), True),
                    StructField("end_ts_utc", TimestampType(), True),
                    StructField("sensor_orientation", StringType(), True),
                    StructField("screen_orientation", StringType(), False),
                    StructField("sensor_height", DecimalType(10, 5), True),
                    StructField("bait", BooleanType(), True),
                    StructField("source_id", StringType(), True),
                    StructField("source", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), False),
                ]
            ),
            logical_keys=["source_id", "source"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
            primary_key_columns=[f"{self.table_name}_id"],
        )

    def run(self) -> None:
        """
        Runs the logic for the deployment table.
        Loading, validating, transforming and writing the dataframe.
        """
        create_table_if_not_exists(
            self.spark,
            self.catalog,
            schema=self.enriched_table.schema,
            table_name=self.enriched_table.table_name,
            path=self.enriched_table.table_path,
            database=self.enriched_table.database,
        )

        raw_deployments = delta_to_df(self.spark, db="raw", table="deployments_validated")

        # Filter deleted_at to prevent soft deleted records from always being recreated in enriched
        raw_deployments_without_deleted = raw_deployments.filter(F.col("deleted_at").isNull())

        valid_data, invalid_data = self._validate(raw_deployments_without_deleted)

        deployment_transformed = self._transform_columns(valid_data)
        deployment_transformed = add_timestamp_columns(deployment_transformed).withColumnRenamed(
            "updated_ts_utc", "updated_ts_utc"
        )

        # Get the sensor and location tables
        sensor_id = delta_to_df(self.spark, db="enriched_id", table="sensor_id")
        location = delta_to_df(self.spark, db="raw", table="locations_validated").filter(F.col("deleted_at").isNull())
        # TODO: Need preprocessing for the lat and lon?

        # Join the raw relationship IDs to the new golden_ids to replace them
        # We use inner joins as deployments with no sensor or/and no location should not be kept in enriched
        deployment_with_relationships = (
            deployment_transformed.join(
                sensor_id.select(
                    "sensor_golden_id",
                    "source",
                    F.col("source_id").alias("source_id_sensor"),
                    F.col("name").alias("sensor_name"),
                ),
                on=["source_id_sensor", "source"],
                how="inner",
            ).join(
                location.select(
                    F.lit("diopsis").alias("source"),
                    F.col("id").cast("string").alias("source_id_location"),
                    F.col("lat").cast(DecimalType(8, 5)).alias("latitude"),
                    F.col("lon").cast(DecimalType(8, 5)).alias("longitude"),
                ),
                on=["source_id_location", "source"],
                how="inner",
            )
        ).drop("source_id_sensor", "source_id_location")

        # Filter both existing data and the new data on deleted_at to deal with soft deletes from Faunabit
        deployment_with_golden_ids = generate_primary_keys(
            df=deployment_with_relationships,
            logical_keys=self.enriched_table.logical_keys,
            primary_key_column=self.enriched_table.primary_key_columns[0],
            existing_data=delta_to_df(self.spark, db="enriched", table=self.table_name),
        )

        # Upsert the enriched table to the Databricks Catalog
        upsert_to_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            df=deployment_with_golden_ids,
            primary_key_columns=self.enriched_table.primary_key_columns,
            static_columns=self.enriched_table.static_columns,
            ignore_change_columns=self.enriched_table.ignore_change_columns,
        )

        # Delete the soft_deleted records from raw in enriched
        delete_rows_from_delta_table(
            self.spark,
            database=self.enriched_table.database,
            table_name=self.enriched_table.table_name,
            logical_key_columns=self.enriched_table.logical_keys,
            rows_to_match=(
                raw_deployments.filter(F.col("deleted_at").isNotNull())
                .select(F.col("id").alias("source_id"), F.lit(self.source).alias("source"))
                .dropDuplicates()
            ),
        )

    @staticmethod
    def _validate(df: DataFrame) -> tuple[DataFrame, DataFrame]:
        # no validation, just return the dataframe as validated and an empty error dataframe
        return df, df.where(lit(False))

    def _transform_columns(self, valid_data: DataFrame) -> DataFrame:
        """Transform Nanopore validated data df into enriched df following
        the data-model for the deployment table.
        """
        return valid_data.select(
            F.col("name").cast("string"),
            F.col("id").cast("string").alias("source_id"),
            F.col("start").cast("timestamp").alias("start_ts_utc"),
            F.col("end").cast("timestamp").alias("end_ts_utc"),
            F.lit(None).cast("string").alias("sensor_orientation"),
            F.lit(None).cast(DecimalType(10, 5)).alias("sensor_height"),
            F.lit(None).cast("string").alias("screen_orientation"),
            F.lit(None).cast("boolean").alias("bait"),
            F.lit(self.source).alias("source"),
            # Relationship with locations
            F.col("location_id").cast("string").alias("source_id_location"),
            # Relationship with sensor
            F.col("device_id").cast("string").alias("source_id_sensor"),
        )
