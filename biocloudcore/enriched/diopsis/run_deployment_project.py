import logging

from biocloudcore.data_lake import DataLake
from biocloudcore.enriched.diopsis.deployment_project import DeploymentProject
from biocloudcore.spark_session import get_environment, set_default_catalog, spark_session


def run():
    table_name = "deployment_project"
    source_name = "diopsis"

    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    catalog = set_default_catalog(spark=spark, domain="biocloud")
    data_lake = DataLake(env=get_environment(spark))

    DeploymentProject(
        spark=spark, catalog=catalog, data_lake=data_lake, source=source_name, table_name=table_name
    ).run()

    spark.stop()


if __name__ == "__main__":
    run()
