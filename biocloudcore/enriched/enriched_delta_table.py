from pyspark.sql.types import LongType, StringType, StructField, StructType, TimestampType

from biocloudcore.data_lake import Stage


class EnrichedDeltaTable:
    def __init__(
        self,
        table_name: str,
        table_stage: Stage,
        schema: StructType,
        logical_keys: list[str],
        static_columns: list[str],
        ignore_change_columns: list[str],
        database: str = "enriched",
        primary_key_columns: list[str] = None,
    ):
        """
        Represents an enriched Delta table with schema, logical keys, and other configurations. This class contains
        all the necessary information to create an enriched Delta table. It also contains a Generator Class
        which uses the created instance of this class to generate an ID table that is connected to this enriched table.

        The ID table is used to manage unique identifiers (Golden IDs) for records in the enriched table. With the
        ID table we can find back in the source system the unique records in the enriched table with its system
        generated golden_id by linking it to their original source_id. This is useful for Master Data Management,
        especially when 2 or more source systems can update the same record in the enriched table.

        For more info on Golden IDs and _id tables, see the wiki page:
        https://gitlab.com/groups/arise-biodiversity/biocloud/-/wikis/4.-Data-model/4.2-Golden-records-&-ids


        :param table_name: Name of the table
        :param schema: Schema of the dataset
        :param table_path: Path to the table
        :param logical_keys: List of logical key columns, the combination of which uniquely identifies a record before
         a golden ID is assigned.
        :param static_columns: List of static columns
        :param ignore_change_columns: List of columns to ignore changes
        :param database: Unity Catalog database name
        :param primary_key_columns: List of primary key columns (This is usually the golden_id column)

        **Example Usage**

        .. code-block:: python

        # Create the actual EnrichedDeltaTable instance
        self.enriched_table = EnrichedDeltaTable(
            table_name=self.table_name,
            schema=StructType(
                [
                    StructField("sequencing_run_golden_id", LongType(), True),
                    StructField("title", StringType(), True),
                    StructField("project_name", StringType(), True),
                    StructField("end_date_time", TimestampType(), True),
                    # StructField("legacy_created_datetime", TimestampType(), True),
                    # StructField("legacy_last_updated_datetime", TimestampType(), True),
                    StructField("last_updated_source", StringType(), True),
                    StructField("inserted_ts_utc", TimestampType(), True),
                    StructField("updated_ts_utc", TimestampType(), True),
                ]
            ),
            logical_keys=["project_name", "title"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
        )
        # Create an ID table for the enriched table with the IdTableGenerator
        self.id_table = IdTableGenerator(self.enriched_table)
        """
        self.table_name = table_name
        self.table_stage = table_stage
        self.table_path = table_stage.get_delta_path(table_name)
        self.schema = schema
        self.logical_keys = logical_keys
        self.static_columns = static_columns
        self.ignore_change_columns = ignore_change_columns
        self.database = database
        self.primary_key_columns = primary_key_columns if primary_key_columns else [f"{self.table_name}_golden_id"]


class IdTableGenerator:
    """
    Generates an ID table for a given enriched table.

    The ID table is used to manage unique identifiers (Golden IDs) for records in the enriched table.
    It ensures that each record in the enriched table can be tracked back to its source system.

    A ID table is generated with a enriched table as input. The ID table schema is based on the enriched table schema
    and can not be created without the existence of an enriched table. By default, each enriched table should have a
    ID table with the same name as the enriched table with "_id" appended to the name in the enriched_id database.
    In rare cases, we could choose to not create the ID table for a specific enriched table by just not calling this
    Generator class and only upserting the enriched table.

    :param enriched_table: An instance of `EnrichedDeltaTable` for which the ID table is being generated.

    **Raises**
    - `ValueError`: If the table_name ends with "_id", indicating that an ID table cannot be created for an ID table.
    """

    def __init__(self, enriched_table: EnrichedDeltaTable):
        if enriched_table.table_name.endswith("_id"):
            raise ValueError("ID table cannot be created for an ID table")

        self.id_table = EnrichedDeltaTable(
            table_name=f"{enriched_table.table_name}_id",
            table_stage=enriched_table.table_stage,
            schema=self.get_id_table_schema(
                enriched_table.table_name,
                logical_keys=[
                    field for field in enriched_table.schema.fields if field.name in enriched_table.logical_keys
                ],
            ),
            logical_keys=enriched_table.logical_keys,
            static_columns=["source_inserted_ts_utc"],
            ignore_change_columns=["source_updated_ts_utc"],
            database=f"{enriched_table.database}_id",
            primary_key_columns=[f"{enriched_table.table_name}_golden_id", "source_id", "source"],
        )

    def __getattr__(self, name):
        """
        Allows access to the attributes of the ID table instance.
        Example:
            `id_table.path` will return the path of the ID table. So we do not have to call `id_table.id_table.path`
        """
        return getattr(self.id_table, name)

    @staticmethod
    def get_id_table_schema(table_name: str, logical_keys: list[StructField]) -> StructType:
        """
        Generates the schema for the ID table associated with the given enriched table.

        :param table_name: Name of the enriched table for which the ID table schema is being generated.
        :param logical_keys: List of logical key columns from the enriched table schema.

        :return: StructType representing the schema of the ID table.

        The schema includes the following fields:
        - {table_name}_golden_id: LongType, Golden ID for the table. This is the system generated primary key.
        - source_id: StringType, Original identifier of the item in the source.
        - source: StringType, The source of the data.
        - source_inserted_ts_utc: TimestampType, Timestamp when the data of the source was inserted.
        - source_updated_ts_utc: TimestampType, Timestamp when the data of the source was last updated.
        - logical_keys: Logical key columns from the enriched table schema, which define when a record is a duplicate.
        """
        return StructType(
            [
                StructField(f"{table_name}_golden_id", LongType(), True),
                StructField("source_id", StringType(), True),
                StructField("source", StringType(), True),
                StructField("source_inserted_ts_utc", TimestampType(), True),
                StructField("source_updated_ts_utc", TimestampType(), True),
                *logical_keys,
            ]
        )
