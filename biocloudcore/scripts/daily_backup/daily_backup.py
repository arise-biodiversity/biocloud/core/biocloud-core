import concurrent.futures
import os.path
from datetime import datetime, timedelta
from sys import argv

# This script is supposed to be run as a nightly scheduled job in Databricks.

environment = argv[1]
# Copy raw tables
raw_source = f"s3://biocloud-core-{environment}-raw/biocloud"
raw_backup = f"s3://biocloud-core-{environment}-raw/biocloud-backup"

# Copy enriched tables
enriched_source = f"s3://biocloud-core-{environment}-enriched/biocloud"
enriched_backup = f"s3://biocloud-core-{environment}-enriched/biocloud-backup"


def copy_wrapper_func(source, dest):
    dbutils.fs.cp(source, dest, True)  # noqa


def iterate_s3_directories_filtered(source_dir, destination_dir):
    items = dbutils.fs.ls(source_dir)  # noqa
    result = []

    for item in items:
        # Extract the last part of the path to get the directory or file name
        item_name = item.path.strip("/").split("/")[-1]

        # Skip items starting with '_'
        if not item_name.startswith("_"):
            source_path = item.path
            # Construct the destination path by appending the item name to the destination directory
            destination_path = os.path.join(destination_dir, item_name)
            # Copy the item to the destination
            if item.isDir():
                print(f"Collect files in dir {source_path}")
                # If it's a directory, create it at the destination and recurse into it
                dbutils.fs.mkdirs(destination_path)  # noqa
                result += iterate_s3_directories_filtered(source_path, destination_path + "/")
            else:
                # If it's a file, copy it directly
                result.append([source_path, destination_path])
    return result


def copy_directory(source_dir, destination_dir):
    """Copy files and directories from the source to the destination, skipping items that start with '_'."""

    thread_pool_size = 25
    th_pool_exec = concurrent.futures.ThreadPoolExecutor(max_workers=thread_pool_size)
    futures = []
    future_results = []

    items = iterate_s3_directories_filtered(source_dir, destination_dir)
    for i in items:
        print(f"Start copying {i[0]} -> {i[1]}")
        futures.append(th_pool_exec.submit(copy_wrapper_func, i[0], i[1]))

    for future in concurrent.futures.as_completed(futures):
        try:
            futureResult = future.result()
            future_results.append(futureResult)
            print(f"Finished copying {len(future_results)}/{len(futures)}")
        except Exception as exc:
            raise exc


def clean_old_backups(base_path, days=3):
    """Delete backup directories older than a specified number of days."""
    cutoff_date = datetime.utcnow() - timedelta(days=days)
    print(f"Clean up backups in {base_path} older than {cutoff_date}")
    for item in dbutils.fs.ls(base_path):  # noqa
        item_path = item.path
        dir_name = item_path.strip("/").split("/")[-1]  # Extract directory name
        try:
            dir_date = datetime.strptime(dir_name, "%Y-%m-%d")
            if dir_date < cutoff_date:
                print(f"Delete backup older than {days} days: {item_path}")
                dbutils.fs.rm(item_path, recurse=True)  # noqa
        except ValueError:
            # If the directory name does not match the date format, skip it
            continue


def make_daily_backup(source_path, backup_base_path, days=3):
    print(f"Make daily backup: {source_path} -> {backup_base_path}")
    today_str = datetime.utcnow().strftime("%Y-%m-%d")
    copy_directory(source_path, backup_base_path + "/daily/" + today_str)
    clean_old_backups(backup_base_path + "/daily", days=days)


def run_backup():
    make_daily_backup(raw_source, raw_backup)
    make_daily_backup(enriched_source, enriched_backup)


if __name__ == "__main__":
    if len(argv) < 2:
        print("Usage: daily_backup.py <environment>")
        exit(1)
    run_backup()
