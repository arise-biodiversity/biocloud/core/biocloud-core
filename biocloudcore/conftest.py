import pytest

# from pyspark.dbutils import DBUtils
from pyspark.sql import Catalog, SparkSession

from biocloudcore.spark_session import spark_session


@pytest.fixture(scope="session")
def spark() -> SparkSession:
    return spark_session()


@pytest.fixture(scope="session")
def catalog() -> Catalog:
    return spark_session().catalog
