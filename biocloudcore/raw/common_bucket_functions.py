import logging

from biocloudcore.data_lake import S3Client

logger = logging.getLogger(__name__)


def find_metadata_files(s3_client: S3Client, bucket_name: str, file_prefix: str) -> list:
    """
    Find all metadata files in the specified S3 bucket and prefix and return the
    S3 paths to the files.

    :param S3Client s3_client: The S3 service client used to list objects.
    :param str bucket_name: The name of the S3 bucket.
    :param str file_prefix: The prefix path within the S3 bucket.
    :return: list of S3 paths to the metadata files.
    """
    return [
        file_name
        for file_name in s3_client.list_objects(bucket_name=bucket_name, prefix=file_prefix, only_files=True)
        # Filter out temporary files
        if "~" not in file_name
    ]


def find_metadata_file_path(s3_client: S3Client, bucket_name: str, file_prefix: str, entity: str) -> str:
    """
    Find a metadata file for a given entity in the specified S3 bucket and prefix and return the
    S3 path to the file.

    :param S3Client s3_client: The S3 service client used to list objects.
    :param str bucket_name: The name of the S3 bucket.
    :param str file_prefix: The prefix path within the S3 bucket.
    :param str entity: The entity name to search for in the metadata files.
    :return: The S3 path to the metadata file.
    :raises MetadataNotFoundError: If the metadata file for the specified entity is not found.
    """

    s3_uris = s3_client.list_objects(bucket_name=bucket_name, prefix=file_prefix, only_files=True, full_uri=True)

    file_uri = next((file_path for file_path in s3_uris if entity in file_path), None)

    if file_uri:
        logger.info(f"Metadata file found for {entity} in {bucket_name}")
        return file_uri
    else:
        logger.error(f"Metadata file not found for {entity} in {bucket_name}")
        raise Exception(f"The metadata file for entity: {entity} is not found in the landing zone.")
