import logging
import re

import pyspark.sql.functions as F
from pyspark.sql import DataFrame

logger = logging.getLogger(__name__)


def check_column_value_match(df1: DataFrame, df2: DataFrame, column1: str, column2: str) -> bool:
    """
    Checks whether any value in df1[column1] appears in df2[column2].
    :param df1 (DataFrame) which values to check against df2
    :param df2 (DataFrame) whose values are checked for duplicates from df1
    :param column1 (str) name of column of df1 to check
    :param column2 (str) name of column of df2 to check
    """
    values_to_check = [row[column1] for row in df1.select(column1).distinct().collect()]
    matching_rows = df2.filter(F.col(column2).isin(values_to_check))
    return matching_rows.count() > 0


def column_has_unique_values(column_name: str, df: DataFrame) -> bool:
    """
    Checks whether all the values in the given column are unique.
    :param column_name (str)
    :param df (DataFrame)
    :return bool
    """
    return df.select(column_name).distinct().count() == df.count()


def filter_list_from_dataframe_column(
    df: DataFrame, column_name: str, filter_list: list
) -> tuple[DataFrame, DataFrame]:
    """
    Filters the list for the given column of the given dataframe. For example, we filter all names from the list
    called 'names' from the dataframe column called 'names'.
    :param df (DataFrame)
    :return tuple[DataFrame, DataFrame] with valid and invalid dataframes
    """
    outside_filter_df: DataFrame = df.filter(~F.col(column_name).isin(filter_list))
    within_filter_df: DataFrame = df.filter(F.col(column_name).isin(filter_list))

    return outside_filter_df, within_filter_df


def find_non_unique_values(column_name: str, df: DataFrame) -> list:
    """
    Finds all non-unique values in the given column of the given dataframe.
    :param column_name (str)
    :param df (DataFrame)
    :return list
    """
    grouped_df = df.groupBy(column_name).count()
    # Filter to get non-unique values (count > 1)
    non_unique_values = grouped_df.filter(F.col("count") > 1)
    # Collect these into a list
    non_unique_values_list = [row[column_name] for row in non_unique_values.collect()]
    return non_unique_values_list


def is_valid_registration_number(registration_number: str, strict_mode: bool = True) -> bool:
    """Checks if a given registration number is valid. Based on the following document:
    https://docs.google.com/document/d/1NiS_JzpCxeGofq808N1xTmHTjH4f7t4awqReWhGr2mg/edit?tab=t.0#heading=h.nsolpicpcfdq

    This is a long function, but necessary because of the many exceptions within the registration numbers. This is
    because the initial design for these numbers stems from the 90's. It could be shortened using some next-level
    regex, but as we like readability, we implemented all requirements provided in the document using clear and
    simple if statements. This also allows easy changes when some obscure new rule needs to be implemented.

    This is a sieve Erathostenes could be proud of.

    :param registration_number (str)
    :param strict_mode (bool, default True) denotes whether we apply stricter rules to the given registration number

    :return bool
    """
    if not registration_number:
        return False

    # These are prefixes that are used in the CRS
    crs_prefixes = [
        "NCBN",
        "BE",
        "ZAM",
        "LBE",
        "RGMS",
        "NBC",
        "NRRMNH",
        "NS",
        "ZMA",
        "TEST",
        "e",
        "RMNH",
        "RGM",
        "RMMH",
        "ZNA",
    ]

    # Characters cannot have both spaces and full stops
    if _has_character(" ", registration_number) and _has_character(".", registration_number):
        return False

    # Prefix ‘L’. Short for the ‘Leiden’ herbarium
    # Prefix ‘U’. Short for the ‘Utrecht’ herbarium
    if registration_number.startswith("L") or registration_number.startswith("U"):
        # These registration numbers should have either a full stop or a space
        if not _has_character(" ", registration_number) and not _has_character(".", registration_number):
            return False
        if _has_character(".", registration_number):
            # Followed by a dot, and a numeric value. The numeric value NEVER starts
            # with a 0, but with a 1 or higher
            sections: list[str] = registration_number.split(".")
            if len(sections) != 2:
                return False
            if not _is_castable_to_int(sections[-1]):
                return False
            if sections[-1].startswith("0"):
                return False
        elif _has_character(" ", registration_number):
            # Followed by 2 spaces, and a numeric value. The numeric value ALWAYS
            # starts with a 0
            sections: list[str] = registration_number.split(" ")
            sections = list(filter(None, sections))

            if registration_number.count(" ") != 2:
                return False
            if len(sections) != 2:
                return False
            if not _is_castable_to_int(sections[-1]):
                return False
            if not sections[-1].startswith("0"):
                return False

    # Prefix ‘WAG’. Short for the ‘Wageningen’ herbarium
    # Prefix ‘AMD’. Short for the ‘Amsterdam’ herbarium
    elif registration_number.startswith("AMD") or registration_number.startswith("WAG"):
        # Space are not allowed for AMD and WAG.
        if _has_character(" ", registration_number):
            return False

        if _has_character(".", registration_number):
            # Followed by a dot, and a numeric value. The numeric value NEVER starts
            # with a 0, but with a 1 or higher
            sections: list[str] = registration_number.split(".")
            if len(sections) != 2:
                return False
            if not _is_castable_to_int(sections[-1]):
                return False
            if sections[-1].startswith("0"):
                return False
        else:
            # Directly followed by a numeric value. The numeric value ALWAYS starts with a 0.
            match = re.match(r"^([A-Z]+)(\d+)", registration_number)
            if match:
                sections: list[str] = [match.group(1), match.group(2)]
                if len(sections) != 2:
                    return False
                if not _is_castable_to_int(sections[-1]):
                    return False
                if not sections[-1].startswith("0"):
                    return False
            else:
                logger.error(f"Could not regex the registration number: {registration_number}.")
                return False

    # Check for CRS prefixes
    elif any(registration_number.startswith(prefix) for prefix in crs_prefixes):
        # This might be a CRS number. These do not have spaces
        if _has_character(" ", registration_number):
            return False
        # Every CRS number has a full stop.
        if not _has_character(".", registration_number):
            return False
        # a Prefix (e.g. UPPERCASE letters, potentially with dots, e.g. ‘RMNH’ or ‘ZMA.INS’),
        # a Catalogue number (a numeric value, e.g. ‘12345’)
        # sometimes a Suffix (mostly a short string of letters e.g. ‘a’).
        # Each part is separated by a dot
        try:
            match = re.findall(r"^([A-Z.]+)(\d+)(.*)$", registration_number)[0]
            if match:
                prefix: str = match[0]
                number: str = match[1]
                suffix: str = match[2] if len(match) == 3 else ""

                # Remove the trailing full stop from the suffix (i need a better regex)
                if suffix and suffix[0] == ".":
                    suffix = suffix[1:]

                # Check if our prefix is capitalized alpha characters only (and full stop)
                if not bool(re.match(r"^[A-Z.]+$", prefix)):
                    return False
                if not _is_castable_to_int(number):
                    return False
                if len(number) > 7:
                    return False

                if suffix:
                    # Suffix can only have alphanumericals and dashes.
                    if not bool(re.match(r"^[a-zA-Z0-9-]+$", suffix)):
                        return False

                    if strict_mode and not _is_strict_suffix(suffix):
                        logger.error(f"Registration_number {registration_number} failed the strict suffix test.")
                        return False

            else:
                logger.error(f"Could not regex the registration number: {registration_number}.")
                return False

        except Exception as e:
            logger.error(f"Regex matching the registration number went wrong: {e} for {registration_number}.")
            return False

    else:
        logger.error(f"Unknown prefix for registration number: {registration_number}.")
        return False

    return True


def _has_character(character: str, string: str) -> bool:
    """
    Checks whether the given string has the given substring
    :param character (str)
    :param string (str)

    :return bool
    """
    return character in string


def _is_castable_to_int(thing) -> bool:
    """
    Checks whether the given object is a number
    :param thing (any)

    :return bool
    """
    try:
        return isinstance(int(thing), int)
    except ValueError:
        return False


def _is_strict_suffix(suffix: str) -> bool:
    """
    Checks if the given suffix has either 1 or 2 numbers, or 1 or 2 lowercased letters.
    :param suffix (str)
    :returns bool
    """
    # Check if we received either 1 or 2 numbers
    return bool(re.match(r"^\d{1,2}$", suffix) or re.match(r"^[a-z]{1,2}$", suffix))


# Some code to test registration numbers. Will be removed later.
if __name__ == "__main__":
    registration_number: str = "RMNH.5180233 "
    print(is_valid_registration_number(registration_number, True))
