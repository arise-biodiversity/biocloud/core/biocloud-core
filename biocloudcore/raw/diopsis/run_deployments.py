import logging

from biocloudcore.data_lake import DataLake
from biocloudcore.raw.diopsis.deployments import Deployments
from biocloudcore.spark_session import get_environment, set_default_catalog, spark_session

logger = logging.getLogger(__name__)


def run():
    table_name = "deployments"

    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    catalog = set_default_catalog(spark=spark, domain="biocloud")
    data_lake = DataLake(env=get_environment(spark))

    Deployments(spark=spark, catalog=catalog, data_lake=data_lake).run(
        bucket_name=data_lake.nimbo_diopsis.landing_zone.get_bucket_name(),
        destination_path=data_lake.nimbo_diopsis.raw.get_path(dataset_name=table_name),
        table_name=table_name,
    )

    spark.stop()


if __name__ == "__main__":
    run()
