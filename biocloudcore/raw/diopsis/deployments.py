import logging

from pyspark.sql import Catalog, SparkSession

from biocloudcore.common_dataframe_functions import add_timestamp_columns
from biocloudcore.data_lake import DataLake
from biocloudcore.raw.common_s3_functions import find_metadata_file_path
from biocloudcore.raw.diopsis.validators import DeploymentsValidator as Validator
from biocloudcore.utils.spark_utils import create_dataframe_from_json, create_table_if_not_exists, upsert_to_delta_table

logger = logging.getLogger(__name__)


class Deployments:
    """
    **Description**

    The Deployments table contains information about ...

    TODO: Schema will be auto schema merge in the future as soon as the Databricks runtime
    (of Serverless) is updated to 15.4 to enable this feature in Delta Lake 3.2.0.

    The schema is flexible and we accept any changes in the schema from source. Only in enriched
    we will control our schema.

    **Schematic overview**

    .. uml::

    :landing_zone_json
    **path/to/landing-zone/deployments.json**;

    :raw_delta
    **path/to/raw/deployments/validated**
    **path/to/raw/deployments/errors**;

    """

    def __init__(self, spark: SparkSession, catalog: Catalog, data_lake: DataLake) -> None:
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake

    def run(self, bucket_name: str, destination_path: str, table_name: str) -> None:
        # Get the file_path from the landing zone
        file_path = find_metadata_file_path(
            self.data_lake.biocloud.get_client(),  # TODO: to be changed, when all code is using data_lake
            bucket_name=bucket_name,
            file_prefix="diopsis/faunabit",
            entity=table_name,
        )

        raw_df = create_dataframe_from_json(self.spark, file_path, schema=None).dropDuplicates(["id"])

        # Validate the dataframe
        valid_df, invalid_df = Validator().validate(raw_df)

        # Define the paths for valid and invalid dataframes
        raw_dfs = [(valid_df, "validated"), (invalid_df, "errors")]

        # Write the dataframes to the delta table
        for df, valid_or_error in raw_dfs:
            if not df.isEmpty():
                df_to_upsert = add_timestamp_columns(df)

                create_table_if_not_exists(
                    self.spark,
                    self.catalog,
                    schema=df_to_upsert.schema,
                    table_name=f"{table_name}_{valid_or_error}",
                    path=f"{destination_path}{valid_or_error}",
                    database="raw",
                )

                upsert_to_delta_table(
                    self.spark,
                    database="raw",
                    table_name=f"{table_name}_{valid_or_error}",
                    df=df_to_upsert,
                    primary_key_columns=["id"],
                    static_columns=["inserted_ts_utc"],
                    ignore_change_columns=["updated_ts_utc", "inserted_ts_utc"],
                    auto_schema_merge=True,
                    optimize_table=True,
                )
