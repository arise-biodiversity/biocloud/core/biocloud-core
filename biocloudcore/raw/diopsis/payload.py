import logging
from datetime import datetime

from pyspark.sql import Catalog, DataFrame, SparkSession

from biocloudcore.data_lake import DataLake
from biocloudcore.raw.common_s3_functions import (
    get_device_folders_with_payload,
    move_files_to_processed_folder,
    upload_files_to_blob,
)
from biocloudcore.raw.diopsis.validators import PayloadValidator as Validator
from biocloudcore.utils.spark_utils import (
    create_table_if_not_exists,
    upsert_to_delta_table,
)

logger = logging.getLogger(__name__)


class Payload:
    """
    **Description**

    The Payloads class handles the processing of payloads. It first creates the meta data table called
    'sensor_media_item', which contains meta data information about each payload object. Next, payload
    files are copied to the blob storage. If this is successful, the 'uploaded' boolean in the meta
    data table will be set to True (and False if not).

    TODO: Schema will be auto schema merge in the future as soon as the Databricks runtime
    (of Serverless) is updated to 15.4 to enable this feature in Delta Lake 3.2.0.

    The schema is flexible and we accept any changes in the schema from source. Only in enriched
    we will control our schema.

    **Schematic overview**

    .. uml::

    :landing_zone_json
    **path/to/landing-zone/diopsis2/payloads**;

    :raw_delta
    **path/to/raw/sensor_media_item/validated**
    **path/to/raw/sensor_media_item/errors**;

    """

    def __init__(
        self,
        spark: SparkSession,
        catalog: Catalog,
        data_lake: DataLake,
        max_upload_workers: int,
        max_s3_connections: int,
    ) -> None:
        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake
        self.max_upload_workers = max_upload_workers  # Python threads that can upload simultaneously.
        # create a custom client for high throughput
        self.s3_client = self.data_lake.biocloud.get_custom_client(max_pool_connections=max_s3_connections)

    def run(
        self,
        source_bucket_name: str,
        destination_blob_name: str,
        destination_blob_path: str,
        destination_path: str,
        source_name: str,
        source_payload_path: str,
        table_name: str,
        camera_numbers: str,  # Select which camera's to process
        max_devices: int,  # Limit number of devices to process. No limit: None, no payloads 0.
    ) -> None:
        # Variables to define in a run file

        devices = self._get_devices(
            bucket_name=source_bucket_name,
            camera_numbers=camera_numbers,
            source=source_name,
            source_payload_path=source_payload_path,
            max_devices=max_devices,
        )

        if not devices:
            raise Exception(f"No devices found for {source_name}.")

        for device in devices:
            logger.warning(f"Processing {source_name} device {device}")

            payload_objects = self.s3_client.list_objects(
                bucket_name=source_bucket_name, prefix=f"{source_payload_path}/{device}/", only_files=True
            )
            logger.warning(f"Found {len(payload_objects)} files in the landing zone for {device}")

            if payload_objects:
                # Get the relative file paths of the payload objects from S3 metadata
                file_paths = payload_objects

                # Create a metadata dataframe for the payload objects with source path and the destination blob path
                metadata_df = self._create_metadata_df(
                    payload_relative_file_paths=file_paths,
                    device_name=device,
                    destination_blob_path=destination_blob_path,
                )

                # Upload the files to blob storage and return the metadata dataframe with upload status
                metadata_with_upload_status = upload_files_to_blob(
                    self.s3_client,
                    max_upload_workers=self.max_upload_workers,
                    source_bucket=source_bucket_name,
                    destination_blob=destination_blob_name,
                    payload_metadata_df=metadata_df,
                )

                # Validate the dataframe
                valid_df, invalid_df = Validator().validate(metadata_with_upload_status)

                # Define the paths for valid and invalid dataframes
                raw_dfs = [(valid_df, "validated"), (invalid_df, "errors")]

                # Write the dataframes to the delta table
                for df, valid_or_error in raw_dfs:
                    if not df.isEmpty():
                        create_table_if_not_exists(
                            self.spark,
                            self.catalog,
                            schema=df.schema,
                            table_name=f"{table_name}_{valid_or_error}",
                            path=f"{destination_path}{valid_or_error}",
                            database="raw",
                        )

                        upsert_to_delta_table(
                            self.spark,
                            database="raw",
                            table_name=f"{table_name}_{valid_or_error}",
                            df=df,
                            primary_key_columns=["blob_path"],
                            static_columns=["inserted_ts_utc"],
                            ignore_change_columns=["updated_ts_utc", "inserted_ts_utc"],
                            auto_schema_merge=True,
                            optimize_table=True,
                        )

                # Move the successfully processed files to the "processed" folder
                uploaded_files = valid_df.select("source_path").collect()
                move_files_to_processed_folder(
                    s3_client=self.s3_client,
                    max_upload_workers=self.max_upload_workers,
                    source_bucket_name=source_bucket_name,
                    uploaded_files=uploaded_files,
                )

            else:
                # Either no metadata or no payload objects are found, or neither.
                logger.warning(f"No payload-files found for {device}.")

    def _get_devices(
        self, bucket_name: str, camera_numbers: str, source: str, source_payload_path: str, max_devices: int
    ) -> list[str]:
        """Return list of devices to be processed."""
        if camera_numbers:
            # Process only given camera numbers
            devices = [f"{source.upper()}-{camera}" for camera in camera_numbers]
        else:
            # Process all camera
            devices = get_device_folders_with_payload(
                s3_client=self.data_lake.biocloud.get_client(),
                source=source,
                bucket=bucket_name,
                prefix=source_payload_path,
                device_limit=max_devices,
            )
        logger.warning(f"Devices {devices} will be processed.")
        return devices

    def _extract_datetime_from_filename(self, file_name: str, device_name: str) -> datetime:
        """
        Extract datetime from filename.
        :param file_name: file name in a specific format, for example: DIOPSIS-207_2024-10-22T07:29:35+02:00.jpg
        :param device_name: device name
        :return: datetime object extracted from filename
        """
        try:
            # Split the filename by the device name and the underscore
            parts = file_name.split(f"{device_name}_")
            if len(parts) != 2:
                raise ValueError(
                    "Filename does not contain the expected structure with device name"
                    f" '{device_name}_[datepart].[file_extension]'."
                )

            # Extract the datetime part before the extension for example (.jpg)
            datetime_str = parts[1].split(".")[0]

            # Attempt to parse the datetime string
            return datetime.fromisoformat(datetime_str)

        except ValueError as e:
            # Handle case where the filename doesn't match the expected structure
            logger.error(f"Error: {e}")
            raise

        except Exception as e:
            # Catch any unexpected errors (like bad datetime format)
            logger.error(f"Unexpected error: {e}")
            raise

    def _create_metadata_df(
        self, payload_relative_file_paths: list[str], destination_blob_path: str, device_name: str
    ) -> DataFrame:
        """
        For every payload file found in the landing zone, create a row in the metadata dataframe
        with information about the payload file and return it.

        :param payload_relative_file_paths: List of payload relative file paths
        :param destination_blob_path: The relative path in the blob storage to which the payloads should be written
        :param device_name: Device name (For example DIOPSIS-204)
        :return: DataFrame with metadata on the payload file
        """
        metadata_list = []

        for path in payload_relative_file_paths:
            image_id = path.split("/")[-1] or "fallback"
            date = self._extract_datetime_from_filename(file_name=image_id, device_name=device_name)

            row = {
                "source_path": path,
                "device_name": device_name,
                "capture_datetime": f"{date.isoformat()}",
                "mime_type": "image/jpeg",
                "blob_path": (
                    f"{destination_blob_path}/{device_name}" f"/original/{str(date.year)}/{date.month:02d}/{image_id}"
                ),
            }

            metadata_list.append(row)

        return self.spark.createDataFrame(metadata_list)
