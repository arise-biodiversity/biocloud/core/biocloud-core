import pyspark.sql.functions as F
from pyspark.sql import DataFrame


class PayloadValidator:
    def validate(self, df: DataFrame) -> tuple[DataFrame, DataFrame]:
        required_columns = ["device_name"]
        valid_records = df.filter(F.col("upload_success").isNotNull()).dropna(subset=required_columns)
        invalid_records = df.exceptAll(valid_records)
        return valid_records, invalid_records
