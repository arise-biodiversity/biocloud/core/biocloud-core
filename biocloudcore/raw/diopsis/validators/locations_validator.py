from pyspark.sql import DataFrame


class LocationsValidator:
    def validate(self, df: DataFrame) -> tuple[DataFrame, DataFrame]:
        required_columns = ["id", "name", "lat", "lon"]
        valid_records = df.dropna(subset=required_columns)
        invalid_records = df.exceptAll(valid_records)
        return valid_records, invalid_records
