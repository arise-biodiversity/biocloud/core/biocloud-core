import logging

from biocloudcore.cmd_parser import ParameterParser
from biocloudcore.data_lake import DataLake
from biocloudcore.raw.diopsis.payload import Payload
from biocloudcore.spark_session import get_environment, set_default_catalog, spark_session

logger = logging.getLogger(__name__)


def run():
    source_name = "diopsis"
    table_name = "sensor_media_items"

    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.WARNING)

    # Load parameters from Databricks or local code execution
    params = ParameterParser().parse()

    spark = spark_session()
    catalog = set_default_catalog(spark=spark, domain="biocloud")
    data_lake = DataLake(env=get_environment(spark))

    # Camera numbers to process (separated by comma) eg. "455,374". None processes all cameras
    camera_numbers = params.get(key="camera_numbers", dtype=list)
    max_devices = params.get(key="max_devices", dtype=int)

    Payload(
        spark=spark,
        catalog=catalog,
        data_lake=data_lake,
        # Python threads that can upload simultaneously.
        max_upload_workers=50,
        # Max concurrent AWS connections for high throughput this is generally between 50 and 100.
        max_s3_connections=60,
    ).run(
        source_bucket_name=data_lake.nimbo_diopsis.landing_zone.get_bucket_name(),
        destination_blob_name=data_lake.nimbo_diopsis.blob.get_bucket_name(),
        destination_blob_path=f"{source_name}/images/insectcamera",
        destination_path=data_lake.nimbo_diopsis.raw.get_path(dataset_name=table_name),
        source_name=source_name,
        source_payload_path=f"media/{source_name}/sensors",
        table_name=table_name,
        camera_numbers=camera_numbers,
        max_devices=max_devices,
    )

    spark.stop()


if __name__ == "__main__":
    run()
