import logging

from biocloudcore.data_lake import DataLake
from biocloudcore.raw.nanopore.consensuses import Consensuses
from biocloudcore.spark_session import get_environment, set_default_catalog, spark_session

logger = logging.getLogger(__name__)


def run():
    source_name = "nanopore"
    table_name = "consensuses"
    layer = "raw"

    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    catalog = set_default_catalog(spark=spark, domain="csc")
    data_lake = DataLake(env=get_environment(spark))

    Consensuses(
        spark=spark, catalog=catalog, data_lake=data_lake, layer=layer, source=source_name, table_name=table_name
    ).run(
        bucket_name=data_lake.nanopore.landing_zone.get_bucket_name(),
        destination_path=data_lake.nanopore.raw.get_path(dataset_name=table_name),
    )


if __name__ == "__main__":
    run()
