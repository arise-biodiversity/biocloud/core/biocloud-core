import pyspark.sql.functions as F
from pyspark.sql import DataFrame

from biocloudcore.raw.common_validator_functions import (
    is_valid_registration_number,
)


class AmpliconsValidator:
    def __init__(self) -> None:
        self.reason: str

    def is_processable(self, df: DataFrame) -> bool:
        """
        Check if the dataframe is processible by our code.
        :param df (DataFrame)
        :returns bool
        """
        # Check if all required columns are in the dataframe
        required_columns: list[str] = [
            "id",
            "marker",
            "forward_primer",
            "pcr_id",
            "project_id",
            "reverse_primer",
            "sequencing_run",
        ]
        if not any(element in df.columns for element in required_columns):
            return False

        # Check if the sheet has rows
        if df.isEmpty():
            self.reason = "Given dataframe is empty."
            return False

        return True

    def filter_invalid_rows(self, df: DataFrame) -> tuple[DataFrame, DataFrame]:
        """
        Validates the incoming dataframe by moving invalid rows to the invalid_df. All remaining
        rows are deemed valid and returned as the valid_df.
        :param df (DataFrame)
        :return tuple[DataFrame, DataFrame] with valid and invalid dataframes
        """
        valid_df, invalid_df = self._filter_invalid_registration_numbers(df)

        return valid_df, invalid_df

    def _filter_invalid_registration_numbers(self, df) -> tuple[DataFrame, DataFrame]:
        """
        Validates the incoming dataframe on the validity of its registration numbers.
        :param df (DataFrame)
        :return tuple[DataFrame, DataFrame], one having valid records, the other invalid
        """
        # Check if all registration numbers are valid
        column_values = df.select("name").collect()
        unacceptable_registration_numbers: list[str] = [
            row[0] for row in column_values if not is_valid_registration_number(row[0], strict_mode=True)
        ]

        invalid_df: DataFrame = df.filter(F.col("name").isin(unacceptable_registration_numbers))
        valid_df: DataFrame = df.filter(~F.col("name").isin(unacceptable_registration_numbers))

        return valid_df, invalid_df
