from pyspark.sql import DataFrame


class SequencingRunsValidator:
    def validate(self, df: DataFrame) -> tuple[DataFrame, DataFrame]:
        required_columns = ["id", "end_time", "title"]
        valid_records = df.dropna(subset=required_columns)
        invalid_records = df.exceptAll(valid_records)
        return valid_records, invalid_records
