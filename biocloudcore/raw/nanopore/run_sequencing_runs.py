import logging

from biocloudcore.data_lake import DataLake
from biocloudcore.raw.nanopore.sequencing_runs import SequencingRuns
from biocloudcore.spark_session import get_environment, set_default_catalog, spark_session

logger = logging.getLogger(__name__)


def run():
    # job_name = Amplicons.__name__

    source_name = "nanopore"
    table_name = "sequencing_runs"
    layer = "raw"

    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    catalog = set_default_catalog(spark=spark, domain="csc")
    data_lake = DataLake(env=get_environment(spark))

    SequencingRuns(
        spark=spark, catalog=catalog, data_lake=data_lake, layer=layer, source=source_name, table_name=table_name
    ).run(
        bucket_name=data_lake.nanopore.landing_zone.get_bucket_name(),
        destination_path=data_lake.nanopore.raw.get_path(dataset_name=table_name),
    )


if __name__ == "__main__":
    run()
