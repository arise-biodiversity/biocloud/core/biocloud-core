from pyspark.sql import Catalog, SparkSession
from pyspark.sql.types import Row, StringType, StructField, StructType, TimestampType

from biocloudcore.common_dataframe_functions import (
    add_timestamp_columns,
)
from biocloudcore.data_lake import DataLake
from biocloudcore.utils.spark_utils import create_table_if_not_exists, upsert_to_delta_table


class Communicator:
    """
    The communicator allows our code to communicate errors and warnings with tables in databricks. This is useful
    for developers to have insights in failed runs, and product owners and data analists to check for the reasons
    their data was not processed. We specify four fields:
        1. the layer in which the error occured
        2. the source from which the error originated
        3. the table whose data caused the error
        4. a comment on the occured error.
    This data, including a timestamp, is then written to the communicator table. In databricks a dashboard has been
    created to easily find and visualise this data (and export it to csv).
    """

    def __init__(
        self, spark: SparkSession, catalog: Catalog, data_lake: DataLake, layer: str, source: str, table_name: str
    ) -> None:
        self.source_name: str = source
        self.table_name = table_name
        self.layer = layer

        self.data_lake = data_lake
        self.spark = spark
        self.catalog = catalog

        self.schema = StructType(
            [
                StructField("layer", StringType(), True),
                StructField("source", StringType(), True),
                StructField("table", StringType(), True),
                StructField("comment", StringType(), True),
                StructField("inserted_ts_utc", TimestampType(), True),
                StructField("updated_ts_utc", TimestampType(), True),
            ]
        )

        self.path = self.data_lake.communicator.raw.get_path(dataset_name="default")

    def communicate(self, reason: str):
        """Communicates the reason of failure to the communication table.

        :param reason (str): the reason a failure occured
        :param table_name (str, optional): optionally provide the table name. Defaults to None.
        """
        df = self.spark.createDataFrame(
            [
                Row(
                    layer=self.layer,
                    source=self.source_name,
                    table=self.table_name,
                    comment=reason,
                    inserted_ts_utc=None,
                    updated_ts_utc=None,
                )
            ],
            self.schema,
        )
        df = add_timestamp_columns(df)

        create_table_if_not_exists(
            self.spark,
            self.spark.catalog,
            schema=df.schema,
            table_name="communicator",
            path=self.path,
            database="default",
        )

        upsert_to_delta_table(
            self.spark,
            database="default",
            table_name="communicator",
            df=df,
            primary_key_columns=["layer", "source", "table", "comment"],
            static_columns=["inserted_ts_utc"],
            ignore_change_columns=["updated_ts_utc"],
            auto_schema_merge=True,
            optimize_table=True,
        )
