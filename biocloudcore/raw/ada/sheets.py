import logging

import pyspark.sql.functions as F
from pyspark.sql import Catalog, DataFrame, SparkSession

from biocloudcore.common_dataframe_functions import add_timestamp_columns
from biocloudcore.data_lake import DataLake
from biocloudcore.raw.ada.sheet_schema import schema as sheets_schema
from biocloudcore.raw.ada.validators import SheetsValidator as Validator
from biocloudcore.raw.common_s3_functions import find_metadata_files
from biocloudcore.raw.communicator import Communicator
from biocloudcore.utils.spark_utils import (
    create_dataframe_from_tsv,
    create_table_if_not_exists,
    upsert_to_delta_table,
)

logger = logging.getLogger(__name__)


class Sheets:
    """
    **Description**

    The sheets table contains all information that is provided by ada spreadsheets.

    The schema is flexible and we accept any changes in the schema from source. Only in enriched
    we will control our schema.

    **Schematic overview**

    .. uml::

    :landing_zone_json
    **path/to/landing-zone/sheets.tsv**;

    :raw_delta
    **path/to/raw/registrations/validated**
    **path/to/raw/registrations/errors**;

    """

    def __init__(
        self,
        spark: SparkSession,
        catalog: Catalog,
        data_lake: DataLake,
        layer: str,
        source: str,
        table_name: str,
        communicator: Communicator,
    ) -> None:
        self.source_name: str = source
        self.table_name = table_name
        self.layer = layer

        self.spark = spark
        self.catalog = catalog
        self.data_lake = data_lake

        self.communicator = communicator

    def run(self, bucket_name: str, destination_path: str) -> None:
        validator: Validator = Validator(self.spark)

        sheets_client = self.data_lake.sheets.get_client()

        # Get all the files the landing zone
        file_paths = find_metadata_files(
            sheets_client,
            bucket_name=bucket_name,
            file_prefix="upload",
        )

        if not file_paths:
            logger.info("No sheets to process.")

        # For every tsv file, create a dataframe and upsert it to the raw table
        for file_name in file_paths:
            logger.info(f"Now processing {file_name}.")
            # TODO: make sure that spark can read from 2 different storage accounts
            raw_df = create_dataframe_from_tsv(
                self.spark,
                s3_client=sheets_client,
                file_path=f"s3://{bucket_name}/{file_name}",
            )

            # Add the sheet name to the dataframe
            raw_df = raw_df.withColumn("sheet_name", F.lit(file_name.split("/")[-1]))

            raw_df = self._preprocess_sheet(raw_df)

            # Check if the columns of the sheet are what we expect. If not, we skip the sheet and move it.
            # We do this before the validator, as we simply cannot process this sheet.
            if not validator.is_processable(raw_df):
                logger.error(f"Sheet is not processable: {file_name}, {validator.reason}")
                self.communicator.communicate(f"{file_name}: {validator.reason}")
                if sheets_client.exists_object(bucket_name, file_name):
                    # Move the processed file from the "upload" to the "cannot_process" folder
                    sheets_client.move_objects(
                        source_bucket=bucket_name,
                        source_key=file_name,
                        destination_bucket=bucket_name,
                        destination_key=file_name.replace("upload/", "cannot_process/"),
                    )
                continue

            raw_df = raw_df.unionByName(self.spark.createDataFrame([], sheets_schema), allowMissingColumns=True)

            # validate the dataframe
            valid_df, invalid_df = validator.filter_invalid_rows(raw_df)

            # Define the paths for valid and invalid dataframes
            raw_dfs = [(valid_df, "validated"), (invalid_df, "errors")]

            valid_data_written = False
            # Write the dataframes to the delta table
            for df, valid_or_error in raw_dfs:
                if not df.isEmpty():
                    df_to_upsert = add_timestamp_columns(df)

                    create_table_if_not_exists(
                        self.spark,
                        self.catalog,
                        database=self.layer,
                        schema=df_to_upsert.schema,
                        table_name=f"{self.table_name}_{valid_or_error}",
                        path=f"{destination_path}/{valid_or_error}",
                    )

                    upsert_to_delta_table(
                        self.spark,
                        database=self.layer,
                        table_name=f"{self.table_name}_{valid_or_error}",
                        df=df_to_upsert,
                        primary_key_columns=["registration_number"],
                        static_columns=["inserted_ts_utc"],
                        ignore_change_columns=["updated_ts_utc", "inserted_ts_utc"],
                        auto_schema_merge=True,
                        optimize_table=True,
                    )

                    if valid_or_error == "validated":
                        valid_data_written = True

            if sheets_client.exists_object(bucket_name, file_name):
                # Move the processed file from the "upload" to the "processed" folder
                sheets_client.move_objects(
                    source_bucket=bucket_name,
                    source_key=file_name,
                    destination_bucket=bucket_name,
                    destination_key=file_name.replace("upload/", "processed/" if valid_data_written else "error/"),
                )

    def _preprocess_sheet(self, df: DataFrame):
        """Preprocesses the sheet by manipulating columns and values. Many data entries need
        some cleaning for it to be able to be processed in raw and enriched.

        :param df (DataFrame)
        :return DataFrame
        """
        # All column names should be lowercased
        df = df.toDF(*[column_name.lower() for column_name in df.columns])

        # Change some columns. FIXME: this needs to be changed in the sheets
        df = df.withColumnRenamed("bait/pheromones", "bait_pheromones")

        # TODO: The bioinformaticions requested this column back. Check with Judith.
        # df = df.drop("registration_type")

        # Drop all rows where ID is null. TODO: check whether this column exists
        if "id" in df.columns:
            df = df.dropna(subset=["id"])

        # Let all dates use the slash as separator
        df = df.withColumn("collecting_date_start", F.regexp_replace(F.col("collecting_date_start"), "-", "/"))
        df = df.withColumn("collecting_date_end", F.regexp_replace(F.col("collecting_date_end"), "-", "/"))

        """NOTA BENE: FROM HERE WE USE PANDAS FOR PREPROCESSING. Pandas allows for better string manipulation."""
        pandas_df = df.toPandas()

        # Use a for loop to iterate through rows
        for index, row in pandas_df.iterrows():
            pandas_df.at[index, "collecting_time_start"] = self._format_time(row["collecting_time_start"])
            pandas_df.at[index, "collecting_time_end"] = self._format_time(row["collecting_time_end"])
            pandas_df.at[index, "collecting_date_start"] = self._format_date(row["collecting_date_start"])
            pandas_df.at[index, "collecting_date_end"] = self._format_date(row["collecting_date_end"])

        return self.spark.createDataFrame(pandas_df)

    def _format_date(self, date_str: str) -> str:
        """
        Format the given date string into something we support down the road.
        For example, we change d:m:yyyy to dd:mm:yyyy.
        :param: date_str (str) that needs formatting
        :return str
        """
        try:
            parts = date_str.split("/")
            date_str = f"{parts[0].zfill(2)}/{parts[1].zfill(2)}/{parts[2]}"
        except Exception:
            return date_str
        return date_str.strip()

    def _format_time(self, time_str: str) -> str:
        """
        Format the given time string into something we support down the road.
        For example, we change h:mm:ss to hh:mm:ss.
        :param: time_str (str) that needs formatting
        :return str
        """
        try:
            parts = time_str.split(":")
            hours = parts[0].zfill(2)
            minutes = parts[1].zfill(2)
            seconds = parts[2].zfill(2) if len(parts) > 2 else "00"
            time_str = f"{hours}:{minutes}:{seconds}"
        except Exception:
            return time_str
        return time_str.strip()
