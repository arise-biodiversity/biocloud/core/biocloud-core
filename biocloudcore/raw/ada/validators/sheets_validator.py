import logging
import re

import pyspark.sql.functions as F
from pyspark.sql import DataFrame, SparkSession

from biocloudcore.raw.ada.sheet_schema import schema
from biocloudcore.raw.common_validator_functions import (
    check_column_value_match,
    column_has_unique_values,
    find_non_unique_values,
    is_valid_registration_number,
)
from biocloudcore.utils.spark_utils import (
    delta_to_df,
)

logger = logging.getLogger(__name__)


class SheetsValidator:
    def __init__(self, spark: SparkSession) -> None:
        self.reason: str
        self.spark = spark

    def is_processable(self, df: DataFrame) -> bool:
        """
        Check if the dataframe is processible by our code. We reject sheets that have duplicate columns or header names
        with unacceptable characters.

        :param df (DataFrame)
        :returns bool
        """
        # Check if the sheet has rows
        if df.isEmpty():
            self.reason = "Given dataframe is empty."
            return False

        # Check if we have the id column
        if "id" not in df.columns:
            self.reason = "Column 'id' not found."
            return False

        # Check if columns have the correct header names
        unacceptable_characters = [" ", "/", "-"]
        for col in df.columns:
            if any(char in col for char in unacceptable_characters):
                self.reason = f"Column '{col}' contains unacceptable character."
                return False

        # Check if the columns we received is a subset of the columns we expect
        if not self._is_subset(set(df.columns), set(schema.fieldNames())):
            self.reason = f"Unacceptable columns: {set(df.columns) - set(schema.fieldNames())}"
            return False

        # Check if the registration numbers are all unique
        if not column_has_unique_values("registration_number", df):
            self.reason = "Not all registration numbers are unique."
            non_unique_values_list = find_non_unique_values("registration_number", df)
            # And add them to our reason
            self.reason = f"Not all registration numbers are unique: {non_unique_values_list}"
            return False

        # Check if all registration numbers are valid
        unacceptable_registration_numbers: list[str] = []
        column_values = df.select("Registration_number").collect()
        for row in column_values:
            if not is_valid_registration_number(row[0], strict_mode=True):
                unacceptable_registration_numbers.append(row[0])

        if unacceptable_registration_numbers:
            self.reason = f"Registration numbers not accepted: {unacceptable_registration_numbers}"
            return False

        # Check if the given registration numbers already occur in the material entity table
        material_entity = delta_to_df(self.spark, db="enriched", table="material_entity")
        if material_entity:
            if check_column_value_match(
                df1=df, df2=material_entity, column1="registration_number", column2="catalog_number"
            ):
                self.reason = "Registration number(s) already occur in the material entity table."
                return False

            # Check if the sheet already appears in the material entity table
            if check_column_value_match(df1=df, df2=material_entity, column1="sheet_name", column2="dataset_name"):
                self.reason = "Sheet already in the material entity table."
                return False

        # Check if the dates we receive are acceptable
        for date_column in ["collecting_date_start", "collecting_date_end"]:
            for row in df.select(date_column).collect():
                received_date: str = row[0]
                if received_date:
                    # We only accept dd/mm/yyyy, so we check for the correct format.
                    match = re.findall(r"^(\d{2})/(\d{2})/(\d{4})$", received_date)
                    if match:
                        try:
                            day = int(match[0][0])
                            month = int(match[0][1])
                            # Check if days and months are within valid ranges
                            if day > 31 or month > 12:
                                self.reason = f"Column {date_column} has invalid days/months: '{received_date}'"
                                return False
                            if not self._day_possible_in_month(day, month):
                                self.reason = f"Given month does not have that many days: '{received_date}'"
                                return False
                        except ValueError:
                            self.reason = f"Column {date_column} cannot be processed: '{received_date}'"
                            return False
                    else:
                        self.reason = f"Column {date_column} has wrong format: '{received_date}'"
                        return False

        # Check if the timestamps we receive are acceptable
        for time_column in ["collecting_time_start", "collecting_time_end"]:
            for row in df.select(time_column).collect():
                received_timestamp: str = row[0]
                if received_timestamp:
                    # We accept timestamps with or without seconds, and with or without prefixed 0.
                    # e.g. hh:mm, h:mm, hh:mm:ss, h:mm:ss, h:m:s.
                    match = re.findall(r"^(\d{2}):(\d{2})(:(\d{2}))?$", received_timestamp)
                    if match:
                        try:
                            hours = match[0][0]
                            minutes = match[0][1]
                            seconds = match[0][2] if len(match) == 3 else ""
                            # Check if hours and minutes are within valid ranges
                            if int(hours) > 23 or int(minutes) > 59:
                                self.reason = f"Column {time_column} has invalid hours/minutes: '{received_timestamp}'"
                                return False
                            # Check if seconds (if provided) are within valid range
                            if seconds and int(seconds) > 59:
                                self.reason = f"Column {time_column} has invalid seconds: '{received_timestamp}'"
                                return False
                        except ValueError:
                            self.reason = f"Column {time_column} cannot be processed: '{received_timestamp}'"
                            return False
                    else:
                        self.reason = f"Column {time_column} has wrong format: '{received_timestamp}'"
                        return False

        # # Check if the counts are indeed integers (we allow null values)
        # if not self._column_has_type(df, "Count", LongType()):
        #     return False

        # # Check if the coordinates are indeed decimals (we allow null values)
        # for column in ["Decimal_Latitude_WGS84", "Decimal_Longitude_WGS84"]:
        #     if not self._column_has_type(df, column, DecimalType()):
        #         return False

        # Check if the columns have values that are compatible with their schema defined dtypes
        df_types = dict(df.dtypes)
        for column, dtype in df_types.items():
            expected_type = schema[column].dataType
            if not self._column_has_type(df, column, expected_type):
                self.reason = f"Column: {column} has wrong types. Expected {str(expected_type)}, received {dtype}."
                return False

        return True

    def filter_invalid_rows(self, df: DataFrame) -> tuple[DataFrame, DataFrame]:
        """
        Validates the individual rows of the dataframe. Those that are acceptable are put in the
        valid records dataframe. The others in the invalid dataframe. Both are returned.

        :param df (DataFrame)
        :returns tuple[DataFrame, DataFrame]
        """
        # Reject rows with an empty Registration_number
        required_columns = ["registration_number"]
        valid_records = df.dropna(subset=required_columns)
        invalid_records = df.exceptAll(valid_records).withColumn(
            "rejection_reason", F.lit("Registration_number has null value")
        )

        return valid_records, invalid_records

    def _is_subset(self, list1: list | set, list2: list | set) -> bool:
        """
        Checks if the first list or set is a subset of the second list or set.
        :param list1 (list | set)
        :param list2 (list | set)
        :return bool
        """
        return all(item in list2 for item in list1)

    def _column_has_type(self, df: DataFrame, column: str, expected_type) -> bool:
        """Checks if the given column has the given type.

        :param df (DataFrame)
        :param column (str)
        :param expected_type (_type_)
        :returns bool
        """
        invalid_rows = df.filter((F.col(column).isNotNull()) & (F.col(column).try_cast(expected_type).isNull()))
        if invalid_rows.count() > 0:
            self.reason = f"Column {column} has values that are not of {expected_type}"
            return False

        return True

    def _day_possible_in_month(self, day: int, month: int) -> bool:
        """
        Checks whether the combination of day and month is possible. For example, we reject November 31.
        :param day (int)
        :param month (int)
        :return bool
        """
        max_days = {month: [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month - 1]}
        return day <= max_days[month]
