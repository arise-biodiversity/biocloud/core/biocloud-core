import pyspark.sql.functions as F
from pyspark.sql import DataFrame, SparkSession

from biocloudcore.common_dataframe_functions import column_values_to_list
from biocloudcore.raw.common_validator_functions import (
    filter_list_from_dataframe_column,
    find_non_unique_values,
    is_valid_registration_number,
)
from biocloudcore.utils.spark_utils import (
    delta_to_df,
)


class RegistrationsValidator:
    def __init__(self, spark: SparkSession) -> None:
        self.reason: str
        self.spark = spark

    def is_processable(self, df: DataFrame) -> bool:
        """
        Check if the dataframe is processable by our code. We reject dumps that have duplicate catalog numbers

        :param df (DataFrame)
        :returns bool
        """
        # Check if all required columns are in the dump
        required_columns: list[str] = ["ContainerCode", "Id", "UserId"]
        if not any(element in df.columns for element in required_columns):
            return False

        # Check if the dataframe has rows
        if df.isEmpty():
            self.reason = "Given dataframe is empty."
            return False

        return True

    def filter_invalid_rows(self, valid_df: DataFrame) -> tuple[DataFrame, DataFrame]:
        """
        Validates the incoming dataframe by moving invalid rows to the invalid_df. All remaining
        rows are deemed valid and returned as the valid_df. We always filter AWAY. Our valid dataframe
        remains our valid dataframe, from which invalid entries are filtered. Results from an applied filter
        are always removed from the valid dataframe.
        :param valid_df (DataFrame)
        :return tuple[DataFrame, DataFrame] with valid and invalid dataframes
        """
        # Create an empty dataframe in which we will put our invalid entries
        invalid_df = self.spark.createDataFrame([], valid_df.schema)
        invalid_df = invalid_df.withColumn("rejection_reason", F.lit(None))

        # Removes any duplicate registration numbers from the dataframe
        non_unique_values_list = find_non_unique_values("ContainerCode", valid_df)
        valid_df, new_invalid_df = filter_list_from_dataframe_column(valid_df, "ContainerCode", non_unique_values_list)
        new_invalid_df = new_invalid_df.withColumn("rejection_reason", F.lit("Duplicate registration number"))
        invalid_df = invalid_df.unionByName(new_invalid_df, allowMissingColumns=True)

        # Remove any invalid registration numbers from the dataframe
        registration_numbers = column_values_to_list(valid_df, "ContainerCode")
        invalid_registration_numbers: list[str] = [
            registration_number
            for registration_number in registration_numbers
            if not is_valid_registration_number(registration_number, strict_mode=True)
        ]
        valid_df, new_invalid_df = filter_list_from_dataframe_column(
            valid_df, "ContainerCode", invalid_registration_numbers
        )
        new_invalid_df = new_invalid_df.withColumn("rejection_reason", F.lit("Invalid registration number"))
        invalid_df = invalid_df.unionByName(new_invalid_df, allowMissingColumns=True)

        # Remove registration numbers that already occur in the material entity table
        material_entity = delta_to_df(self.spark, db="enriched", table="material_entity")
        if material_entity:
            registration_numbers = column_values_to_list(material_entity, "catalog_number")
            # We do not want the registration numbers in the list,
            # so our valid dataframe has values that are outside the filter
            valid_df, invalid_df = filter_list_from_dataframe_column(valid_df, "ContainerCode", registration_numbers)
            new_invalid_df = new_invalid_df.withColumn(
                "rejection_reason", F.lit("Registration number already in material entity")
            )
            invalid_df = invalid_df.unionByName(new_invalid_df, allowMissingColumns=True)

        return valid_df, invalid_df
