import logging
from concurrent.futures import ThreadPoolExecutor, as_completed

import pyspark.sql.functions as F
from pandas.core.interchange.dataframe_protocol import DataFrame
from pyspark import Row

from biocloudcore.data_lake import S3Client

logger = logging.getLogger(__name__)


def find_metadata_files(s3_client: S3Client, bucket_name: str, file_prefix: str) -> list:
    """
    Find all metadata files in the specified S3 bucket and prefix and return the
    S3 paths to the files.

    :param S3Client s3_client: The S3 service client used to list objects.
    :param str bucket_name: The name of the S3 bucket.
    :param str file_prefix: The prefix path within the S3 bucket.
    :return: list of S3 paths to the metadata files.
    """
    return [
        file_name
        for file_name in s3_client.list_objects(bucket_name=bucket_name, prefix=file_prefix, only_files=True)
        # Filter out temporary files
        if "~" not in file_name
    ]


def find_metadata_file_path(s3_client: S3Client, bucket_name: str, file_prefix: str, entity: str) -> str:
    """
    Find a metadata file for a given entity in the specified S3 bucket and prefix and return the
    S3 path to the file.

    :param S3Client s3_client: The S3 service client used to list objects.
    :param str bucket_name: The name of the S3 bucket.
    :param str file_prefix: The prefix path within the S3 bucket.
    :param str entity: The entity name to search for in the metadata files.
    :return: The S3 path to the metadata file.
    :raises MetadataNotFoundError: If the metadata file for the specified entity is not found.
    """

    s3_uris = s3_client.list_objects(bucket_name=bucket_name, prefix=file_prefix, only_files=True, full_uri=True)

    file_uri = next((file_path for file_path in s3_uris if entity in file_path), None)

    if file_uri:
        logger.info(f"Metadata file found for {entity} in {bucket_name}")
        return file_uri
    else:
        logger.error(f"Metadata file not found for {entity} in {bucket_name}")
        raise Exception(f"The metadata file for entity: {entity} is not found in the landing zone.")


def get_device_folders_with_payload(
    s3_client: S3Client, source: str, bucket: str, prefix: str, device_limit: int
) -> list[str]:
    """
    Retrieve the device folders containing payload data to process from the specified S3 bucket.

    :param s3_client: The S3 client instance used for interacting with the S3 service.
    :param source: A descriptive source name for logging purposes.
    :param bucket: The name of the S3 bucket where the device folders are located.
    :param prefix: The S3 prefix to filter folders by (e.g., a base folder path).
    :param device_limit: The maximum number of device folders to process. If None, all folders will be processed.
    :return: A list of device folder paths to process.
    """
    devices = []
    if device_limit == 0:
        logger.warning(
            f"No payload folders will be processed. "
            f"Set a non-zero value for {source} max_devices_to_process to process payload."
        )
    else:
        devices = s3_client.list_objects(bucket_name=bucket, prefix=prefix, only_folders=True)
        logger.warning(f"Found {len(devices)} device folders.")

        if len(devices) == 0:
            logger.warning(f"No folders found in {prefix}. Nothing to process.")
        elif device_limit and device_limit > 0:
            devices = devices[:device_limit]
            logger.warning(f"Processing only the first {device_limit} device folders.")
        else:
            logger.warning("All selected devices will be processed as there is no device limit.")

    return devices


def upload_files_to_blob(
    s3_client: S3Client,
    max_upload_workers: int,
    source_bucket: str,
    destination_blob: str,
    payload_metadata_df: DataFrame,
) -> DataFrame:
    """
    Upload payload files from the source bucket to blob storage in parallel, reporting upload success or failure.

    :param s3_client: The S3 client instance used for interacting with the S3 service.
    :param max_upload_workers: The maximum number of threads that can simultaneously upload
    :param source_bucket: The name of the source bucket in S3.
    :param destination_blob: The destination blob storage for uploading files.
    :param payload_metadata_df: A Spark DataFrame containing metadata for the files to be uploaded.
    :return: A DataFrame containing the original file metadata with an additional column `upload_success` indicating
            the status of each file upload (True if successful, False otherwise).
    """
    copied_count = 0
    skipped_count = 0
    failed_count = 0

    def upload_file(row):
        nonlocal copied_count, skipped_count, failed_count
        source_path = row["source_path"]
        blob_path = row["blob_path"]
        try:
            # Upload the file and return the status
            status = _write_payload_in_blob_storage(
                s3_client=s3_client,
                source_bucket=source_bucket,
                destination_blob=destination_blob,
                source_path=source_path,
                destination_path=blob_path,
            )
            if status == "copied":
                copied_count += 1
            elif status == "skipped":
                skipped_count += 1
            else:
                failed_count += 1
            return True  # File uploaded successfully
        except Exception:
            failed_count += 1
            logger.error(f"Failed to upload {source_path}")
            return False  # File upload failed

    # Convert Spark DataFrame to Pandas DataFrame for easier iteration
    file_metadata = payload_metadata_df.toPandas()

    # Run the file upload operation in parallel using ThreadPoolExecutor
    with ThreadPoolExecutor(max_workers=max_upload_workers) as executor:
        results = list(executor.map(upload_file, file_metadata.to_dict("records")))

    # Log the final upload status counts
    logger.warning(
        f"Upload completed: {copied_count} files copied, {skipped_count} files skipped, {failed_count} files failed."
    )

    # Add a new column to the DataFrame to indicate upload success/failure
    result_df = payload_metadata_df.withColumn("upload_success", F.array(*[F.lit(res) for res in results]))

    return result_df.withColumn("upload_success", result_df["upload_success"].getItem(0))


def move_files_to_processed_folder(
    s3_client: S3Client, max_upload_workers: int, source_bucket_name: str, uploaded_files: list[Row]
):
    """
    Move uploaded files to the 'processed' folder in the same source bucket.

    :param s3_client: The S3 client instance used for interacting with the S3 service.
    :param max_upload_workers: The maximum number of threads that can upload simultaneously
    :param source_bucket_name: The name of the source S3 bucket where the files are stored.
    :param uploaded_files: A list of uploaded file metadata (including source paths).
    :return: None
    """

    def move_file(s3_client, source_bucket, source_path, destination_path):
        # Move the file (copy and then delete it)
        s3_client.move_objects(
            source_bucket=source_bucket,
            destination_bucket=source_bucket,
            source_key=source_path,
            destination_key=destination_path,
        )

    logger.warning(f"Copying {len(uploaded_files)} uploaded files to the processed folder.")

    # Use ThreadPoolExecutor to handle file move operations concurrently
    with ThreadPoolExecutor(max_workers=max_upload_workers) as executor:
        futures = []

        # Submit move tasks to the executor for each file
        for row in uploaded_files:
            source_path = row.source_path
            destination_path = source_path.replace("media/", "media/processed/")
            futures.append(executor.submit(move_file, s3_client, source_bucket_name, source_path, destination_path))

        # Wait for all futures to complete and handle errors
        for future in as_completed(futures):
            future.result()  # This will re-raise any exception that occurred in the thread

    logger.warning(
        f"Successfully moved {len(uploaded_files)} files to the 'processed' folder. The payload is processed."
    )


def _write_payload_in_blob_storage(
    s3_client: S3Client, source_bucket: str, destination_blob: str, source_path: str, destination_path: str
) -> str:
    """
    Copy a file from the landing zone to blob storage if it doesn't already exist in the destination.

    :param s3_client: The S3 client instance used for interacting with the S3 service.
    :param source_bucket: The name of the source bucket in S3.
    :param destination_blob: The name of the destination blob storage.
    :param source_path: The key (path) of the source file in the source bucket.
    :param destination_path: The key (path) where the file should be copied in the destination blob.

    :return: A status string: 'copied', 'skipped', or 'failed'.
            - 'copied' if the file was successfully copied.
            - 'skipped' if the file already exists in the destination blob.
            - 'failed' if an error occurred during the copy operation.
    """
    try:
        if s3_client.exists_object(destination_blob, destination_path):
            logger.info(f"Skipping copy of {source_path}. File already exists in {destination_blob}.")
            return "skipped"
        else:
            s3_client.copy_object(
                source_bucket=source_bucket,
                destination_bucket=destination_blob,
                source_key=source_path,
                destination_key=destination_path,
            )
        return "copied"
    except Exception as e:
        logger.error(
            f"Error while copying object {source_path} from {source_bucket} "
            f"to {destination_path} in {destination_blob}: {str(e)}"
        )
        return "failed"
