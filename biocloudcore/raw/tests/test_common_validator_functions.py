import pytest

import biocloudcore.raw.tests.data.common_validator_functions_data as data
from biocloudcore.raw.common_validator_functions import (
    column_has_unique_values,
    find_non_unique_values,
    is_valid_registration_number,
)
from biocloudcore.utils.test_tools.test_dataset import TestDataset


@pytest.fixture(scope="module")
def test_dataframes(spark):
    test_dfs = [
        TestDataset(
            "pokedex_with_duplicate_names",
            data.pokedex_with_duplicate_names["INPUT_DATA"],
            data.pokedex_with_duplicate_names["INPUT_SCHEMA"],
        ),
        TestDataset(
            "pokedex_without_duplicate_names",
            data.pokedex_without_duplicate_names["INPUT_DATA"],
            data.pokedex_without_duplicate_names["INPUT_SCHEMA"],
        ),
    ]

    return {dataset.filename: spark.createDataFrame(dataset.data, dataset.schema) for dataset in test_dfs}


def test_column_has_unique_values(test_dataframes) -> None:
    df_without_duplicates = test_dataframes["pokedex_without_duplicate_names"]
    df_with_duplicates = test_dataframes["pokedex_with_duplicate_names"]

    has_unique_values: bool = column_has_unique_values("name", df_without_duplicates)
    assert has_unique_values is True

    has_unique_values: bool = column_has_unique_values("name", df_with_duplicates)
    assert has_unique_values is False


def test_find_non_unique_values(test_dataframes) -> None:
    df_without_duplicates = test_dataframes["pokedex_without_duplicate_names"]
    df_with_duplicates = test_dataframes["pokedex_with_duplicate_names"]

    non_unique_values: list[str] = find_non_unique_values("name", df_without_duplicates)
    assert not non_unique_values

    non_unique_values: list[str] = find_non_unique_values("name", df_with_duplicates)
    assert "Pikachu" in non_unique_values


def test_is_valid_registration_number() -> None:
    registration_number_tuples = data.registration_numbers

    for tuple in registration_number_tuples:
        assert is_valid_registration_number(tuple[0], True) is tuple[1]
