from pyspark.sql.types import StringType, StructField, StructType

pokedex_without_duplicate_names = {
    "INPUT_SCHEMA": StructType(
        [
            StructField("name", StringType(), True),
            StructField("type", StringType(), True),
        ]
    ),
    "INPUT_DATA": [
        {"name": "Pikachu", "type": "Electric"},
        {"name": "Charmander", "type": "Fire"},
    ],
}

pokedex_with_duplicate_names = {
    "INPUT_SCHEMA": StructType(
        [
            StructField("name", StringType(), True),
            StructField("type", StringType(), True),
        ]
    ),
    "INPUT_DATA": [
        {"name": "Pikachu", "type": "Electric"},
        {"name": "Pikachu", "type": "Electric"},
        {"name": "Charmander", "type": "Fire"},
    ],
}

registration_numbers = [
    ("L.3236127", True),
    ("L  0051628", True),
    ("U.1373083", True),
    ("U.1748153", True),
    ("U  0052290", True),
    ("WAG0369305", True),
    ("WAG.1669093", True),
    ("AMD.120162", True),
    ("AMD.45295", True),
    ("AMD0000103", True),
    ("L1608329", False),
    ("L.0969317", False),
    ("L. 1608329", False),
    ("U.0568902", False),
    ("U.  1608329", False),
    ("WAG  1669093", False),
    ("WAG.0279347", False),
    ("AMD  120162", False),
    ("L.12a3", False),
    ("U.34x5v", False),
    ("XYZ.123", False),
]
