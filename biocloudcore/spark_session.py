import getpass
import logging

from pyspark.sql import Catalog, SparkSession

from databricks.sdk import WorkspaceClient


def spark_session(spark_logging_level: int = logging.WARNING) -> SparkSession:
    """
    Create a SparkSession object for the Databricks workspace. If running locally, use the DatabricksSession builder.

    :param spark_logging_level: The logging level to set for PySpark and other system libraries.
    The standard log level is logging.WARNING to prevent spam logs from Databricks and Spark.
    By setting the log level to another level when initializing the SparkSession,
    we can control the log level for the entire session.

    :return: The SparkSession object.
    """

    configure_pyspark_logging(spark_logging_level=spark_logging_level)

    # If the getpass user starts with "spark", we're running in Databricks
    if "spark" in getpass.getuser():
        spark = SparkSession.builder.getOrCreate()
    # If we're running locally, we can use the DatabricksSession builder to create a SparkSession
    else:
        # If so, use the DatabricksSession builder to create a SparkSession
        from databricks.connect import DatabricksSession

        spark = DatabricksSession.builder.profile("development").serverless().getOrCreate()

    return spark


def get_current_user(spark: SparkSession) -> str:
    """Get the current user running the code in Databricks workspace"""
    current_user = spark.sql("SELECT current_user()").collect()[0][0]
    if not current_user:
        raise Exception("Could not determine the current user from the Spark session.")
    return current_user


def get_environment(spark: SparkSession) -> str:
    """Get the environment in which the code is being run from Databricks workspace"""
    workspace_name = spark.conf.get("spark.databricks.workspaceUrl")
    if workspace_name == "dbc-f85138d6-9600.cloud.databricks.com":
        env = "production"
    elif workspace_name == "dbc-e111fa3c-c5db.cloud.databricks.com":
        env = "development"
    else:
        raise Exception("Could not determine the Databricks workspace from the Spark session.")
    return env


def set_default_catalog(spark: SparkSession, domain: str) -> Catalog:
    """
    Set the default catalog to the specified domain.
    :param spark: The SparkSession object.
    :param domain: The domain to set as the default catalog.
    :return: The Catalog object.
    """
    environment = get_environment(spark)
    spark.catalog.setCurrentCatalog(f"{domain}_{environment}")
    return spark.catalog


def configure_pyspark_logging(spark_logging_level: int) -> None:
    """
    Configure the logging level for PySpark and other system libraries.

    This function sets the logging level for various libraries at the initialization of the SparkSession.
    Custom logging levels for user-defined logs can be set separately in the run file.

    :param spark_logging_level: The logging level to set for PySpark and other libraries.
    """
    libraries = [
        "boto3",
        "botocore",
        "py4j",
        "pyspark",
        "databricks.sdk",
        "databricks.connect",
        "urllib3.connectionpool",
    ]

    for lib in libraries:
        logging.getLogger(lib).setLevel(level=spark_logging_level)


def get_dbutils():
    # Authenticate with the workspace on our configured profile
    profile = "databricks" if "spark" in getpass.getuser() else "development"

    w = WorkspaceClient(profile=profile)
    return w.dbutils


def get_databricks_secret(key: str, domain: str, env: str) -> str:
    """Get a secret from the Databricks secret store"""
    return get_dbutils().secrets.get(scope=f"{domain}_{env}", key=key)
