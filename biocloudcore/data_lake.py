import dataclasses
import logging

import boto3
from botocore.client import Config
from botocore.exceptions import ClientError, NoCredentialsError, PartialCredentialsError

from biocloudcore.spark_session import get_databricks_secret


class DataLake:
    def __init__(self, env: str):
        """
        Initialize the DataLake class

        :param env: The environment in which to run (production | development)
        """
        self.env = env
        self._initialize_environment()
        self.storage_stages = StorageStages()
        self._initialize_storage_accounts()
        self._initialize_datasets()
        # self._initialize_blob_datasets()
        # self.blob_clients = self._initialize_blob_clients()

        # self.anonymizer = self._initialize_anonymization()
        # self.telemetry = self._initialize_telemetry()
        # self.api_keys = self._initialize_api_keys()

    def _initialize_environment(self):
        """
        Initialize all parameters related to the environment (connections, id's, etc)

        :return: None
        """

        # TODO: Initialize all the needed keys and other stuff here
        self.tenant_id = "xxxxxxxxxxxxxxxxxxxx"

    # def _initialize_api_keys(self) -> ApiKeys:
    #     return ApiKeys(
    #         openweathermap=self._safe_get_secret(
    #             "dataplatform-openweathermap-api-key-{}".format(self.env), ""
    #         ),
    #         here=self._safe_get_secret(
    #             "dataplatform-here-api-key-{}".format(self.env), ""
    #         ),
    #         airship=Credentials(
    #             client_id=self._safe_get_secret(
    #                 "dataplatform-airship-api-id-{}".format(self.env), ""
    #             ),
    #             client_secret=self._safe_get_secret(
    #                 "dataplatform-airship-api-key-{}".format(self.env), ""
    #             ),
    #         ),
    #     )

    # def _initialize_blob_clients(self) -> BlobClients:
    #     return BlobClients(
    #         occupancy_extrapolation_sandbox_model=AzureBlobClient(
    #             account_name=self.storage_account_derived.name,
    #             account_key=self.storage_account_derived.key,
    #             container_name="occupancy",
    #             blob_name="extrapolation_model/sandbox/latest/model.pickle",
    #         ),
    #         ebs_raw_blobclient=AzureBlobClient(
    #             account_name=self.storage_account_raw.name,
    #             account_key=self.storage_account_raw.key,
    #             container_name="ebs",
    #             blob_name="{}",
    #         ),
    #         network_changes_scheduler_config=AzureBlobClient(
    #             account_name=self.storage_account_derived.name,
    #             account_key=self.storage_account_derived.key,
    #             container_name="network",
    #             blob_name="network_change_scheduler_config.json",
    #         ),
    #     )

    def _initialize_storage_accounts(self):
        """
        Initialize all storage accounts related to the data lake

        :return: None
        """
        self.biocloud = StorageAccount(
            name=f"biocloud-core-{self.env}",
            key_id=get_databricks_secret(key="S3_ACCESS_KEY_ID", domain="biocloud", env=self.env),
            key_secret=get_databricks_secret(key="S3_SECRET_ACCESS_KEY", domain="biocloud", env=self.env),
            region_name="eu-west-1",
        )
        self.csc = StorageAccount(
            name=f"core-sequence-cloud-{self.env}",
            key_id=get_databricks_secret(key="S3_ACCESS_KEY_ID", domain="csc", env=self.env),
            key_secret=get_databricks_secret(key="S3_SECRET_ACCESS_KEY", domain="csc", env=self.env),
            region_name="eu-north-1",
        )

        self.sheets = StorageAccount(
            name=f"core-sequence-cloud-{self.env}",
            key_id=get_databricks_secret(key="SHEET_S3_ACCESS_KEY_ID", domain="csc", env=self.env),
            key_secret=get_databricks_secret(key="SHEET_S3_SECRET_ACCESS_KEY", domain="csc", env=self.env),
            region_name="eu-north-1",
        )

        # TODO: PII?
        # self.storage_account_pii = StorageAccount(
        #     name="dataplatformpiisa{}".format(self.env),
        #     key=self._safe_get_secret(
        #         "dataplatform-sa-pii-key-{}".format(self.env), ""
        #     ),
        # )

    def _initialize_datasets(self):
        """
        Set all blob datasets related to the data lake.
        Each blob is represented by an instance of the Dataset class.

        :return: None
        """

        self.diopsis = Dataset(
            paths={
                self.storage_stages.LANDING_ZONE: PathStructure(
                    storage_account=self.biocloud,
                    container="",
                    base_path="",
                ),
            }
        )

        self.nimbo_diopsis = Dataset(
            paths={
                self.storage_stages.BLOB: PathStructure(
                    storage_account=self.biocloud,
                    container="nimbostratus",
                    base_path="diopsis",
                ),
                self.storage_stages.LANDING_ZONE: PathStructure(
                    storage_account=self.biocloud,
                    container="",
                    base_path="",
                ),
                self.storage_stages.RAW: PathStructure(
                    storage_account=self.biocloud,
                    container="nimbostratus",
                    base_path="diopsis",
                ),
                self.storage_stages.ENRICHED: PathStructure(
                    storage_account=self.biocloud,
                    container="nimbostratus",
                    base_path="",
                ),
            }
        )

        self.dsi = Dataset(
            paths={
                self.storage_stages.CURATED: PathStructure(
                    storage_account=self.biocloud,
                    container="biocloud",
                    base_path="dsi",
                ),
            }
        )

        self.dna = Dataset(
            paths={
                self.storage_stages.RAW: PathStructure(
                    storage_account=self.csc,
                    container="csc",
                    base_path="",
                ),
                self.storage_stages.ENRICHED: PathStructure(
                    storage_account=self.csc,
                    container="csc",
                    base_path="",
                ),
                self.storage_stages.CURATED: PathStructure(
                    storage_account=self.csc,
                    container="csc",
                    base_path="service_layer",
                ),
            }
        )

        self.nanopore = Dataset(
            paths={
                self.storage_stages.LANDING_ZONE: PathStructure(
                    storage_account=self.csc,
                    container="nanopore",
                    base_path="",
                ),
                self.storage_stages.RAW: PathStructure(
                    storage_account=self.csc,
                    container="csc",
                    base_path="nanopore",
                ),
            }
        )

        self.ada = Dataset(
            paths={
                self.storage_stages.LANDING_ZONE: PathStructure(
                    storage_account=self.csc,
                    container="adadump",
                    base_path="",
                ),
                self.storage_stages.RAW: PathStructure(
                    storage_account=self.csc,
                    container="csc",
                    base_path="ada",
                ),
            }
        )

        self.ada_sheets = Dataset(
            paths={
                self.storage_stages.REGISTRATIONS: PathStructure(
                    storage_account=self.sheets,
                    container="",
                    base_path="",
                ),
                self.storage_stages.RAW: PathStructure(
                    storage_account=self.csc,
                    container="csc",
                    base_path="ada",
                ),
            }
        )

        self.communicator = Dataset(
            paths={
                self.storage_stages.RAW: PathStructure(
                    storage_account=self.csc,
                    container="communicator",
                    base_path="",
                )
            }
        )


class StorageAccount:
    """
    A class representing an azure storage account

    :param name: The name of the storage account
    :param key: The access key of the storage account
    """

    def __init__(self, name: str, key_id: str, key_secret: str, region_name: str):
        self.name = name
        self.key_id = key_id
        self.key_secret = key_secret
        self.region_name = region_name

        self.client = S3Client(
            access_key_id=self.key_id,
            secret_access_key=self.key_secret,
            region_name=self.region_name,
        )

    def get_client(self):
        return self.client

    def get_custom_client(self, max_pool_connections: int):
        """Update the max_pool_connections and recreate the client."""
        self.client = S3Client(
            access_key_id=self.key_id,
            secret_access_key=self.key_secret,
            region_name=self.region_name,
            max_pool_connections=max_pool_connections,
        )
        return self.client


class PathStructure:
    def __init__(self, storage_account: StorageAccount, container: str, base_path: str):
        self.storage_account = storage_account
        self.container = container
        self.base_path = base_path

    def get_full_path(
        self,
        stage: str,
        date_string: str | None = None,
        dataset_name: str = "",
        subdir: str = "",
        filename: str | None = None,
    ) -> str:
        relative_path = (
            f"{self.container + '/' if self.container else ''}{self.base_path + '/' if self.base_path else ''}"
            f"{dataset_name + '/' if dataset_name else ''}{subdir + '/' if subdir else ''}"
            f"{date_string + '/' if date_string else ''}"
        )
        full_path = f"s3://{self.storage_account.name}-{stage}/{relative_path}"
        if filename:
            full_path += filename
        return full_path

    def get_path(
        self,
        stage: str,
        date_string: str | None = None,
        dataset_name: str = "",
        subdir: str = "",
        filename: str | None = None,
    ) -> str:
        return self.get_full_path(stage, date_string, dataset_name, subdir, filename)


class Dataset:
    def __init__(self, paths: dict[str, PathStructure]):
        self.paths = paths
        for stage, path_structure in self.paths.items():
            setattr(self, stage.replace("-", "_"), Stage(stage, path_structure, self))

    def check_valid_stage(self, stage):
        if stage not in list(self.paths.keys()):
            raise KeyError(
                f"The stage {stage} was not found for this dataset. "
                f"Available stages are {self.get_available_stages()}"
            )

    def get_available_stages(self):
        return list(self.paths.keys())


class Stage:
    def __init__(self, name: str, path_structure: PathStructure, dataset: Dataset):
        self.name = name
        self.path_structure = path_structure
        self.dataset = dataset

    def get_path(
        self, date_string: str | None = None, dataset_name: str = "", subdir: str = "", filename: str | None = None
    ) -> str:
        """
        Get the full path to a dataset.

        :param stage: The stage of the data [raw, pii, clean, derived, export, tmp]
        :param date_string: Optional suffix to the path (get all files in YYYY/MM/DD folder structure)
        :param dataset_name: Optional name of the dataset, to be used when multiple tables are grouped under a sin
        gle instance
        :param subdir: Optional subdirectory to append to the path in the dataset folder
        :param filename: Optional filename of a single specific file to read or group of files (using * wildcards)
        :return: The path to the dataset
        """

        self.dataset.check_valid_stage(self.name)
        return self.path_structure.get_path(self.name, date_string, dataset_name, subdir, filename)

    def get_delta_path(self, table_name: str) -> str:
        """
        Get the path to a delta table
        """
        if self.name == "raw":
            if "validated" in table_name:
                return self.get_path(dataset_name=table_name, subdir="validated")
            elif "errors" in table_name:
                return self.get_path(dataset_name=table_name, subdir="errors")
            else:
                raise Exception(f"Table {table_name} is not a valid raw table")
        elif self.name == "enriched":
            return self.get_path(dataset_name=table_name, subdir="table")
        else:
            raise Exception(f"Stage {self.name} does not have delta tables")

    def get_storage_account_name(self, stage: str) -> str:
        """
        Example: containername@storageaccount.blob.core.windows.net/path/to/data => storageaccount

        :param stage: The stage of the data [raw, pii, clean, derived, export]
        """
        self.dataset.check_valid_stage(self.name)
        return self.path_structure.storage_account.name

    def get_bucket_name(self) -> str:
        """
        Example: containername@storageaccount.blob.core.windows.net/path/to/data => storageaccount

        :param stage: The stage of the data [raw, pii, clean, derived, export]
        """
        self.dataset.check_valid_stage(self.name)
        return f"{self.get_storage_account_name(self.name)}-{self.name}"

    def get_container_name(self) -> str:
        """
        Example: containername@storageaccount.blob.core.windows.net/path/to/data => containername

        :param stage: The stage of the data [raw, pii, clean, derived, export]
        :raises KeyError: Invalid stage is given
        """
        self.dataset.check_valid_stage(self.name)
        return self.path_structure.container

    def get_client(self):
        return self.path_structure.storage_account.get_client()

    """
    TODO: Nice to have method
    def get_relative_path(self, stage: str) -> str:
        \"\"\"
        Example: containername@storageaccount.blob.core.windows.net/path/to/data => path/to/data

        :param stage: The stage of the data [raw, pii, clean, derived]
        \"\"\"
        self._check_valid_stage(stage)
        try:
            return (
                f"{self.paths[stage].container}/{self.paths[stage].base_path}/{self.paths[stage].dataset_name}/
                {self.paths[stage].date_string}"
            )
        except IndexError:
            return ""
    """


class S3Client:
    """
    A class representing an S3 client
    """

    def __init__(self, access_key_id: str, secret_access_key: str, region_name: str, max_pool_connections: int = 20):
        self.access_key_id = access_key_id
        self.secret_access_key = secret_access_key
        self.region_name = region_name

        self.client = self.create_client(
            aws_access_key_id=access_key_id,
            aws_secret_access_key=secret_access_key,
            region_name=region_name,
            max_pool_connections=max_pool_connections,
        )

    def get_client(self):
        return self.client

    @staticmethod
    def create_client(
        aws_access_key_id: str = None,
        aws_secret_access_key: str = None,
        region_name: str = "eu-west-1",
        max_pool_connections: int = 20,
    ) -> boto3.client:
        """Create an S3 client."""
        return boto3.client(
            service_name="s3",
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            region_name=region_name,
            config=Config(signature_version="s3v4", max_pool_connections=max_pool_connections),
        )

    def put_object(self, bucket_name: str, key: str, body: str) -> None:
        """
        Put an object in S3
        :param bucket_name: The name of the bucket
        :param key: The key of the object (the relative path to the object)
        :param body: The body of the object, in string format. This is usually a JSON string.
        """
        logging.info(f"Writing {key} to S3...")
        try:
            self.client.put_object(Bucket=bucket_name, Key=key, Body=body)
            logging.info(f"Successfully wrote {key} to S3.")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def get_object(self, bucket_name: str, key: str) -> str:
        """
        Get an object from S3.
        :param bucket_name: The name of the bucket
        :param key: The key of the object (the relative path to the object)
        :return: The body of the object, in string format. For example, a JSON string or a CSV string. UTF-8 encoded.
        """
        logging.info(f"Fetching {key} from S3...")
        try:
            response = self.client.get_object(Bucket=bucket_name, Key=key)
            logging.info(f"Successfully fetched {key} from S3.")
            return response["Body"].read().decode("utf-8")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def get_payload(self, bucket_name: str, key: str) -> str:
        """Get an object from S3."""
        logging.info(f"Fetching {key} from S3...")
        try:
            response = self.client.get_object(Bucket=bucket_name, Key=key)
            logging.info(f"Successfully fetched {key} from S3.")
            return response["Body"].read()
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def copy_object(self, source_bucket: str, destination_bucket: str, source_key: str, destination_key: str) -> None:
        """
        Copy an object from a source bucket to a destination bucket
        :param source_bucket: The name of the source bucket
        :param destination_bucket: The name of the destination bucket
        :param source_key: The key of the source object (the relative path to the object)
        :param destination_key: The key of the destination object (the relative path to the object)
        """
        logging.info(f"Copying {source_bucket}/{source_key} to {destination_bucket}/{destination_key}")
        try:
            self.client.copy_object(
                Bucket=destination_bucket,
                CopySource={"Bucket": source_bucket, "Key": source_key},
                Key=destination_key,
            )
            logging.info(f"Successfully copied {source_key} to {destination_key}.")
        except Exception as e:
            logging.error(f"Error found when copying {source_key}: {str(e)}")
            raise

    def download_file(self, bucket_name: str, key: str, filename: str, decode=True) -> str:
        """Download an object from S3 locally."""
        logging.info(f"Fetching {key} from S3 bucket {bucket_name} and storing into {filename}")
        try:
            self.client.download_file(Bucket=bucket_name, Key=key, Filename=filename)
            logging.info(f"Successfully fetched {key} from S3 and stored into {filename}")
            return filename
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def upload_file(self, bucket_name: str, key: str, filename: str) -> None:
        """Upload a file to S3."""
        logging.info(f"Uploading {filename} to S3 bucket {bucket_name} as {key}.")
        try:
            self.client.upload_file(Bucket=bucket_name, Key=key, Filename=filename)
            logging.info(f"Successfully uploaded {filename} to S3 as {key}.")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def exists_folder(self, bucket_name: str, folder_name: str) -> bool:
        """
        Check if a folder exists in S3.
        :param bucket_name: The name of the S3 bucket.
        :param folder_name: The name of the folder to check.
        return: True if the folder exists, False otherwise.
        """
        logging.info(f"Checking if {folder_name} exists in S3...")
        try:
            folder_list = self.list_objects(bucket_name=bucket_name, prefix=folder_name, only_folders=True)
            if len(folder_list) > 0:
                logging.info(f"{folder_name} exists in S3.")
                return True
            else:
                logging.info(f"{folder_name} does not exist in S3.")
                return False
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def exists_object(self, bucket_name: str, key: str) -> bool:
        """
        Check if an object exists in S3.
        :param bucket_name: The name of the S3 bucket.
        :param key: The key of the object (the relative path to the object)
        :return: True if the object exists, False otherwise.
        """
        logging.info(f"Checking if {key} exists in S3...")
        try:
            self.client.head_object(Bucket=bucket_name, Key=key)
            logging.info(f"{key} exists in S3.")
            return True
        except self.client.exceptions.ClientError as e:
            if e.response["Error"]["Code"] == "404":
                logging.info(f"{key} does not exist in S3.")
                return False
            else:
                logging.error(f"ClientError found: {str(e)}")
                raise
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def delete_object(self, bucket_name: str, key: str) -> None:
        """
        Delete an object from S3.
        :param bucket_name: The name of the S3 bucket.
        :param key: The key of the object (the relative path to the object)
        """
        logging.info(f"Deleting {key} from S3...")
        try:
            self.client.delete_object(Bucket=bucket_name, Key=key)
            logging.info(f"Successfully deleted {key} from S3.")
        except Exception as e:
            logging.error(f"Error found: {str(e)}")
            raise

    def delete_objects(self, bucket_name: str, keys: list):
        """
        Delete objects from an S3 bucket.
        :param bucket_name: The name of the S3 bucket.
        :param keys: The keys of the objects (the relative paths to the objects)
        """

        #  Split the list of keys into chunks of 1000 for batch deletion
        chunk_size = 1000
        for i in range(0, len(keys), chunk_size):
            chunk = keys[i : i + chunk_size]
            # Prepare the format required by delete_objects
            objects_to_delete = [{"Key": key} for key in chunk]
            response = self.client.delete_objects(
                Bucket=bucket_name,
                Delete={
                    "Objects": objects_to_delete,
                    "Quiet": True,  # Change to False if you want to get the list of successful deletions
                },
            )
            logging.info(f"Deleted from S3 {response.get('Deleted', [])}")
            errors = response.get("Errors", [])
            if errors:
                logging.error(f"Errors while deleting from S3: {errors}")

    def list_objects(
        self,
        bucket_name: str,
        prefix: str = "",
        only_files: bool = False,
        only_folders: bool = False,
        include_metadata: bool = False,
        full_uri: bool = False,
    ) -> list[dict | str]:
        """
        List all objects or folders in an S3 bucket.

        :param bucket_name: The name of the S3 bucket.
        :param prefix: The prefix to filter the objects or folders.
        :param only_files: If True, only files are returned.
        :param only_folders: If True, only folders are returned.
        :param include_metadata: If True, the metadata of the objects is included in the list.
            When the parameter only_folders is set to True, this parameter will be ignored as it has no effect.
        :param full_uri: If True, it returns the full URI of the objects.
        :return: By default, this function returns the relative paths to all the objects in a certain S3 prefix for a
        given bucket name.
        By setting the optional parameters, the metadata or s3_uri can also be returned.
        """
        # The paginator will paginate objects to 1000 per page so multiple calls will handle big amounts of data
        paginator = self.client.get_paginator("list_objects_v2")

        # The Delimiter is added to filter server-side on folders when only_folders is True
        operation_parameters = {
            "Bucket": bucket_name,
            "Prefix": f"{prefix}/" if only_folders and prefix != "" and not prefix.endswith("/") else prefix,
            "Delimiter": "/" if only_folders else "",
        }
        object_list = []
        pages = 0

        try:
            for page in paginator.paginate(**operation_parameters):
                if only_folders:
                    # 'CommonPrefixes' contains the folder paths (prefixes) if only_folders is True
                    # Creates a list of folder names or full URIs
                    object_list.extend(
                        f"s3://{bucket_name}/{prefix['Prefix']}" if full_uri else prefix["Prefix"].split("/")[-2]
                        for prefix in page.get("CommonPrefixes", [])
                        if "Prefix" in prefix
                    )
                else:
                    # 'Contents' contains the object keys if only_folders is False
                    # Creates a list of relative object paths or full URIs to the objects
                    # Optionally includes metadata in the object list retrieved from the S3 call
                    objects = [
                        {**content, "Key": f"s3://{bucket_name}/{content['Key']}"} if full_uri else content
                        for content in page.get("Contents", [])
                        if not (only_files and content["Key"].endswith("/"))
                    ]
                    object_list.extend(objects if include_metadata else [obj["Key"] for obj in objects])

                pages = pages + 1
            logging.info(f"Found {len(object_list)} objects from {bucket_name} with prefix {prefix} in {pages} pages.")

        except (NoCredentialsError, PartialCredentialsError) as e:
            logging.error(f"Error: {str(e)}. Please configure AWS CLI or provide credentials.")
        except ClientError as e:
            logging.error(f"Error: {e.response['Error']['Message']}")
        except Exception as e:
            logging.error(f"An unexpected error occurred: {str(e)}")

        return object_list

    def move_objects(self, source_bucket: str, source_key: str, destination_bucket: str, destination_key: str):
        """
        Moves objects from one s3 location to another one. It simply calls the copy objects function, followed
        by the delete function (which is basically what move does).

        :param source_bucket: The name of the source bucket
        :param destination_bucket: The name of the destination bucket
        :param source_key: The key of the source object (the relative path to the object)
        :param destination_key: The key of the destination object (the relative path to the object)
        """
        try:
            self.copy_object(
                source_bucket=source_bucket,
                destination_bucket=destination_bucket,
                source_key=source_key,
                destination_key=destination_key,
            )
            self.delete_object(bucket_name=source_bucket, key=source_key)
            logging.info(f"{source_key} moved successfully")
        except NameError as e:
            logging.error(f"An error has occured trying to move file {source_key}: {e}")


"""
TODO: Add for BLobs?
class BlobClient:

    def __init__(
            self, account_name: str, account_key: str, container_name: str, blob_name: str
    ):
        self.account_name = account_name
        self.account_key = account_key
        self.container_name = container_name
        self.blob_name = blob_name

    def _get_blob_service(self) -> BlobServiceClient:

        account_url = "https://{}.blob.core.windows.net/".format(self.account_name)
        return BlobServiceClient(account_url, credential=self.account_key)

    def _get_blob_client(self, fmt_str: Optional[str] = None) -> BlobClient:

        account_url = "https://{}.blob.core.windows.net/".format(self.account_name)
        if fmt_str:
            blob_name = self.blob_name.format(fmt_str)
        else:
            blob_name = self.blob_name
        return BlobClient(
            account_url, self.container_name, blob_name, credential=self.account_key
        )

    def get_blob_content(self, fmt_str: Optional[str] = None):

        return self._get_blob_client(fmt_str=fmt_str).download_blob().readall()

    def list_container_content(self, directory_path: Optional[str] = None)-> list:


        container_client = self._get_blob_service().get_container_client(
            container=self.container_name
        )
        blob_list = container_client.list_blobs(directory_path)
        return blob_list

    def copy_blob_content(self, src_file_path: str, dest_file_path: str):


        source_url = f"https://{self.account_name}.blob.core.windows.net/{self.container_name}/{src_file_path}"

        dest_blob = self._get_blob_client(fmt_str=dest_file_path)
        dest_blob.start_copy_from_url(source_url=source_url)

    def delete_blob_by_path(self, delete_file_path: str):

        blob_to_delete = self._get_blob_client(fmt_str=delete_file_path)
        blob_to_delete.delete_blob(delete_snapshots="include")
"""


@dataclasses.dataclass
class StorageStages:
    """
    A class representing the different stages in the datalake
    """

    BLOB: str = "blob"
    LANDING_ZONE: str = "landing-zone"
    RAW: str = "raw"
    ENRICHED: str = "enriched"
    CURATED: str = "curated"
    REGISTRATIONS: str = "registrations"
    # CLEAN: str = "clean"
    # PII: str = "pii"


@dataclasses.dataclass
class ApiKeys:
    """
    A class representing the available api keys in the key vault
    """

    openweathermap: str
    here: str
    # airship: Credentials
    asset_maintenance_prd: str


"""
TODO: might be interesting for Blob in future


@dataclasses.dataclass
class BlobClients:
    \"\"\"
    A class to represent all blob clients available to the data lake
    \"\"\"

    occupancy_extrapolation_sandbox_model: AzureBlobClient
    occupancy_extrapolation_reporting_model: AzureBlobClient
    passenger_count_boarding_extrapolation_sandbox_model: AzureBlobClient
    passenger_count_boarding_extrapolation_reporting_model: AzureBlobClient
    crowdedness_prediction_config: AzureBlobClient
    occupancy_routes_for_timeseries_config: AzureBlobClient
    hbv_incident_config: AzureBlobClient
    core_vehicle_manual_config: AzureBlobClient
    ebs_metadata_blobclient: AzureBlobClient
    ebs_raw_blobclient: AzureBlobClient
    network_changes_scheduler_config: AzureBlobClient
    extrapolation_buildings_of_interest: AzureBlobClient
    excel_reports_blobclient: AzureBlobClient

"""

# TODO: Future possibility to connect with Naturalis Key Vault
# def _connect_key_vault(
#         self, service_principal: dict, tenant_id: str, vault_url: str
# ) -> SecretClient:
#     """
#     Initialize the connection to the azure key vault.
#
#     :param service_principal: Dictionary containing service principal credentials used to access the
#     key vault (sp_id, sp_secret)
#     :param tenant_id: The tenant id for the service principal
#     :param vault_url: The url of the keyvault for which the client will work
#     :return: a KeyVault SecretClient instance
#     """
#
#     credentials = ClientSecretCredential(
#         tenant_id=tenant_id,
#         client_id=service_principal.get("id"),
#         client_secret=service_principal.get("secret"),
#     )
#
#     client = SecretClient(vault_url=vault_url, credential=credentials)
#     return client

# def _safe_get_secret(self, secret_name: str, secret_version: str) -> str:
#     """
#     Get a key vault secret by name and vault url. If the secret does not exist an empty string is returned.
#
#     :param secret_name: The name of the secret
#     :param secret_version: The version of the secret (empty string for latest)
#     :return: The string value of the secret
#     """
#     try:
#         return self._vault_client.get_secret(secret_name, secret_version).value
#     except Exception as e:
#         logging.error("Exception when fetching key: {}".format(repr(e)))
#         return ""

# @dataclasses.dataclass
# class Credentials:
#     """
#     A class representing the user/secret pair in the key vault
#     """
#
#     client_id: str
#     client_secret: str


# TODO: interesting if we ever wanna use eventhubs or service busses
# class AzureEventHub:
#     """
#     A class representing an Azure EventHub
#
#     :param write_conn_str: Connection string of the eventhub with write authorizations
#     Supported format Endpoint=sb://{NAMESPACE}.servicebus.windows.net/{EVENT_HUB_NAME};EntityPath={EVENT_HUB_NAME};
#     SharedAccessKeyName={ACCESS_KEY_NAME};SharedAccessKey={ACCESS_KEY}
#     """
#     def __init__(self, eventhub_name: str, write_conn_str: str) -> None:
#         self.eventhub_name = eventhub_name
#         self.write_conn_str = write_conn_str
#
#     def get_eventhub_producer(self):
#         """
#         Creates a producer for this event hub
#
#         :return: a producer for the event hub
#         """
#         return EventHubProducerClient.from_connection_string(self.write_conn_str)
#
#     def send_to_eventhub(self, df: DataFrame):
#         """
#         Sends the content of a DataFrame *df* to the eventhub. Sends one event per row in the dataframe.
#         Each event is sent as a stringified JSON object
#
#         :param df: DataFrame to send to the eventhub
#         """
#         producer = self.get_eventhub_producer()
#
#         batch_datasets = []
#         batch_data = producer.create_batch()
#
#         for event in df.toJSON().toLocalIterator():
#             event = EventData(event.encode())
#             try:
#                 batch_data.add(event)
#             except ValueError:
#                 batch_datasets.append(batch_data)
#                 batch_data = producer.create_batch()
#                 batch_data.add(event)
#         batch_datasets.append(batch_data)
#
#         for batch in batch_datasets:
#             producer.send_batch(batch)
#
#         producer.close()
#
#
# class AzureServiceBus:
#     """
#     A class representing an Azure Service Bus
#
#     :param servicebus_namespace: The namespace of the Azure Service Bus
#     :param topic_name: The name of the Service Bus topic
#     :param write_conn_str: Connection string of the Service Bus with write authorizations
#     """
#     def __init__(self, servicebus_namespace: str, topic_name: str, write_conn_str: str) -> None:
#         self.servicebus_namespace = servicebus_namespace
#         self.topic_name = topic_name
#         self.write_conn_str = write_conn_str
#
#     def get_servicebus_sender(self):
#         """
#         Creates a sender for the Service Bus
#
#         :return: ServiceBusClient
#         """
#         client = ServiceBusClient.from_connection_string(self.write_conn_str)
#         return client.get_topic_sender(topic_name=self.topic_name)
#
#     def send_df_to_servicebus(self, df: DataFrame):
#         """
#         Sends DataFrame to the Service Bus topic.
#
#         :param df: The DataFrame containing data to be sent.
#         """
#         sender = self.get_servicebus_sender()
#         try:
#             for row in df.toJSON().toLocalIterator():
#                 message = ServiceBusMessage(body=row)
#
#                 # Check if the DataFrame contains a SERVICEBUS_MESSAGE_ID column (used for duplicate detection)
#                 if 'SERVICEBUS_MESSAGE_ID' in row:
#                     message.message_id = json.loads(row)['SERVICEBUS_MESSAGE_ID']
#
#                 sender.send_messages(message)
#         finally:
#             sender.close()


# TODO: interesting if we want to use telemetry or anonymization
# def _initialize_anonymization(self) -> Anonymizer:
#     """
#     Set up the anonymization object, with provided salt and salt_nri
#     salt will be used in the 'normal' anonymization flow where hash lookup tables are generated
#     salt_nri (non re-identifiable) will be used when we want to skip the creation of a hash lookup table
#
#     :return: An Anonymizer object
#     """
#     salt = self._safe_get_secret("dataplatform-salt-{}".format(self.env), "")
#     salt_nri = self._safe_get_secret(
#         "dataplatform-salt-to-store-in-a-fancy-new-place-{}".format(self.env), ""
#     )
#     return Anonymizer(salt=salt, salt_nri=salt_nri)

# def _initialize_telemetry(self):
#     instrumentation_key = self._safe_get_secret(
#         f"dataplatform-app-insights-dataplatform-key-{self.env}", ""
#     )
#     return DataplatformTelemetry(instrumentation_key)


# TODO: dataset with history
# class DatasetWithHistory(Dataset):
#     """
#     Overrides the base Dataset class to add functionality to work with data lake path which change over time.
#     It allows for the configuration of expiry dates for each version of a path, and the dynamic resolution
#     of the proper path based on a date.
#
#     Example
#
#     .. code-block:: python
#
#         dataset = DatasetWithExpiry(
#             paths = {'raw': 'path/to_{{templated}}/{0}/folder'.format('prd')
#             path_history = {'raw': {datetime.date(2020,1,1): {'templated': 'old'},
#                                     datetime.date(9999,1,1): {'templated': 'current'}}
#                             }
#         )
#
#     :param paths: A mapping of stages and their related path.
#         If a part of a path needs to be templated based on the path history,
#         the template kwarg should be surrounded by double accolades.
#     :param path_history: A dictionary the history of the path for the relevant stages.
#         The history for a stage is a dict with an expiry date - template kwargs mapping.
#         The currently valid path should have an expiry date far in the future.
#     """
#
#     def __init__(
#             self,
#             paths: Dict[str, str],
#             path_history: Dict[str, Dict[datetime.date, Dict[str, str]]] = None,
#     ):
#         self.path_history = path_history or {}
#         super().__init__(paths)
#
#     def get_path(
#             self,
#             stage: str,
#             path_suffix: str = None,
#             dataset_name: str = "",
#             path_date: datetime.date = None,
#     ) -> str:
#         """
#         Get the full path to a dataset.
#
#         Example
#
#         .. code-block:: python
#
#             dataset.get_path('raw', path_suffix='suffix', dataset_name='dataset', path_date=datetime.date(2020,2,1))
#             > 'path/to_current/prd/folder/dataset/suffix'
#             dataset.get_path('raw', path_suffix='*/*', path_date=datetime.date(2019,2,1))
#             > 'path/to_old/prd/folder/*/*'
#
#         :param stage: The stage of the data [raw, pii, clean, derived, export, tmp]
#         :param path_suffix: Optional suffix to the path (eg \*/\*/\* to get all files in YYYY/MM/DD folder structure)
#         :param dataset_name: Optional name of the dataset, to be used when multiple tables are grouped under
#           a single Dataset instance
#         :param path_date: Specifies for which date the path should be valid
#         :return: The path to the dataset
#         """
#         self._check_valid_stage(stage)
#         path = self._handle_path_history(stage, path_date)
#
#         suffix = f"{path_suffix}/" if path_suffix else ""
#         full_path = f"{path}/{dataset_name}" if dataset_name else path
#
#         return f"wasbs://{full_path}/{suffix}"
#
#     def get_relative_path(self, stage: str, path_date: datetime.date = None) -> str:
#         """
#         returns the path to the top level of the dataset within the container
#         Example
#
#         .. code-block:: python
#
#             dataset.get_relative_path('raw', path_date=datetime.date(2020,2,1))
#             > 'path/to_current/prd/folder'
#
#         :param stage: The stage of the data [raw, pii, clean, derived]
#         """
#         self._check_valid_stage(stage)
#         path = self._handle_path_history(stage, path_date)
#         try:
#             return path.split(".blob.core.windows.net/")[1]
#         except IndexError:
#             return ""
#
#     def _handle_path_history(self, stage: str, path_date: datetime.date) -> str:
#         """
#         Resolves the proper path for the history of paths based on a given date.
#
#         :param stage: The selected stage
#         :param path_date: The date for which to select the path from the history
#         :return: The proper path for the specified date
#         :raises ValueError: The specified date is after than the most recent expiry date
#         """
#         s = self.path_history.get(stage)
#         path = self.paths[stage]
#         if not s or not path_date:
#             return path
#
#         try:
#             current_valid_date = [d for d in sorted(s.keys()) if d >= path_date][0]
#         except IndexError:
#             raise ValueError(
#                 "received path_date (%s) past lat expiry date (%s)"
#                 % (path_date, sorted(s.keys())[-1])
#             )
#         return path.format(**s.get(current_valid_date))
