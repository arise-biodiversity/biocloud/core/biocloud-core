from pyspark.sql import DataFrame
from pyspark.sql.functions import current_timestamp, lit


def add_timestamp_columns(df: DataFrame) -> DataFrame:
    """Add timestamp columns to the given dataframe.
    inserted_ts_utc  timestamp
    updated_ts_utc   timestamp
    :param df (DataFrame)
    :return DataFrame
    """
    current_utc_timestamp = current_timestamp()
    df = df.withColumns({"inserted_ts_utc": lit(current_utc_timestamp), "updated_ts_utc": lit(current_utc_timestamp)})
    return df


def column_values_to_list(df: DataFrame, column_name: str) -> list:
    """
    Returns all values from a dataframe column as a list
    :param df (DataFrame)
    :param column_name (str)
    :return list
    """
    return [row[column_name] for row in df.select(column_name).collect()]
