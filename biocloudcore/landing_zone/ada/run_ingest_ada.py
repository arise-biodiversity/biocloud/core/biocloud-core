import logging

from biocloudcore.data_lake import DataLake
from biocloudcore.landing_zone.ada.ingest_ada import Ada
from biocloudcore.spark_session import get_environment, spark_session


def run():
    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    data_lake = DataLake(env=get_environment(spark))

    Ada(
        spark=spark,
        data_lake=data_lake,
    ).run(
        bucket_name=data_lake.ada.landing_zone.get_bucket_name(),
        resources=["codeTables", "registrations"],
        dump_folder="adadump",
    )

    spark.stop()


if __name__ == "__main__":
    run()
