import logging
from datetime import datetime, timedelta

from pyspark.sql import SparkSession

from biocloudcore.data_lake import DataLake, S3Client

logger = logging.getLogger(__name__)


class Ada:
    """
    Ada data is retrieved from JSON files that are put on our landingzone in the adadump folder.
    An Azure function from the apps team arranges this dump of data. We fetch the data from the latest timestamped
    folder and store it in S3.
    """

    AVAILABLE_S3_RESOURCES = [
        "codeTables",
        "registrations",
        # "species", This is currently not sent to us from the apps team's Function apps
        # "taxonomic" Can be sent to us again by request
    ]

    def __init__(self, spark: SparkSession, data_lake: DataLake):
        self.spark = spark
        self.data_lake = data_lake
        self.s3_client = self.data_lake.csc.get_client()

    def run(self, bucket_name: str, resources: list[str], dump_folder: str):
        deployment_data = {}

        latest_timestamp_directory = self.get_latest_ada_dump_directory(self.s3_client, bucket_name, dump_folder)

        for resource in resources:
            if resource not in self.AVAILABLE_S3_RESOURCES:
                raise ValueError(f"Not a valid Ada resource: {resource}.")

            json_file = f"{dump_folder}/{latest_timestamp_directory}/{resource}.json"

            deployment_data[resource] = self.s3_client.get_object(bucket_name=bucket_name, key=json_file)

            self.s3_client.put_object(
                bucket_name=bucket_name,
                key=f"ada/{resource}.json",
                body=deployment_data[resource],
            )

        logger.info("Successfully fetched and stored Ada data in S3.")

    def get_latest_ada_dump_directory(self, s3_client: S3Client, landing_zone_bucket: str, dump_folder: str) -> str:
        """
        Get the latest timestamped folder from the adadump.

        :param S3Client s3_client: S3 client to interact with S3
        :param str landing_zone_bucket: name of the bucket where the Ada dump is stored
        :param str dump_folder: folder where the Ada dump is stored
        :return str: the absolute path to the latest timestamped folder
        """
        adadump_directories = s3_client.list_objects(
            bucket_name=landing_zone_bucket, prefix=dump_folder, only_folders=True
        )

        try:
            latest_timestamp_directory = max(adadump_directories)
            no_recent_ada_dump = (datetime.today() - timedelta(days=2)) > datetime.strptime(
                latest_timestamp_directory, "%Y-%m-%d-%H:%M:%S"
            )
        except Exception as e:
            raise ValueError(f"Invalid datestamp directory in adadump found: {adadump_directories}.") from e

        if no_recent_ada_dump:
            raise Exception(
                "No recent Ada dump found in the last 24 hours. Latest timestamp directory: "
                f"{latest_timestamp_directory}"
            )

        return latest_timestamp_directory
