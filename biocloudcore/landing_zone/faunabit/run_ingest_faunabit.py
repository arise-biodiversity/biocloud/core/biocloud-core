import logging

from biocloudcore.data_lake import DataLake
from biocloudcore.landing_zone.faunabit.ingest_faunabit import Faunabit
from biocloudcore.spark_session import get_environment, spark_session


def run():
    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    data_lake = DataLake(env=get_environment(spark))

    Faunabit(
        spark=spark,
        data_lake=data_lake,
    ).run(
        bucket_name=data_lake.diopsis.landing_zone.get_bucket_name(),
        api_url="https://api.faunabit.eu/api",
        resources=["locations", "researches", "devices"],
        api_timeout=900,
    )

    spark.stop()


if __name__ == "__main__":
    run()
