import json
import logging

from pyspark.sql import SparkSession

from biocloudcore.data_lake import DataLake
from biocloudcore.landing_zone.faunabit.api.faunabit_api import FaunabitAPI
from biocloudcore.spark_session import get_databricks_secret

logger = logging.getLogger(__name__)


class Faunabit:
    def __init__(self, spark: SparkSession, data_lake: DataLake):
        self.spark = spark
        self.data_lake = data_lake
        self.s3_client = data_lake.biocloud.get_client()

    def run(self, bucket_name: str, api_url: str, resources: list[str], api_timeout: int):
        faunabit_api = FaunabitAPI(
            url=api_url,
            token=get_databricks_secret(key="FAUNABIT_TOKEN", domain="biocloud", env=self.data_lake.env),
            timeout=api_timeout,
        )

        faunabit_data = faunabit_api.get_data(resources=resources)

        faunabit_data["deployments"] = self._transform_faunabit_deployments(faunabit_data["researches"])
        faunabit_data["projects"] = self._transform_faunabit_projects(faunabit_data["researches"])

        for resource in faunabit_data:
            self.s3_client.put_object(
                bucket_name=bucket_name,
                key=f"diopsis/faunabit/{resource}.json",
                body=json.dumps(faunabit_data[resource]),
            )

    def _transform_faunabit_deployments(self, researches_data: list[dict]) -> list[dict]:
        # TODO: Move this method to the raw layer and delete here as soon as old Biocloud is deprecated
        logger.info("Transform researches data to deployments data")
        logger.warning("This method should move to the raw layer! It is not a good practice to transform data here.")

        deployments = []
        for obj in researches_data:
            for deployment in obj["location_researches"]:
                deployments.append(deployment)
        return deployments

    def _transform_faunabit_projects(self, researches_data: list[dict]) -> list[dict]:
        # TODO: Move this method to the raw layer and delete here as soon as old Biocloud is deprecated
        logger.info("Transform researches data to projects data")
        logger.warning("This method should move to the raw layer! It is not a good practice to transform data here.")

        projects = []
        for obj in researches_data:
            deployments = []
            for deployment in obj["location_researches"]:
                deployments.append(deployment["id"])
            projects.append(
                {
                    "id": obj["id"],
                    "name": obj["name"],
                    "description": obj["description"],
                    "deployments": deployments,
                    "organization_id": obj["organization_id"],
                    "start_date": obj["start_date"],
                    "end_date": obj["end_date"],
                    "created_at": obj["created_at"],
                    "deleted_at": obj["deleted_at"],
                }
            )
        return projects
