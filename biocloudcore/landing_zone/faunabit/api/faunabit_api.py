import json
import logging

import requests

logger = logging.getLogger(__name__)


class FaunabitAPI:
    """
    Class for querying the Faunabit API to get metdata on the DIOPSIS cameras. The API is maintained by Faunabit,
    which has a database of all the cameras and their locations, which can be managed by users via the Faunabit portal.
    The API is used to get the metadata of these cameras, their locations, and the researches that are being conducted.
    """

    AVAILABLE_API_RESOURCES = ["locations", "researches", "devices"]

    def __init__(self, url: str, token: str, timeout: int):
        self.url = url
        self.token = token
        self.timeout = timeout

    def get_data(self, resources: list[str]) -> dict[str, json]:
        """
        Fetches data from Faunabit API and returns a dictionary containing the selected resources.

        :param list[str] resources: List of resources to fetch from the Faunabit API.
        :return dict: A dictionary containing consensuses, sequencing runs, and amplicons fetched from Nanopore API.
        """
        data = {}

        with requests.Session() as session:
            # Create the session with bearer token so it can be reused for all requests
            session.headers.update({"Authorization": f"Bearer {self.token}"})

            for resource in resources:
                if resource not in self.AVAILABLE_API_RESOURCES:
                    raise ValueError(f"Not a valid Faunabit resource: {resource}.")
                else:
                    logging.info(f"Fetching {resource} data from Faunabit API...")
                    data[resource] = self._call_api(session=session, endpoint=resource)

        logging.info("Successfully fetched all deployment metadata from Faunabit API.")

        return data

    def _call_api(self, session: requests.Session(), endpoint: str) -> list[dict]:
        """
        Get Resources from the Faunabit API for the given endpoint. Use the session object to make the request, so that
        the bearer token is reused for all requests.

        :param requests.Session session: Session object to make the request
        :param str endpoint: name of the endpoint to get the data from
        :return list[dict]: JSON response containing the data of the resource
        """

        try:
            response = session.get(f"{self.url}/{endpoint}?include_deleted=1", timeout=self.timeout)
            response.raise_for_status()
            return response.json()

        except requests.exceptions.HTTPError as e:
            logger.error(f"HTTP error occurred: status={e.response.status_code}, reason={e.response.reason}, error={e}")
            raise
        except requests.exceptions.RequestException as e:
            logger.error(f"Request error: {e}")
            raise
        except ValueError as e:
            logger.error(f"JSON decode error (probably empty response): {e}")
            raise
