import logging
import os
import time
from datetime import datetime
from threading import Lock, local

import requests
from deprecated import deprecated
from requests.models import Response
from requests.sessions import Session

from biocloudcore.data_lake import S3Client

auth_lock = Lock()

logger = logging.getLogger(__name__)

thread_local = local()


@deprecated(
    version="0.1.2",
    reason="The NaturalisAIServerAPI is deprecated, as Faunabit now pushes data directly to us. We still need this "
    "until the old Biocloud has been deleted and al old data has been ingested from the AI server.",
)
class NaturalisAIServerAPI:
    """Class for receiving Diopsis payload data from ai.naturalis.nl server."""

    def __init__(self, url: str, username: str, password: str):
        self.url = url
        self.username = username
        self.password = password

        self._authenticate()

    def _get_request_session(self) -> Session:
        if not hasattr(thread_local, "session"):
            thread_local.session = requests.Session()
        return thread_local.session

    def get_image_ids(self, camera_number: str) -> list[str]:
        logger.info(f"Fetching image ids for sensor {camera_number} from ai.naturalis.nl server...")
        image_ids = self._get_image_ids_from_camera(camera_number)
        logger.info(
            f"Successfully fetched {len(image_ids)} image ids for sensor {camera_number} from ai.naturalis.nl server."
        )
        return image_ids

    def process_image(self, s3_client: S3Client, bucket_name: str, camera_number: str, image_id: str) -> None:
        """Fetch image payload and store in S3."""
        payload_key = f"faunabit/sensors/{camera_number}/payload/{image_id}"
        if not s3_client.exists_object(bucket_name=bucket_name, key=payload_key):
            payload = self._get_image_payload(camera_number, image_id)
            if payload is not None:
                s3_client.put_object(bucket_name=bucket_name, key=payload_key, body=payload)
            else:
                logger.error(
                    f"Failed to store image {image_id} for sensor {camera_number} "
                    f"in S3 because payload or metadata is empty."
                )

    def _get_image_payload(self, camera_number: str, image_id: str) -> bytes | None:
        image_data = self._get_image_data(camera_number, image_id)
        logger.info(
            f"Successfully fetched payload image {image_id} for sensor {camera_number} " f"from ai.naturalis.nl server."
        )

        return image_data

    def _authenticate(self, max_retries: int = 5) -> None:
        """Authenticate with the server and store the token."""
        session = self._get_request_session()
        with auth_lock:
            for attempt in range(max_retries):
                try:
                    auth_response: Response = session.post(
                        f"{self.url}/authentication",
                        data={"username": self.username, "password": self.password},
                        timeout=300,
                    )

                    # If HTTP 409 error, retry
                    if auth_response.status_code == 409:
                        raise requests.exceptions.HTTPError()

                    # Raise exception for other HTTP errors
                    auth_response.raise_for_status()

                    token = auth_response.json()["secure_token"]

                    if not token:
                        logger.error("Token is null or empty")
                        raise ValueError("Token is null or empty")

                except (requests.exceptions.HTTPError, ValueError) as e:
                    logger.error(f"{auth_response.status_code} {auth_response.reason}")
                    if attempt < max_retries - 1:  # no need to sleep on the last attempt
                        sleep_time = 2**attempt  # exponential backoff
                        time.sleep(sleep_time)
                        continue
                    else:
                        raise e

                # Store the token if no exception was raised
                self._token = token
                return

        # If it reached here, all attempts have failed.
        logger.error(f"All {max_retries} attempts failed to authenticate.")

    def _get_image_ids_from_camera(self, camera_number: str, max_retries: int = 5) -> list[str]:
        """Get Image Ids from images stored in the server for the given camera number."""
        session = self._get_request_session()
        for attempt in range(max_retries):
            try:
                image_ids: Response = session.get(
                    url=(
                        f"{self.url}/projects/insectcam_insectpi{camera_number}"
                        f"/groups/__all__/images?secure_token={self._token}"
                    ),
                    timeout=300,
                )

                # If HTTP 409 error, authenticate and retry
                if image_ids.status_code == 409:
                    self._authenticate()
                    raise requests.exceptions.HTTPError()

                # Raise exception for other HTTP errors
                image_ids.raise_for_status()

            except requests.exceptions.HTTPError as e:
                logger.error(f"{image_ids.status_code} {image_ids.reason}")
                if attempt < max_retries - 1:  # no need to sleep on the last attempt
                    sleep_time = 2**attempt  # exponential backoff
                    time.sleep(sleep_time)
                    continue
                else:
                    raise e

            # Return the image IDs if no exception was raised
            return image_ids.json()["items"]

        # If it reached here, all attempts have failed.
        logger.error(f"All {max_retries} attempts failed to get image IDs.")
        return []

    def _get_image_data(self, camera_number: str, image_id: str, max_retries: int = 5) -> bytes | None:
        """Get image payload from server for the given id from the given camera."""
        session = self._get_request_session()
        for attempt in range(max_retries):
            try:
                media_request: Response = session.get(
                    url=(
                        f"{self.url}/projects/insectcam_insectpi{camera_number}"
                        f"/groups/__all__/images/{image_id}/media?secure_token={self._token}"
                    ),
                    timeout=300,
                )

                # If HTTP 409 error, authenticate and retry
                if media_request.status_code == 409:
                    self._authenticate()
                    raise requests.exceptions.HTTPError()

                # Raise exception for other HTTP errors
                media_request.raise_for_status()

                # Check the content length
                if media_request.headers["Content-length"] == "0":
                    logger.warning(
                        f"SKIPPING: No image content for <{image_id}> - status code = {media_request.status_code}"
                    )
                    return None

                if not media_request.ok:
                    logger.warning(f"status code is not OK => {media_request.status_code}")
                    return None

            except requests.exceptions.HTTPError as e:
                logger.error(f"{media_request.status_code} {media_request.reason}")
                if attempt < max_retries - 1:  # no need to sleep on the last attempt
                    sleep_time = 2**attempt  # exponential backoff
                    time.sleep(sleep_time)
                    continue
                else:
                    raise e

            # Return the image data if no exception was raised
            return media_request.content

        # If it reached here, all attempts have failed.
        logger.error(f"All {max_retries} attempts failed to get image data.")
        return None


def get_image_metadata(camera_number: str, image_id: str, device_id: str) -> dict:
    json_obj = {}
    try:
        date = datetime.strptime(image_id, "%Y%m%d%H%M%S")
        resolution = "original"
        json_obj = {
            "device_name": f"DIOPSIS-{camera_number}",
            "capture_datetime": f"{date.isoformat()}",
            "mime_type": "image/jpeg",
            "irods_uri": os.path.join("/arise/home/diopsis/sensors", camera_number, f"{image_id}.jpg"),
            "s3_uri": os.path.join(
                f"diopsis/images/insectcamera/DIOPSIS-{camera_number}",
                resolution,
                str(date.year),
                f"{date.month:02d}",
                f"{image_id}.jpg",
            ),
            "device_id": str(device_id),
        }

    except ValueError:
        logger.error(f"Invalid image id: {image_id}")

    return json_obj
