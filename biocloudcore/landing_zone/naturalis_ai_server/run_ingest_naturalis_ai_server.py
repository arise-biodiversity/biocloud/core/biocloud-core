import logging

from biocloudcore.data_lake import DataLake
from biocloudcore.landing_zone.naturalis_ai_server.ingest_naturalis_ai_server import NaturalisAIServer
from biocloudcore.spark_session import get_environment, spark_session


def run():
    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    data_lake = DataLake(env=get_environment(spark))

    # TODO: write a way to set these parameters from Databricks in the run file

    NaturalisAIServer(
        spark=spark,
        data_lake=data_lake,
    ).run(
        bucket_name=data_lake.diopsis.landing_zone.get_bucket_name(),
        diopsis_payload_url="https://ai.naturalis.nl/v1",
        camera_numbers=["417"],
        max_payload_files_per_device=1000,
        parallel_downloads=10,
        startdate_filter="2024-06-01",
        enddate_filter="2025-01-22",
    )

    spark.stop()


if __name__ == "__main__":
    run()
