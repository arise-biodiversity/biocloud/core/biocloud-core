import concurrent.futures
import json
import logging
from datetime import datetime

from deprecated import deprecated
from pyspark.sql import SparkSession

from biocloudcore.data_lake import DataLake
from biocloudcore.landing_zone.faunabit.api.faunabit_api import FaunabitAPI
from biocloudcore.landing_zone.naturalis_ai_server.api.naturalis_ai_server_api import (
    NaturalisAIServerAPI,
    get_image_metadata,
)
from biocloudcore.spark_session import get_databricks_secret

logger = logging.getLogger(__name__)


@deprecated(
    version="0.1.2",
    reason="The whole NaturalisAIServer is deprecated, as Faunabit now pushes data directly to us. We still need this "
    "until the old Biocloud has been deleted and al old data has been ingested from the AI server.",
)
class NaturalisAIServer:
    def __init__(self, spark: SparkSession, data_lake: DataLake):
        self.spark = spark
        self.data_lake = data_lake
        self.s3_client = data_lake.biocloud.get_client()

    def run(
        self,
        bucket_name: str,
        diopsis_payload_url: str,
        camera_numbers: list[str],
        max_payload_files_per_device: int,
        parallel_downloads: int,
        startdate_filter: str,
        enddate_filter: str,
    ):
        startdate_filter = self._validate_time_filter(startdate_filter)
        enddate_filter = self._validate_time_filter(enddate_filter)

        if len(camera_numbers) > 0:
            logger.info(f"Camera numbers to be processed: {camera_numbers}")
            naturalis_server = NaturalisAIServerAPI(
                url=diopsis_payload_url,
                username=get_databricks_secret(key="DIOPSIS_PAYLOAD_USER", domain="biocloud", env=self.data_lake.env),
                password=get_databricks_secret(key="DIOPSIS_PAYLOAD_PASS", domain="biocloud", env=self.data_lake.env),
            )
            if max_payload_files_per_device == 0:
                logger.info("No payload will be processed. Change MAX_PAYLOAD_FILES_PER_DEVICE to process payload.")
            else:
                # Get the availble devices which were ingested from the Faunabit API
                available_device_ids = self.get_device_ids(bucket_name, path="diopsis/faunabit/devices.json")

                # If the camera_numbers list contains "all", get all available device ids
                if camera_numbers == ["all"]:
                    camera_numbers = [device["number"] for device in available_device_ids if device["number"].isdigit()]

                # Process image metadata and payload for each camera number
                for camera_number in camera_numbers:
                    device_id = next((id["id"] for id in available_device_ids if camera_number == id["number"]), None)
                    if not device_id:
                        logger.error(f"Device id not found for camera number {camera_number}.")
                        continue

                    # Get image ids that belong to camera number
                    image_ids = naturalis_server.get_image_ids(camera_number)

                    # Apply year filter
                    if startdate_filter > datetime.min or enddate_filter > datetime.min:
                        image_ids = self._filter_date(image_ids, startdate_filter, enddate_filter)

                    if max_payload_files_per_device > 0:
                        logger.info(
                            f"Only {max_payload_files_per_device} files will be processed. "
                            "Change MAX_PAYLOAD_FILES_PER_DEVICE value to -1 to process all payload."
                        )
                        image_ids = image_ids[:max_payload_files_per_device]

                    if len(image_ids) > 0:
                        self._process_image_metadata(bucket_name, camera_number, device_id, image_ids)

                        # Process image payload
                        parallel_downloads = int(parallel_downloads)
                        with concurrent.futures.ThreadPoolExecutor(max_workers=parallel_downloads) as executor:
                            for image_id in image_ids:
                                executor.submit(
                                    naturalis_server.process_image,
                                    s3_client=self.s3_client,
                                    bucket_name=bucket_name,
                                    camera_number=camera_number,
                                    image_id=image_id,
                                )

                        logger.info(
                            f"Successfully wrote {len(image_ids)} image payload"
                            "and metadata from sensor {camera_number} to S3."
                        )
                logger.info(f"Successfully fetched and stored image data in S3: {camera_numbers}")

    def _validate_time_filter(self, date_string) -> datetime:
        """Transform filter value from string type to datetime type"""
        filter_date = datetime.min
        try:
            if date_string and date_string != "None":
                filter_date = datetime.strptime(date_string, "%Y-%m-%d")
        except ValueError:
            logger.error(f"Invalid date: {date_string} does not match format 'yyyy-mm-dd' ")
            raise
        return filter_date

    def _process_faunabit_metadata(self, faunabit_api: FaunabitAPI, bucket_name: str) -> None:
        """Get all useful Faunabit metadata from the FaunabitAPI and write it to the landing zone."""
        deployment_data = faunabit_api.get_faunabit_metadata()

        for key in deployment_data:
            json_file = f"diopsis/faunabit/{key}.json"

            logger.debug(f"Writing {json_file} to S3...")
            self.s3_client.put_object(
                bucket_name=bucket_name,
                key=json_file,
                body=json.dumps(deployment_data[key]),
            )

        logger.info("Successfully fetched and stored Faunabit metadata in S3.")

    def _process_image_metadata(
        self, bucket_name: str, camera_number: str, device_id: str, image_ids: list[str]
    ) -> None:
        """Create jsonl image metadata and store in landing zone bucket"""
        merged_json = ""
        for image_id in image_ids:
            merged_json += f"{json.dumps(get_image_metadata(camera_number, image_id, device_id))}\n"

        timestamp = int(datetime.now().timestamp())
        metadata_key = f"diopsis/sensors/{camera_number}/processed/metadata/merged-{timestamp}.jsonl"
        if not self.s3_client.exists_object(bucket_name=bucket_name, key=metadata_key):
            self.s3_client.put_object(bucket_name=bucket_name, key=metadata_key, body=merged_json)

    def _filter_date(self, image_ids: list[str], startdate_filter: datetime, enddate_filter: datetime) -> list[str]:
        """Select only images of a certain year"""
        if enddate_filter == datetime.min:
            enddate_filter = datetime.now()
        invalid_ids = []
        selected_ids = []
        for image_id in image_ids:
            try:
                image_date = datetime.strptime(image_id[:8], "%Y%m%d")
                if startdate_filter <= image_date <= enddate_filter:
                    selected_ids.append(image_id)
            except ValueError as e:
                logger.error(f"Payload file {image_id} is not a valid datetime: {e}")
                invalid_ids.append(image_id)
        if len(invalid_ids) > 0:
            logger.info(f"WARNING: {len(invalid_ids)} invalid file names found: {invalid_ids}")
        logger.info(
            f"{len(selected_ids)} images found between {startdate_filter.strftime('%Y-%m-%d')} "
            f"and {enddate_filter.strftime('%Y-%m-%d')}"
        )
        return selected_ids

    def get_device_ids(self, bucket_name: str, path: str) -> list[dict]:
        """Get device ids for which to fetch images"""
        try:
            json_obj = json.loads(self.s3_client.get_object(bucket_name=bucket_name, key=path))
        except Exception as e:
            logger.error(f"Error while reading device ids, Ingest Faunabit probably did not run yet! error: {e}")
            raise

        return [{"number": obj["name"][8:], "id": obj["id"]} for obj in json_obj]
