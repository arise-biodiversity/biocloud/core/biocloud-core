import json
import logging

from pyspark.sql import SparkSession

from biocloudcore.data_lake import DataLake
from biocloudcore.landing_zone.nanopore.api.nanopore_api import NanoporeAPI
from biocloudcore.spark_session import get_databricks_secret

logger = logging.getLogger(__name__)


class Nanopore:
    def __init__(self, spark: SparkSession, data_lake: DataLake):
        self.spark = spark
        self.data_lake = data_lake
        self.s3_client = self.data_lake.csc.get_client()

    def run(self, bucket_name: str, api_url: str, resources: list[str], api_timeout: int):
        nanopore_api = NanoporeAPI(
            url=api_url,
            token=get_databricks_secret("NANOPORE_TOKEN", domain="csc", env=self.data_lake.env),
            timeout=api_timeout,
        )

        nanopore_data = nanopore_api.get_data(resources=resources)

        for resource in nanopore_data:
            self.s3_client.put_object(
                bucket_name=bucket_name,
                key=f"nanopore/{resource}.json",
                body=json.dumps(nanopore_data[resource]),
            )

        logger.info("Successfully fetched and stored Nanopore data in S3.")
