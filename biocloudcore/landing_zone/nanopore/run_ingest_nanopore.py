import logging

from biocloudcore.data_lake import DataLake
from biocloudcore.landing_zone.nanopore.ingest_nanopore import Nanopore
from biocloudcore.spark_session import get_environment, spark_session


def run():
    # Set the log level for custom written logs throughout the code
    logging.getLogger("root").setLevel(level=logging.INFO)

    spark = spark_session()
    data_lake = DataLake(env=get_environment(spark))

    Nanopore(
        spark=spark,
        data_lake=data_lake,
    ).run(
        bucket_name=data_lake.nanopore.landing_zone.get_bucket_name(),
        api_url="https://sdr-development-api.hosts.naturalis.io/api-csc/",
        resources=["consensuses", "sequencing_runs", "amplicons"],
        api_timeout=900,
    )

    spark.stop()


if __name__ == "__main__":
    run()
