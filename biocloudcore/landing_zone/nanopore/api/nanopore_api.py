import logging

import requests

logger = logging.getLogger(__name__)


class NanoporeAPI:
    """Class for querying the Nanopore API to get the metadata."""

    AVAILABLE_API_RESOURCES = ["consensuses", "sequencing_runs", "amplicons"]

    def __init__(self, url: str, token: str, timeout: int):
        self.url = url
        self.token = token
        self.timeout = timeout
        self.endpoint = "retrieve_data"

    def get_data(self, resources: list[str]) -> dict:
        """
        Fetches data from Nanopore API and returns a dictionary containing consensuses, sequencing runs, and amplicons.

        return dict: A dictionary containing consensuses, sequencing runs, and amplicons fetched from Nanopore API.
        """

        for resource in resources:
            if resource not in self.AVAILABLE_API_RESOURCES:
                raise ValueError(f"Not a valid Nanopore resource: {resource}.")

        logging.info("Fetching data from Nanopore API...")

        json_response = self._call_api(endpoint="retrieve_data")

        logging.info("Successfully fetched all data from Nanopore API.")

        return {resource: json_response[resource] for resource in resources}

    def _call_api(self, endpoint: str):
        """Get Resources from the Nanopore API

        :param str endpoint: name of the endpoint to get the data from
        :return json: json response containing the data of the resource
        """

        response = requests.get(
            url=f"{self.url}{endpoint}/",
            headers={"Authorization": f"Token {self.token}"},
            timeout=self.timeout,
        )

        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            logging.error(f"{response.status_code} {response.reason}")
            raise e

        return response.json()
